package model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "terms")
public class TermOfLecture {

	@Id
	private int id;

	@ManyToOne(fetch = FetchType.LAZY, cascade = { CascadeType.DETACH })
	private Professor professor;

	@ManyToOne(fetch = FetchType.LAZY, cascade = { CascadeType.DETACH })
	private StudentClass studentClass;

	@ManyToOne(fetch = FetchType.LAZY, cascade = { CascadeType.DETACH })
	private Subject subject;

	@ManyToOne(fetch = FetchType.LAZY, cascade = { CascadeType.DETACH })
	private Room room;

	@Column(nullable = false)
	private int dayOfWeek;

	@Column(nullable = false)
	private int ordinalNumberOfHour;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Professor getProfessor() {
		return professor;
	}

	public void setProfessor(Professor professor) {
		this.professor = professor;
	}

	public StudentClass getStudentClass() {
		return studentClass;
	}

	public void setStudentClass(StudentClass studentClass) {
		this.studentClass = studentClass;
	}

	public Subject getSubject() {
		return subject;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	public Room getRoom() {
		return room;
	}

	public void setRoom(Room room) {
		this.room = room;
	}

	public int getDayOfWeek() {
		return dayOfWeek;
	}

	public void setDayOfWeek(int dayOfWeek) {
		this.dayOfWeek = dayOfWeek;
	}

	public int getOrdinalNumberOfHour() {
		return ordinalNumberOfHour;
	}

	public void setOrdinalNumberOfHour(int ordinalNumberOfHour) {
		this.ordinalNumberOfHour = ordinalNumberOfHour;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + dayOfWeek;
		result = prime * result + ordinalNumberOfHour;
		result = prime * result + ((studentClass == null) ? 0 : studentClass.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TermOfLecture other = (TermOfLecture) obj;
		if (dayOfWeek != other.dayOfWeek)
			return false;
		if (ordinalNumberOfHour != other.ordinalNumberOfHour)
			return false;
		if (studentClass == null) {
			if (other.studentClass != null)
				return false;
		} else if (!studentClass.equals(other.studentClass))
			return false;
		return true;
	}

}