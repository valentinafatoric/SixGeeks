package model;

import java.io.Serializable;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "subjectsInfos")
@IdClass(SubjectInfoId.class)
@Cacheable(true)
public class SubjectInfoForProf {

	@Id
	@ManyToOne(fetch = FetchType.LAZY, cascade = { CascadeType.DETACH })
	private Professor professor;

	@Id
	@ManyToOne(fetch = FetchType.LAZY, cascade = { CascadeType.DETACH })
	private Subject subject;

	@Id
	@Column(nullable = false, length = 15)
	private String schoolYear;

	@Column(nullable = false, length = 1000)
	private String curriculum;

	@Column(nullable = false, length = 1000)
	private String teachingLetter;

	public Professor getProfessor() {
		return professor;
	}

	public void setProfessor(Professor professor) {
		this.professor = professor;
	}

	public Subject getSubject() {
		return subject;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	public String getSchoolYear() {
		return schoolYear;
	}

	public void setSchoolYear(String schoolYear) {
		this.schoolYear = schoolYear;
	}

	public String getCurriculum() {
		return curriculum;
	}

	public void setCurriculum(String curriculum) {
		this.curriculum = curriculum;
	}

	public String getTeachingLetter() {
		return teachingLetter;
	}

	public void setTeachingLetter(String teachingLetter) {
		this.teachingLetter = teachingLetter;
	}

}

class SubjectInfoId implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	Professor professor;

	Subject subject;

	String schoolYear;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((professor == null) ? 0 : professor.hashCode());
		result = prime * result + ((schoolYear == null) ? 0 : schoolYear.hashCode());
		result = prime * result + ((subject == null) ? 0 : subject.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SubjectInfoId other = (SubjectInfoId) obj;
		if (professor == null) {
			if (other.professor != null)
				return false;
		} else if (!professor.equals(other.professor))
			return false;
		if (schoolYear == null) {
			if (other.schoolYear != null)
				return false;
		} else if (!schoolYear.equals(other.schoolYear))
			return false;
		if (subject == null) {
			if (other.subject != null)
				return false;
		} else if (!subject.equals(other.subject))
			return false;
		return true;
	}

}
