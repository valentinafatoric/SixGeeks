package model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "consultations")
@IdClass(ConsultationId.class)
@Cacheable(true)
public class Consultation {

	@Id
	@ManyToOne(fetch = FetchType.LAZY, cascade = { CascadeType.DETACH })
	private StudentClass studentClass;

	@Id
	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable = false)
	private Date dateAndTime;

	@ManyToOne(fetch = FetchType.LAZY, cascade = { CascadeType.DETACH })
	private Room room;

	private boolean parentMeeting;

	public StudentClass getStudentClass() {
		return studentClass;
	}

	public void setStudentClass(StudentClass studentClass) {
		this.studentClass = studentClass;
	}

	public Date getDateAndTime() {
		return dateAndTime;
	}

	public void setDateAndTime(Date dateAndTime) {
		this.dateAndTime = dateAndTime;
	}

	public Room getRoom() {
		return room;
	}

	public void setRoom(Room room) {
		this.room = room;
	}

	public boolean getParentMeeting() {
		return parentMeeting;
	}

	public void setParentMeeting(boolean parentMeeting) {
		this.parentMeeting = parentMeeting;
	}

}

class ConsultationId implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	StudentClass studentClass;

	Date dateAndTime;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dateAndTime == null) ? 0 : dateAndTime.hashCode());
		result = prime * result + ((studentClass == null) ? 0 : studentClass.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ConsultationId other = (ConsultationId) obj;
		if (dateAndTime == null) {
			if (other.dateAndTime != null)
				return false;
		} else if (!dateAndTime.equals(other.dateAndTime))
			return false;
		if (studentClass == null) {
			if (other.studentClass != null)
				return false;
		} else if (!studentClass.equals(other.studentClass))
			return false;
		return true;
	}

}