package model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import dao.DAOProvider;

@Entity
@Table(name = "professors")
public class Professor extends User {

	@Column(nullable = false, length = 45)
	private String name;

	@Column(nullable = false, length = 45)
	private String surname;

	@Column(nullable = false, length = 11)
	private String OIB;

	@Column(nullable = false, length = 15)
	private String phoneNumber;
	
	@Column(nullable = false, length = 1000)
	private String picture;

	@ManyToOne(fetch = FetchType.LAZY, cascade = { CascadeType.DETACH })
	private School school;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getOIB() {
		return OIB;
	}

	public void setOIB(String oIB) {
		OIB = oIB;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	public School getSchool() {
		return school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public boolean isDeputyProfessor() {
		if (DAOProvider.getDAO().getClassOfDeputyProfessor(this) != null) {
			return true;
		} else {
			return false;
		}
	}

}
