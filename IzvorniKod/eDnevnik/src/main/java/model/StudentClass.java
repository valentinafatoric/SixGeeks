package model;

import java.util.LinkedList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "classes")
public class StudentClass {

	@Id
	@GeneratedValue
	private int id;

	@Column(nullable = false, length = 5)
	private String name;

	@Column(nullable = false, length = 15)
	private String schoolYear;

	@OneToMany(mappedBy = "classOfStudent", fetch = FetchType.LAZY, cascade = { CascadeType.MERGE, CascadeType.DETACH })
	private List<Student> students = new LinkedList<>();

	@OneToOne(fetch = FetchType.LAZY, cascade = { CascadeType.DETACH })
	private Professor professor;

	@OneToOne(fetch = FetchType.LAZY, cascade = { CascadeType.DETACH })
	private Professor deputyProfessor;

	@ManyToOne(fetch = FetchType.LAZY, cascade = { CascadeType.DETACH })
	private School school;

	@OneToMany(mappedBy = "studentClass", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<Consultation> consultations = new LinkedList<>();

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSchoolYear() {
		return schoolYear;
	}

	public void setSchoolYear(String schoolYear) {
		this.schoolYear = schoolYear;
	}

	public List<Student> getStudents() {
		return students;
	}

	public void setStudents(List<Student> students) {
		this.students = students;
	}

	public Professor getProfessor() {
		return professor;
	}

	public void setProfessor(Professor professor) {
		this.professor = professor;
	}

	public Professor getDeputyProfessor() {
		return deputyProfessor;
	}

	public void setDeputyProfessor(Professor deputyProfessor) {
		this.deputyProfessor = deputyProfessor;
	}

	public School getSchool() {
		return school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public List<Consultation> getConsultations() {
		return consultations;
	}

	public void setConsultations(List<Consultation> consultations) {
		this.consultations = consultations;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StudentClass other = (StudentClass) obj;
		if (id != other.id)
			return false;
		return true;
	}

}
