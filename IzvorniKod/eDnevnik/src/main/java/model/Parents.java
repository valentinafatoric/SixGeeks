package model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "parents")
public class Parents extends User {

	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Student child;

	@Column(nullable = false, length = 100)
	private String mothersName;

	@Column(nullable = false, length = 100)
	private String fathersName;

	public Student getChild() {
		return child;
	}

	public void setChild(Student child) {
		this.child = child;
	}

	public String getMothersName() {
		return mothersName;
	}

	public void setMothersName(String mothersName) {
		this.mothersName = mothersName;
	}

	public String getFathersName() {
		return fathersName;
	}

	public void setFathersName(String fathersName) {
		this.fathersName = fathersName;
	}

}
