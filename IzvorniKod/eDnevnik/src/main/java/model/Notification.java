package model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="notifications")
public class Notification {

	@Id @GeneratedValue
	private int id;
	
	@Column(nullable=false, length=30)
	private String title;
	
	@ManyToOne(fetch=FetchType.LAZY, cascade={CascadeType.DETACH})
	private Administrator creator;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable=false)
	private Date date;

	@Column(nullable=false, length=150)
	private String text;

	@Column(nullable=true, length=500)
	private String picture;
	
	@Column(nullable=false)
	private boolean archived;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Administrator getCreator() {
		return creator;
	}

	public void setCreator(Administrator creator) {
		this.creator = creator;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}
	
	public void setArchived(boolean archived) {
		this.archived = archived;
	}
	
	public boolean isArchived() {
		return archived;
	}
	
	public int getId() {
		return id;
	}
	
	
}
