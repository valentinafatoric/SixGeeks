package model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "students")
public class Student extends User {

	@Column(nullable = false, length = 45)
	private String name;

	@Column(nullable = false, length = 45)
	private String surname;

	@Column(nullable = false, length = 10)
	private String sex;

	@Column(nullable = false, length = 11)
	private String OIB;

	@Temporal(TemporalType.DATE)
	@Column(nullable = false)
	private Date dateOfBirth;

	@Column(nullable = false, length = 45)
	private String placeOfBirth;

	@Column(nullable = false, length = 1000)
	private String picture;

	@Column(nullable = false, length = 45)
	private String currentStatus;

	@OneToOne(mappedBy = "child", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	private Parents parents;

	@ManyToOne(fetch = FetchType.LAZY, cascade = { CascadeType.DETACH })
	private School school;

	@ManyToOne(fetch = FetchType.LAZY, cascade = { CascadeType.DETACH })
	private StudentClass classOfStudent;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getOIB() {
		return OIB;
	}

	public void setOIB(String oIB) {
		OIB = oIB;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getPlaceOfBirth() {
		return placeOfBirth;
	}

	public void setPlaceOfBirth(String placeOfBirth) {
		this.placeOfBirth = placeOfBirth;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	public String getCurrentStatus() {
		return currentStatus;
	}

	public void setCurrentStatus(String currentStatus) {
		this.currentStatus = currentStatus;
	}

	public Parents getParents() {
		return parents;
	}

	public void setParents(Parents parents) {
		this.parents = parents;
	}

	public School getSchool() {
		return school;
	}

	public void setSchool(School school) {
		this.school = school;
	}

	public StudentClass getClassOfStudent() {
		return classOfStudent;
	}

	public void setClassOfStudent(StudentClass classOfStudent) {
		this.classOfStudent = classOfStudent;
	}

}
