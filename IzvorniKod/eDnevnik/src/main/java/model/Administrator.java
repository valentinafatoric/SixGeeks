package model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import dao.DAOProvider;

@Entity
@Table(name = "administrators")
public class Administrator extends User {

	@Column(nullable = false, length = 45)
	private String name;

	@Column(nullable = false, length = 45)
	private String surname;

	@Column(nullable = false, length = 11)
	private String OIB;

	@ManyToOne(fetch = FetchType.EAGER, cascade = { CascadeType.DETACH })
	private School school;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getOIB() {
		return OIB;
	}

	public void setOIB(String oIB) {
		OIB = oIB;
	}

	public School getSchool() {
		return DAOProvider.getDAO().getSchool(school);
	}

	public void setSchool(School school) {
		this.school = school;
	}

}
