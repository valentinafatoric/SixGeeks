package model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "comments")
@IdClass(CommentId.class)
@Cacheable(true)
public class Comment {

	@Id
	@ManyToOne(fetch = FetchType.LAZY, cascade = { CascadeType.DETACH })
	private SubjectGradesForStudent studentSubject;

	@Id
	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable = false)
	private Date date;

	@Column(nullable = false, length = 150)
	private String text;

	public SubjectGradesForStudent getStudentSubject() {
		return studentSubject;
	}

	public void setStudentSubject(SubjectGradesForStudent studentSubject) {
		this.studentSubject = studentSubject;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

}

class CommentId implements Serializable {

	private static final long serialVersionUID = 1L;

	SubjectGradesForStudent studentSubject;

	Date date;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + ((studentSubject == null) ? 0 : studentSubject.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CommentId other = (CommentId) obj;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (studentSubject == null) {
			if (other.studentSubject != null)
				return false;
		} else if (!studentSubject.equals(other.studentSubject))
			return false;
		return true;
	}

}
