package model;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "subjectGrades")
@IdClass(SubjectGradesId.class)
@Cacheable(true)
public class SubjectGradesForStudent {

	@Id
	@ManyToOne(fetch = FetchType.LAZY, cascade = { CascadeType.DETACH })
	private Student student;

	@Id
	@ManyToOne(fetch = FetchType.LAZY, cascade = { CascadeType.DETACH })
	private Subject subject;

	@Id
	@Column(nullable = false, length = 15)
	private String schoolYear;

	private boolean currentlyActive;

	@Column(nullable = true)
	private int finalGrade;

	@OneToMany(mappedBy = "studentSubject", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	private List<Grade> grades = new LinkedList<>();

	@OneToMany(mappedBy = "studentSubject", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	private List<Comment> comments = new LinkedList<>();

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public Subject getSubject() {
		return subject;
	}

	public void setSubject(Subject subject) {
		this.subject = subject;
	}

	public String getSchoolYear() {
		return schoolYear;
	}

	public void setSchoolYear(String schoolYear) {
		this.schoolYear = schoolYear;
	}

	public boolean isCurrentlyActive() {
		return currentlyActive;
	}

	public void setCurrentlyActive(boolean currentlyActive) {
		this.currentlyActive = currentlyActive;
	}

	public int getFinalGrade() {
		return finalGrade;
	}

	public void setFinalGrade(int finalGrade) {
		this.finalGrade = finalGrade;
	}

	public List<Grade> getGrades() {
		return grades;
	}

	public void setGrades(List<Grade> grades) {
		this.grades = grades;
	}

	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comment) {
		this.comments = comment;
	}

}

class SubjectGradesId implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	Student student;

	Subject subject;

	String schoolYear;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((schoolYear == null) ? 0 : schoolYear.hashCode());
		result = prime * result + ((student == null) ? 0 : student.hashCode());
		result = prime * result + ((subject == null) ? 0 : subject.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SubjectGradesId other = (SubjectGradesId) obj;
		if (schoolYear == null) {
			if (other.schoolYear != null)
				return false;
		} else if (!schoolYear.equals(other.schoolYear))
			return false;
		if (student == null) {
			if (other.student != null)
				return false;
		} else if (!student.equals(other.student))
			return false;
		if (subject == null) {
			if (other.subject != null)
				return false;
		} else if (!subject.equals(other.subject))
			return false;
		return true;
	}

}
