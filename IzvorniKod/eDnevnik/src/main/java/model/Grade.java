package model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "grades")
@IdClass(GradeId.class)
public class Grade {

	@Id
	@ManyToOne(fetch = FetchType.LAZY, cascade = { CascadeType.DETACH })
	private SubjectGradesForStudent studentSubject;

	@Id
	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable = false)
	private Date date;

	@Column(nullable = false)
	private int value;

	@Column(nullable = false, length = 30)
	private String component;

	public SubjectGradesForStudent getStudentSubject() {
		return studentSubject;
	}

	public void setStudentSubject(SubjectGradesForStudent studentSubject) {
		this.studentSubject = studentSubject;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public String getComponent() {
		return component;
	}

	public void setComponent(String component) {
		this.component = component;
	}

}

class GradeId implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	SubjectGradesForStudent studentSubject;

	Date date;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + ((studentSubject == null) ? 0 : studentSubject.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GradeId other = (GradeId) obj;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (studentSubject == null) {
			if (other.studentSubject != null)
				return false;
		} else if (!studentSubject.equals(other.studentSubject))
			return false;
		return true;
	}

}
