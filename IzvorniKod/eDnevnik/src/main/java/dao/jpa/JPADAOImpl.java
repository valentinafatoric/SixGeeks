package dao.jpa;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;

import dao.DAO;
import dao.DAOException;
import model.Absence;
import model.Administrator;
import model.Comment;
import model.Consultation;
import model.EventDiary;
import model.Grade;
import model.Notification;
import model.Parents;
import model.Professor;
import model.Room;
import model.School;
import model.Student;
import model.StudentClass;
import model.Subject;
import model.SubjectGradesForStudent;
import model.SubjectInfoForProf;
import model.TermOfLecture;
import model.User;

public class JPADAOImpl implements DAO {
	
	@Override
	public User getUser(String username) {
		User user = JPAEMProvider.getEntityManager().find(Student.class, username);
		if (user != null) {
			return user;
		}

		user = JPAEMProvider.getEntityManager().find(Parents.class, username);
		if (user != null) {
			return user;
		}

		user = JPAEMProvider.getEntityManager().find(Professor.class, username);
		if (user != null) {
			return user;
		}

		user = JPAEMProvider.getEntityManager().find(Administrator.class, username);
		return user;
	}

	@Override
	public void addOrUpdateAbsence(Absence absence) {
		JPAEMProvider.getEntityManager().merge(absence);
	}
	
	@Override
	public Absence getAbsence(TermOfLecture term, Student student, Date date) {
		try {
			Query query = JPAEMProvider.getEntityManager().createQuery(
					"select a from Absence as a where a.student = :student and a.term = :term and a.date = :date", Absence.class);
			query.setParameter("student", student);
			query.setParameter("date", date);
			query.setParameter("term", term);

			return (Absence) query.getSingleResult();
		} catch (NoResultException ex) {
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Absence> getAbsencesForStudent(Student student) {
		Query query = JPAEMProvider.getEntityManager()
				.createQuery("select a from Absence as a where a.student = :student", Absence.class);
		query.setParameter("student", student);

		return query.getResultList();
	}
	
	@Override
	public List<Absence> getAbsencesForDate(TermOfLecture term, Date date) {
		TypedQuery<Absence> query = JPAEMProvider.getEntityManager()
				.createQuery("select a from Absence as a where a.term = :term and a.date = :date", Absence.class);
		query.setParameter("term", term);
		query.setParameter("date", date);
		
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Absence> getAbsencesForClass(StudentClass studentClass, Date date, TermOfLecture term) {
		List<Absence> absences = new LinkedList<>();
		Query query = null;

		for (Student student : studentClass.getStudents()) {
			query = JPAEMProvider.getEntityManager().createQuery(
					"select a from Absence as a where a.student = :student and a.date = :date and a.term = :term", Absence.class);
			query.setParameter("student", student);
			query.setParameter("date", date);
			query.setParameter("term", term);
			
			if (query.getResultList() != null && !query.getResultList().isEmpty()) {
				absences.addAll(query.getResultList());
			}
		}
		
		return absences;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Map<Student, List<Absence>> getAbsencesForClass(StudentClass studentClass) {
		Map<Student, List<Absence>> map = new HashMap<>();
		Query query = null;

		for (Student student : studentClass.getStudents()) {
			query = JPAEMProvider.getEntityManager()
					.createQuery("select a from Absence as a where a.student = :student", Absence.class);
			query.setParameter("student", student);
			
			if (query.getResultList() != null && !query.getResultList().isEmpty()) {
				map.put(student, query.getResultList());
			}
		}
		
		return map;
	}

	@Override
	public void addAdministrator(Administrator admin) {
		JPAEMProvider.getEntityManager().persist(admin);
	}

	@Override
	public void addOrUpdateComment(Comment comment) {
		JPAEMProvider.getEntityManager().merge(comment);
	}
	
	@Override
	public void deleteComment(SubjectGradesForStudent studentSubject, Date date) {
		try {
			Query query = JPAEMProvider.getEntityManager().createQuery(
					"select c from Comment c where c.date = :date and c.studentSubject = :studentSubject", Comment.class);
			query.setParameter("date", date);
			query.setParameter("studentSubject", studentSubject);

			Comment comment = (Comment) query.getSingleResult();
			JPAEMProvider.getEntityManager().remove(comment);
		} catch (Exception ex) {
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Comment> getCommentsForSubject(SubjectGradesForStudent subjectGrades) {
		Query query = JPAEMProvider.getEntityManager()
				.createQuery("select c from Comment as c where c.studentSubject = :subjectGrades", Comment.class);
		query.setParameter("subjectGrades", subjectGrades);

		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Comment> getCommentsForSubject(Student student, Subject subject) {
		Query query = JPAEMProvider.getEntityManager().createQuery(
				"select c from Comment as s where c.studentSubject.student = :student and c.studentSubject.subject = :subject", Comment.class);
		query.setParameter("student", student);
		query.setParameter("subject", subject);

		return query.getResultList();
	}

	@Override
	public void addOrUpdateConsultation(Consultation consultation) {
		JPAEMProvider.getEntityManager().merge(consultation);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Consultation> getConsultations(StudentClass studentClass) {
		Query query = JPAEMProvider.getEntityManager().createQuery(
				"select c from Consultation as c where c.studentClass = :studentClass", Consultation.class);
		query.setParameter("studentClass", studentClass);
		
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Consultation> getConsultations(StudentClass classOfStudent, boolean parentMeeting) {
		Query query = JPAEMProvider.getEntityManager().createQuery(
				"select c from Consultation as c where c.studentClass = :studentClass and c.parentMeeting = :parentMeeting", Consultation.class);
		query.setParameter("studentClass", classOfStudent);
		query.setParameter("parentMeeting", parentMeeting);

		return query.getResultList();
	}

	@Override
	public void addOrUpdateGrade(Grade grade) {
		JPAEMProvider.getEntityManager().merge(grade);
	}
	
	@Override
	public void deleteGrade(SubjectGradesForStudent studentSubject, Date date) {
		try {
			Query query = JPAEMProvider.getEntityManager().createQuery(
					"select g from Grade g where g.date = :date and g.studentSubject = :studentSubject", Grade.class);
			query.setParameter("date", date);
			query.setParameter("studentSubject", studentSubject);

			Grade grade = (Grade) query.getSingleResult();
			JPAEMProvider.getEntityManager().remove(grade);
		} catch (Exception ex) {
		}
	}

	@Override
	public void addOrUpdateNotification(Notification notification) {
		JPAEMProvider.getEntityManager().merge(notification);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Notification> getCurrentNotifications() {
		Query query = JPAEMProvider.getEntityManager()
				.createQuery("select n from Notification as n where n.date >= :date", Notification.class);

		Date date = new Date();
		Calendar c = Calendar.getInstance();
		c.add(Calendar.DATE, -60);
		date.setTime(c.getTime().getTime());
		query.setParameter("date", date, TemporalType.TIMESTAMP);
		
		List<Notification> notifications = (List<Notification>) query.getResultList();
		
		notifications = notifications.stream()
				  					.sorted((n1, n2)->n2.getDate().compareTo(n1.getDate()))
				  					.collect(Collectors.toList());

		return notifications;
	}

	@Override
	public void addOrUpdateParents(Parents parents) {
		JPAEMProvider.getEntityManager().merge(parents);
	}

	@Override
	public void addProfessor(Professor professor) {
		JPAEMProvider.getEntityManager().persist(professor);
	}

	@Override
	public void updateProfessor(Professor professor) {
		JPAEMProvider.getEntityManager().merge(professor);
	}

	@Override
	public void addRoom(Room room) {
		JPAEMProvider.getEntityManager().persist(room);
	}
	
	@Override
	public Room getRoom(String name) {
		try {
			TypedQuery<Room> query = JPAEMProvider.getEntityManager()
					.createQuery("select r from Room as r where r.name = :name", Room.class);
			query.setParameter("name", name);
			
			return query.getSingleResult();
		} catch (NoResultException ex) {
			return null;
		}
	}

	@Override
	public void addSchool(School school) {
		JPAEMProvider.getEntityManager().persist(school);
	}
	
	@Override
	public School getSchool(School school) {
		try {
			Query query = JPAEMProvider.getEntityManager()
					.createQuery("select p.school from Professor as p where p.school = :school", School.class);
			query.setParameter("school", school);
			return (School) query.getSingleResult();
		} catch (NoResultException ex) {
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<School> getSchools() {
		Query query = JPAEMProvider.getEntityManager().createQuery("select s from School as s", School.class);
		return query.getResultList();
	}

	public void addOrUpdateStudent(Student student) {
		JPAEMProvider.getEntityManager().merge(student);
	}
	
	@Override
	public Student getStudent(String username) throws DAOException {
		return JPAEMProvider.getEntityManager().find(Student.class, username);
	}
	
	@Override
	public Student getStudent(Student student) {
		try {
			Query query = JPAEMProvider.getEntityManager()
					.createQuery("select p.child from Parents as p where p.child = :student", Student.class);
			query.setParameter("student", student);
			return (Student) query.getSingleResult();
		} catch (NoResultException ex) {
			return null;
		}
	}
	
	@Override
	public Student getStudentByOIB(String OIB) {
		try {
			Query query = JPAEMProvider.getEntityManager().createQuery("select s from Student as s where s.OIB = :OIB",
					Student.class);
			query.setParameter("OIB", OIB);
			return (Student) query.getSingleResult();
		} catch (NoResultException ex) {
			return null;
		}
	}

	@Override
	public void addStudentClass(StudentClass studentClass) {
		JPAEMProvider.getEntityManager().persist(studentClass);
	}

	@Override
	public void updateStudentClass(StudentClass studentClass) {
		JPAEMProvider.getEntityManager().merge(studentClass);
	}
	
	@Override
	public StudentClass getStudentClass(int id) {
		return JPAEMProvider.getEntityManager().find(StudentClass.class, id);
	}
	
	@Override
	public StudentClass getStudentClass(String className, String schoolYear) {
		try {
			TypedQuery<StudentClass> query = JPAEMProvider.getEntityManager().createQuery(
					"select s from StudentClass as s where s.schoolYear = :schoolYear and s.name = :className", StudentClass.class);
			query.setParameter("schoolYear", schoolYear);
			query.setParameter("className", className);
			
			return query.getSingleResult();
		} catch (NoResultException ex) {
			return null;
		}
	}

	
	@Override
	public StudentClass getStudentClass(StudentClass studentClass) {
		try {
			Query query = JPAEMProvider.getEntityManager().createQuery(
					"select c.classOfStudent from Student as c where c.classOfStudent = :classOfStudent", StudentClass.class);
			query.setParameter("classOfStudent", studentClass);
			return (StudentClass) query.getSingleResult();
		} catch (NoResultException ex) {
			return null;
		}
	}
	
	@Override
	public List<StudentClass> getClasses() {
		TypedQuery<StudentClass> query = JPAEMProvider.getEntityManager().createQuery("select s from StudentClass as s",
				StudentClass.class);
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Set<StudentClass> getClassesForProfessorSubject(SubjectInfoForProf subInfoForProfessor) {
		Query query = JPAEMProvider.getEntityManager().createQuery(
				"select t.studentClass from TermOfLecture as t where t.professor = :professor and t.subject = :subject", StudentClass.class);
		query.setParameter("professor", subInfoForProfessor.getProfessor());
		query.setParameter("subject", subInfoForProfessor.getSubject());
		
		Set<StudentClass> set = new HashSet<>();
		set.addAll(query.getResultList());

		return set;
	}

	@Override
	public StudentClass getClassOfDeputyProfessor(Professor professor) {
		List<StudentClass> classes = getClasses();
		
		for (StudentClass c : classes) {
			if (c.getDeputyProfessor().getOIB().equals(professor.getOIB())
					|| c.getProfessor().getOIB().equals(professor.getOIB())) {
				return c;
			}
		}
		
		return null;
	}

	@Override
	public void addSubject(Subject subject) {
		JPAEMProvider.getEntityManager().persist(subject);
	}
	
	@Override
	public List<Subject> getSubjects() throws DAOException {
		TypedQuery<Subject> query = JPAEMProvider.getEntityManager().createQuery("select s from Subject as s", Subject.class);
		return query.getResultList();
	}
	
	@Override
	public Subject getSubject(int id) {
		return JPAEMProvider.getEntityManager().find(Subject.class, id);
	}

	@Override
	public void addOrUpdateSubjectGradesForStudent(SubjectGradesForStudent subjectGradesForStudent) {
		JPAEMProvider.getEntityManager().merge(subjectGradesForStudent);
	}
	
	@Override
	public SubjectGradesForStudent getSubjectGradesForStudent(Student student, Subject subject, String schoolYear) {
		try {
			Query query = JPAEMProvider.getEntityManager().createQuery(
					"select g from SubjectGradesForStudent as g where g.student = :student and g.subject = :subject and g.schoolYear = :schoolYear", SubjectGradesForStudent.class);
			query.setParameter("student", student);
			query.setParameter("subject", subject);
			query.setParameter("schoolYear", schoolYear);

			return (SubjectGradesForStudent) query.getSingleResult();
		} catch (NoResultException ex) {
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SubjectGradesForStudent> getCurrentlyActiveSubjectsForStudent(Student student) {
		Query query = JPAEMProvider.getEntityManager().createQuery(
				"select g from SubjectGradesForStudent as g where g.student = :student and g.currentlyActive = :currentlyActive", SubjectGradesForStudent.class);
		query.setParameter("student", student);
		query.setParameter("currentlyActive", true);

		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SubjectGradesForStudent> getSubjectsOfSchoolYearForStudent(Student student, String schoolYear) {
		Query query = JPAEMProvider.getEntityManager().createQuery(
				"select g from SubjectGradesForStudent as g where g.student = :student and g.schoolYear = :schoolYear", SubjectGradesForStudent.class);
		query.setParameter("student", student);
		query.setParameter("schoolYear", schoolYear);

		return query.getResultList();
	}

	@Override
	public void addOrUpdateSubjectInfoForProf(SubjectInfoForProf subjectInfoForProf) {
		JPAEMProvider.getEntityManager().merge(subjectInfoForProf);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SubjectInfoForProf> getSubjectInfoForProfessor(Professor professor, String schoolYear) {
		Query query = JPAEMProvider.getEntityManager().createQuery(
				"select c from SubjectInfoForProf as c where c.professor = :professor  and c.schoolYear = :schoolYear", SubjectInfoForProf.class);
		query.setParameter("professor", professor);
		query.setParameter("schoolYear", schoolYear);

		return query.getResultList();
	}

	@Override
	public void addOrUpdateTermOfLecture(TermOfLecture term) {
		JPAEMProvider.getEntityManager().merge(term);
	}
	
	@Override
	public TermOfLecture getTerm(int id) {
		return JPAEMProvider.getEntityManager().find(TermOfLecture.class, id);
	}
	
	@Override
	public TermOfLecture getTerm(Professor professor, StudentClass studentClass, Subject subject, Room room,
			int dayOfWeek, int hour) {
		try {
			TypedQuery<TermOfLecture> query = JPAEMProvider.getEntityManager().createQuery(
					"select t from TermOfLecture as t where t.professor=:professor and t.studentClass = :studentClass and "
							+ "t.subject = :subject and t.room = :room and t.dayOfWeek = :dayOfWeek and t.ordinalNumberOfHour = :hour", TermOfLecture.class);
			query.setParameter("professor", professor);
			query.setParameter("studentClass", studentClass);
			query.setParameter("subject", subject);
			query.setParameter("room", room);
			query.setParameter("dayOfWeek", dayOfWeek);
			query.setParameter("hour", hour);
			
			return query.getSingleResult();
		} catch (NoResultException ex) {
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TermOfLecture> getTermsForSubject(Subject subject, StudentClass studentClass) {
		Query query = JPAEMProvider.getEntityManager().createQuery(
				"select t from TermOfLecture as t where t.subject = :subject and t.studentClass = :studentClass", TermOfLecture.class);
		query.setParameter("subject", subject);
		query.setParameter("studentClass", studentClass);

		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	private Map<String, TermOfLecture> getTermsForProfessor(int dayOfWeek, Professor professor) {
		Query query = JPAEMProvider.getEntityManager().createQuery(
				"select t from TermOfLecture as t where t.dayOfWeek = :dayOfWeek and t.professor = :professor", TermOfLecture.class);
		query.setParameter("dayOfWeek", dayOfWeek);
		query.setParameter("professor", professor);

		List<TermOfLecture> terms = query.getResultList();

		Map<String, TermOfLecture> mapTerms = new HashMap<>();

		for (TermOfLecture term : terms) {
			mapTerms.put(String.valueOf(term.getOrdinalNumberOfHour()), term);
		}

		return mapTerms;
	}

	@SuppressWarnings("unchecked")
	private Map<String, TermOfLecture> getTermsForStudent(int dayOfWeek, Student student) {
		Query query = JPAEMProvider.getEntityManager().createQuery(
				"select t from TermOfLecture as t where t.dayOfWeek = :dayOfWeek and t.studentClass = :studentClass", TermOfLecture.class);
		query.setParameter("dayOfWeek", dayOfWeek);
		query.setParameter("studentClass", student.getClassOfStudent());

		List<TermOfLecture> terms = query.getResultList();

		Map<String, TermOfLecture> mapTerms = new HashMap<>();

		for (TermOfLecture term : terms) {
			mapTerms.put(String.valueOf(term.getOrdinalNumberOfHour()), term);
		}

		return mapTerms;
	}

	@Override
	public Map<String, Map<String, TermOfLecture>> getSchedule(User user) {
		Map<String, Map<String, TermOfLecture>> schedule = new HashMap<>();

		for (int i = 1; i <= 7; i++) {
			if (user instanceof Student) {
				schedule.put(String.valueOf(i), getTermsForStudent(i, (Student) user));
			} else if (user instanceof Professor) {
				schedule.put(String.valueOf(i), getTermsForProfessor(i, (Professor) user));
			}
		}

		return schedule;
	}

	@Override
	public int getNextTermId() {
		TypedQuery<Long> query = JPAEMProvider.getEntityManager().createQuery("SELECT COUNT(t) FROM TermOfLecture as t",
				Long.class);
		
		return query.getSingleResult().intValue();
	}

	@Override
	public List<Professor> getProfessors() {
		TypedQuery<Professor> query = JPAEMProvider.getEntityManager().createQuery("select prof from Professor as prof", Professor.class);
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Professor> getProfessorByOIB(String OIB) {
		Query query = JPAEMProvider.getEntityManager().createQuery("select n from Professor as n where n.OIB = :oib", Professor.class);
		query.setParameter("oib", OIB);
		return query.getResultList();
	}

	@Override
	public List<Room> getRoomsInSchool(School school) {
		TypedQuery<Room> query = JPAEMProvider.getEntityManager().createQuery("select r from Room as r where r.school=:school", Room.class);
		query.setParameter("school", school);
		return query.getResultList();
	}

	@Override
	public Room getRoomById(int id) {
		TypedQuery<Room> query = JPAEMProvider.getEntityManager().createQuery("select r from Room as r where r.id = :id", Room.class);
		query.setParameter("id", id);
		return query.getResultList().get(0);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TermOfLecture> getLecturesForProfessorAtTime(Professor prof, int dayOfWeek, int hourOfDay) {
		Query query = JPAEMProvider.getEntityManager().createQuery("select t from TermOfLecture as t where t.professor = :professor"
				+ " and t.dayOfWeek = :dayOfWeek and t.ordinalNumberOfHour = :hour", TermOfLecture.class);

		query.setParameter("professor", prof);
		query.setParameter("dayOfWeek", dayOfWeek);
		query.setParameter("hour", hourOfDay);
		
		return query.getResultList();
	}

	@Override
	public List<TermOfLecture> getLectureForClassAt(StudentClass studentClass, int dayOfWeek, int hourOfDay) {
		TypedQuery<TermOfLecture> query = JPAEMProvider.getEntityManager().createQuery("select t from TermOfLecture as t where"
				+ " t.dayOfWeek=:dayOfWeek and t.ordinalNumberOfHour=:hour and t.studentClass=:studentClass", TermOfLecture.class);

		query.setParameter("studentClass", studentClass);
		query.setParameter("dayOfWeek",dayOfWeek);
		query.setParameter("hour",hourOfDay);
		return query.getResultList();
	}

	@Override
	public List<TermOfLecture> getLecturesAtTerm(int dayOfWeek, int hourOfDay) {
		TypedQuery<TermOfLecture> query = JPAEMProvider.getEntityManager().createQuery("select t from TermOfLecture as t where"
				+ " t.dayOfWeek=:dayOfWeek and t.ordinalNumberOfHour=:hour", TermOfLecture.class);

		query.setParameter("dayOfWeek",dayOfWeek);
		query.setParameter("hour",hourOfDay);
		return query.getResultList();
	}

	@Override
	public void addEvent(EventDiary event) {
		JPAEMProvider.getEntityManager().merge(event);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void deleteTermById(int id) {
		TermOfLecture term = getTerm(id);
		
		Query query = JPAEMProvider.getEntityManager().createQuery("select a from Absence as a where a.term = :term", Absence.class);
		query.setParameter("term", term);
		
		List<Absence> absences = query.getResultList();
		absences.forEach(a -> deleteAbsence(a));
		
		JPAEMProvider.getEntityManager().remove(term);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SubjectInfoForProf> getProfsOfSubjectsInYearAndSchool(School school, String year) {
		Query query = JPAEMProvider.getEntityManager().createQuery("select c from SubjectInfoForProf as c where c.schoolYear = :schoolYear "
				+ "and c.professor.school = :school", SubjectInfoForProf.class);
		query.setParameter("schoolYear", year);
		query.setParameter("school", school);
		List<SubjectInfoForProf> result = query.getResultList();
		return result;
	}

	@Override
	public SubjectInfoForProf getSubjectInfoForProfByProf(Professor prof) {
		TypedQuery<SubjectInfoForProf> query = JPAEMProvider.getEntityManager()
				.createQuery("select s from SubjectInfoForProf as s where s.professor = :prof", SubjectInfoForProf.class);
		query.setParameter("prof", prof);
		return query.getSingleResult();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<EventDiary> getEventDiaryForSchool(School school) {
		Query query = JPAEMProvider.getEntityManager().createQuery("select e from EventDiary as e where e.school = :school", EventDiary.class);

		query.setParameter("school", school);
		return query.getResultList();
	}

	@Override
	public List<Student> getStudents(){
		TypedQuery<Student> query = JPAEMProvider.getEntityManager().createQuery("select s from Student as s", Student.class);
		List<Student> students = query.getResultList().stream()
				.sorted((s1, s2) -> s1.getClassOfStudent().getName().compareTo(s2.getClassOfStudent().getName())).collect(Collectors.toList());
		
		return students;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Map<String, TermOfLecture>> getTermOfClass(StudentClass studentClass){
		Map<String, Map<String, TermOfLecture>> results = new LinkedHashMap<>();
		
		for (int i = 1; i <= 7; ++i) {
			Query query = JPAEMProvider.getEntityManager().createQuery("select t from TermOfLecture as t where t.dayOfWeek = :dayOfWeek and t.studentClass = :studentClass", TermOfLecture.class);
			query.setParameter("dayOfWeek", i);
			query.setParameter("studentClass", studentClass);
			
			List<TermOfLecture> terms = query.getResultList();
			Map<String, TermOfLecture> mapTerms = new HashMap<>();
			
			for (TermOfLecture term : terms) {
				mapTerms.put(String.valueOf(term.getOrdinalNumberOfHour()), term);
			}
			
			results.put(String.valueOf(i), mapTerms);
		}
		
		return results;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Notification> getAllNotificationsForSchool(School school) {
		Query query = JPAEMProvider.getEntityManager().createQuery("select n from Notification as n where n.creator.school = :school", Notification.class);
		
		query.setParameter("school", school);
		
		return query.getResultList();
	}

	@Override
	public void deleteNotification(Notification n) {
		JPAEMProvider.getEntityManager().remove(n);
	}
	
	@Override
	public Notification getNotificationById(int id) {
		Query query = JPAEMProvider.getEntityManager().createQuery("select n from Notification as n where n.id = :id", Notification.class);
		query.setParameter("id", id);
		
		return (Notification) query.getSingleResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public void deleteStudentClass(StudentClass studentClass) {
		Query query = JPAEMProvider.getEntityManager().createQuery("select s from Student as s where s.classOfStudent = :class", Student.class);
		query.setParameter("class", studentClass);
		
		List<Student> students = query.getResultList();
		students.forEach(s -> {
			s.setClassOfStudent(null);
			addOrUpdateStudent(s);
		});
		
		query = JPAEMProvider.getEntityManager().createQuery("select t from TermOfLecture as t where t.studentClass = :class", Student.class);
		query.setParameter("class", studentClass);
		
		List<TermOfLecture> terms = query.getResultList();
		terms.forEach(t -> deleteTermById(t.getId()));
		
		JPAEMProvider.getEntityManager().remove(studentClass);
	}

	@Override
	public void updateSubject(Subject subject) {
		JPAEMProvider.getEntityManager().merge(subject);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void deleteSubject(Subject subject) {
		Query query = JPAEMProvider.getEntityManager().createQuery("select s from SubjectInfoForProf as s where s.subject = :subject", SubjectInfoForProf.class);
		query.setParameter("subject", subject);
	
		List<SubjectInfoForProf> subjectInfos = query.getResultList();
		subjectInfos.forEach(s -> deleteSubjectInfoForProf(s));
		
		query = JPAEMProvider.getEntityManager().createQuery("select s from SubjectGradesForStudent as s where s.subject = :subject", SubjectGradesForStudent.class);
		query.setParameter("subject", subject);
		
		List<SubjectGradesForStudent> subjectGrades = query.getResultList();
		subjectGrades.forEach(s -> deleteSubjectGradeForStudent(s));
		
		query = JPAEMProvider.getEntityManager().createQuery("select t from TermOfLecture as t where t.subject = :subject", TermOfLecture.class);
		query.setParameter("subject", subject);
		
		List<TermOfLecture> terms = query.getResultList();
		terms.forEach(t -> deleteTermById(t.getId()));
		
		JPAEMProvider.getEntityManager().remove(subject);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<StudentClass> getClassesForSchool(School school) {
		Query query = JPAEMProvider.getEntityManager().createQuery("select c from StudentClass as c where c.school = :school", StudentClass.class);

		query.setParameter("school", school);
		return query.getResultList();
	}

	@Override
	public void deleteAbsence(Absence absence) {
		JPAEMProvider.getEntityManager().remove(absence);
	}

	@Override
	public void deleteConsultation(Consultation consultation) {
		JPAEMProvider.getEntityManager().remove(consultation);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void deleteProfessor(Professor professor) {
		Query query = JPAEMProvider.getEntityManager().createQuery("select t from TermOfLecture as t where t.professor = :professor", TermOfLecture.class);
		query.setParameter("professor", professor);
		
		List<TermOfLecture> terms = query.getResultList();
		terms.forEach(t -> deleteTermById(t.getId()));
		
		JPAEMProvider.getEntityManager().remove(getSubjectInfoForProfByProf(professor));
		JPAEMProvider.getEntityManager().remove(professor);
	}

	@Override
	public void deleteSubjectGradeForStudent(SubjectGradesForStudent subjectGradesForStudent) {
		Query query = JPAEMProvider.getEntityManager()
				.createQuery("delete from Grade g where g.studentSubject = :studentSubject");
		query.setParameter("studentSubject", subjectGradesForStudent);
		query.executeUpdate();

		query = JPAEMProvider.getEntityManager()
				.createQuery("delete from Comment c where c.studentSubject = :studentSubject");
		query.setParameter("studentSubject", subjectGradesForStudent);
		query.executeUpdate();

		JPAEMProvider.getEntityManager().remove(subjectGradesForStudent);
	}

	@Override
	public void deleteSubjectInfoForProf(SubjectInfoForProf subjectInfoForProf) {
		JPAEMProvider.getEntityManager().remove(subjectInfoForProf);
	}
}
