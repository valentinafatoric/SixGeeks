package dao.jpa;

import javax.persistence.EntityManagerFactory;

public class JPAEMFProvider {

	/** Entity manager Factory */
	public static EntityManagerFactory emf;

	/**
	 * Method that returns entity manager factory.
	 * 
	 * @return entity manager factory
	 */
	public static EntityManagerFactory getEmf() {
		return emf;
	}

	/**
	 * Method that sets entity manager factory.
	 * 
	 * @param emf
	 *            - entity manager factory which is set
	 */
	public static void setEmf(EntityManagerFactory emf) {
		JPAEMFProvider.emf = emf;
	}

}
