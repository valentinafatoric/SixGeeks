package dao;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import model.Absence;
import model.Administrator;
import model.Comment;
import model.Consultation;
import model.EventDiary;
import model.Grade;
import model.Notification;
import model.Parents;
import model.Professor;
import model.Room;
import model.School;
import model.Student;
import model.StudentClass;
import model.Subject;
import model.SubjectGradesForStudent;
import model.SubjectInfoForProf;
import model.TermOfLecture;
import model.User;

public interface DAO {
	
	User getUser(String username);

	void addOrUpdateAbsence(Absence absence);
	void deleteAbsence(Absence absence);
	Absence getAbsence(TermOfLecture term, Student student, Date date);
	List<Absence> getAbsencesForDate(TermOfLecture term, Date date);
	List<Absence> getAbsencesForStudent(Student student);
	Map<Student, List<Absence>> getAbsencesForClass(StudentClass studentClass);
	List<Absence> getAbsencesForClass(StudentClass studentClass, Date date, TermOfLecture term);

	void addAdministrator(Administrator admin);

	void addOrUpdateComment(Comment comment);
	void deleteComment(SubjectGradesForStudent subjectGrades, Date date);
	List<Comment> getCommentsForSubject(SubjectGradesForStudent subjectGrades);
	List<Comment> getCommentsForSubject(Student student, Subject subject);

	void addOrUpdateConsultation(Consultation consultation);
	void deleteConsultation(Consultation consultation);
	List<Consultation> getConsultations(StudentClass studentClass);
	List<Consultation> getConsultations(StudentClass classOfStudent, boolean parentMeeting);

	void addEvent(EventDiary eventDiary);
	List<EventDiary> getEventDiaryForSchool(School school);
	
	void addOrUpdateGrade(Grade grade);
	void deleteGrade(SubjectGradesForStudent subjectGrades, Date date);

	void addOrUpdateNotification(Notification notification);
	void deleteNotification(Notification notification);
	Notification getNotificationById(int id);
	List<Notification> getCurrentNotifications();
	List<Notification> getAllNotificationsForSchool(School school);

	void addOrUpdateParents(Parents parents);
	void addProfessor(Professor professor);
	void updateProfessor(Professor professor);
	void deleteProfessor(Professor professor);
	List<Professor> getProfessors();
	List<Professor> getProfessorByOIB(String OIB);

	void addRoom(Room room);
	Room getRoom(String room);
	Room getRoomById(int id);
	List<Room> getRoomsInSchool(School school);

	void addSchool(School school);
	School getSchool(School school);
	List<School> getSchools();

	void addOrUpdateStudent(Student student);
	Student getStudent(String username);
	Student getStudentByOIB(String OIB);
	Student getStudent(Student student);
	List<Student> getStudents();

	void addStudentClass(StudentClass studentClass);
	void updateStudentClass(StudentClass studentClass);
	void deleteStudentClass(StudentClass studentClass);
	StudentClass getStudentClass(StudentClass studentClass);
	StudentClass getStudentClass(int id);
	StudentClass getStudentClass(String className, String schoolYear);
	StudentClass getClassOfDeputyProfessor(Professor professor);
	List<StudentClass> getClasses();
	List<StudentClass> getClassesForSchool(School school);
	Set<StudentClass> getClassesForProfessorSubject(SubjectInfoForProf subInfoForProfessor);

	void addSubject(Subject subject);
	void updateSubject(Subject subject);
	void deleteSubject(Subject subject);
	Subject getSubject(int id);
	List<Subject> getSubjects();

	void addOrUpdateSubjectGradesForStudent(SubjectGradesForStudent subjectGradesForStudent);
	void deleteSubjectGradeForStudent(SubjectGradesForStudent subjectGradesForStudent);
	SubjectGradesForStudent getSubjectGradesForStudent(Student student, Subject subject, String schoolYear);
	List<SubjectGradesForStudent> getCurrentlyActiveSubjectsForStudent(Student student);
	List<SubjectGradesForStudent> getSubjectsOfSchoolYearForStudent(Student student, String schoolYear);

	void addOrUpdateSubjectInfoForProf(SubjectInfoForProf subjectInfoForProf);
	void deleteSubjectInfoForProf(SubjectInfoForProf subjectInfoForProf);
	List<SubjectInfoForProf> getSubjectInfoForProfessor(Professor professor, String schoolYear);
	List<SubjectInfoForProf> getProfsOfSubjectsInYearAndSchool(School school, String currentSchoolYear);
	SubjectInfoForProf getSubjectInfoForProfByProf(Professor prof);

	void addOrUpdateTermOfLecture(TermOfLecture term);
	void deleteTermById(int id);
	TermOfLecture getTerm(int id);
	TermOfLecture getTerm(Professor professor, StudentClass studentClass, Subject subject, Room room, int dayOfWeek, int hour);
	List<TermOfLecture> getTermsForSubject(Subject subject, StudentClass studentClass);
	List<TermOfLecture> getLecturesForProfessorAtTime(Professor professor, int dayOfWeek, int hourOfDay);
	List<TermOfLecture> getLectureForClassAt(StudentClass studentClass, int dayOfWeek, int hourOfDay);
	List<TermOfLecture> getLecturesAtTerm(int dayOfWeek, int hourOfDay);
	Map<String, Map<String, TermOfLecture>> getTermOfClass(StudentClass studentClass);
	
	Map<String, Map<String, TermOfLecture>> getSchedule(User user);

	int getNextTermId();
}
