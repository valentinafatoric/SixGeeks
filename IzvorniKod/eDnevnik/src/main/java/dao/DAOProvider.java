package dao;

import dao.jpa.JPADAOImpl;

public class DAOProvider {

	/** Provider of access to data subsystem */
	private static DAO dao = new JPADAOImpl();
	
	/**
	 * Method which returns encapsulated provider of access to data subsystem.
	 * @return encapsulated provider of access to data subsystem
	 */
	public static DAO getDAO() {
		return dao;
	}
	
}
