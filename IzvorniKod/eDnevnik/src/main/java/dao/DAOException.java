package dao;

public class DAOException extends RuntimeException {

	/** Serial version UID */
	private static final long serialVersionUID = 1L;

	/**
	 * Default constructor without detailed message.
	 */
	public DAOException() {
	}

	/**
	 * Constructs with the detail message, cause, suppression enabled or disabled, and writable stack 
	 * trace enabled or disabled.
	 * 
	 * @param message - detailed message
	 * @param cause - cause
	 * @param enableSuppression - whether or not suppression is enabled or disabled
	 * @param writableStackTrace - whether or not the stack trace should be writable
	 */
	public DAOException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	/**
	 * Constructor with detailed message and cause.
	 * @param message - detailed message
	 * @param cause - cause
	 */
	public DAOException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Constructor with detailed message.
	 * @param message - detailed message
	 */
	public DAOException(String message) {
		super(message);
	}

	/**
	 * Constructor with cause.
	 * @param cause - cause
	 */
	public DAOException(Throwable cause) {
		super(cause);
	}
	
}
