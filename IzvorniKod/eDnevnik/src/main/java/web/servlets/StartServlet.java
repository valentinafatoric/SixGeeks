package web.servlets;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DAOException;
import dao.DAOProvider;
import dao.jpa.JPAEMProvider;
import model.Absence;
import model.Administrator;
import model.Comment;
import model.Consultation;
import model.Grade;
import model.Notification;
import model.Parents;
import model.Professor;
import model.Room;
import model.School;
import model.Student;
import model.StudentClass;
import model.Subject;
import model.SubjectGradesForStudent;
import model.SubjectInfoForProf;
import model.TermOfLecture;

@WebServlet(name = "startServlet", urlPatterns = { "", "/", "/servlets/homepage", "/servlets/index", "/index.jsp" })
public class StartServlet extends HttpServlet {

	/** Serial version UID */
	private static final long serialVersionUID = 1L;

	private static List<School> schools = new LinkedList<>();

	private static List<Student> students = new LinkedList<>();

	private static List<Professor> professors = new LinkedList<>();

	private static List<Administrator> administrators = new LinkedList<>();

	private static List<StudentClass> classes = new LinkedList<>();

	private static List<Subject> subjects = new LinkedList<>();

	private static List<SubjectGradesForStudent> subjectGrades = new LinkedList<>();

	private static List<TermOfLecture> terms = new LinkedList<>();

	private static List<Room> rooms = new LinkedList<>();

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		if (DAOProvider.getDAO().getSchools().isEmpty()) {
			intializeBase(req);
		}
		
		UtilServlets.initialize(req);

		if (req.getSession().getAttribute("user") == null) {
			req.getRequestDispatcher("/WEB-INF/pages/Home.jsp").forward(req, resp);
		} else {
			resp.sendRedirect(req.getServletContext().getContextPath() + "/servlets/main");
		}
	}

	private void intializeBase(HttpServletRequest req) throws IOException {
		initializeSchools(req);
		initializeRooms(req);
		initializeAdministrators(req);
		initializeNotifications(req);
		initializeProfessors(req);
		initializeStudentClasses(req);
		initializeStudents(req);
		initializeParents(req);
		initializeSubjects(req);
		initializeTerms(req);
		initializeSubjectInfos(req);
		initializeConsultations(req);
		initializeAbsences(req);
		initializeSubjectGrades(req);
		initilaizeGrades(req);
		initializeComments(req);
	}

	private void initializeSchools(HttpServletRequest req) throws IOException {
		List<String> lines = Files.readAllLines(
				Paths.get(req.getServletContext().getRealPath("/WEB-INF/base/schools.txt")), StandardCharsets.UTF_8);

		for (String line : lines) {
			if (line.trim().isEmpty()) {
				break;
			}

			String[] parts = line.split("\\|");

			School school = new School();
			school.setName(parts[1]);
			school.setAddress(parts[2]);

			DAOProvider.getDAO().addSchool(school);
			schools.add(school);
		}
		
		JPAEMProvider.close();
	}

	private void initializeRooms(HttpServletRequest req) throws IOException {
		List<String> lines = Files.readAllLines(
				Paths.get(req.getServletContext().getRealPath("/WEB-INF/base/rooms.txt")), StandardCharsets.UTF_8);

		for (String line : lines) {
			if (line.trim().isEmpty()) {
				break;
			}

			String[] parts = line.split("\\|");

			Room room = new Room();
			room.setName(parts[2]);
			room.setSchool(schools.get(Integer.valueOf(parts[3])));

			DAOProvider.getDAO().addRoom(room);
			rooms.add(room);
		}
		
		JPAEMProvider.close();
	}

	private void initializeAdministrators(HttpServletRequest req) throws IOException {
		List<String> lines = Files.readAllLines(
				Paths.get(req.getServletContext().getRealPath("/WEB-INF/base/administrators.txt")),
				StandardCharsets.UTF_8);

		for (String line : lines) {
			if (line.trim().isEmpty()) {
				break;
			}

			String[] parts = line.split("\\|");

			Administrator admin = new Administrator();
			admin.setUsername(parts[1]);
			admin.setPassword(parts[2]);
			admin.setName(parts[3]);
			admin.setSurname(parts[4]);
			admin.setOIB(parts[5]);
			admin.setSchool(schools.get(Integer.valueOf(parts[6])));

			DAOProvider.getDAO().addAdministrator(admin);
			administrators.add(admin);
		}
		
		JPAEMProvider.close();
	}

	private void initializeNotifications(HttpServletRequest req) throws IOException {
		List<String> lines = Files.readAllLines(
				Paths.get(req.getServletContext().getRealPath("/WEB-INF/base/notifications.txt")),
				StandardCharsets.UTF_8);

		for (String line : lines) {
			if (line.trim().isEmpty()) {
				break;
			}

			String[] parts = line.split("\\|");

			Notification notification = new Notification();
			notification.setTitle(parts[1]);
			notification.setCreator(administrators.get(Integer.valueOf(parts[2])));
			notification.setDate(parseDate(parts[3]));
			notification.setText(parts[4]);
			notification.setArchived(parts[6].trim().equals("true"));

			DAOProvider.getDAO().addOrUpdateNotification(notification);
		}
		
		JPAEMProvider.close();
	}

	private void initializeProfessors(HttpServletRequest req) throws IOException {
		List<String> lines = Files.readAllLines(
				Paths.get(req.getServletContext().getRealPath("/WEB-INF/base/professors.txt")), StandardCharsets.UTF_8);

		for (String line : lines) {
			if (line.trim().isEmpty()) {
				break;
			}

			String[] parts = line.split("\\|");

			Professor professor = new Professor();
			professor.setUsername(parts[1]);
			professor.setPassword(parts[2]);
			professor.setName(parts[3]);
			professor.setSurname(parts[4]);
			professor.setOIB(parts[5]);
			professor.setPhoneNumber(parts[6]);
			professor.setPicture("http://cdn2.iconfinder.com/data/icons/education-4-1/256/Teacher-512.png");
			professor.setSchool(schools.get(Integer.valueOf(parts[7])));

			DAOProvider.getDAO().addProfessor(professor);
			professors.add(professor);
		}
		
		JPAEMProvider.close();
	}

	private void initializeStudentClasses(HttpServletRequest req) throws IOException {
		List<String> lines = Files.readAllLines(
				Paths.get(req.getServletContext().getRealPath("/WEB-INF/base/classes.txt")), StandardCharsets.UTF_8);

		for (String line : lines) {
			if (line.trim().isEmpty()) {
				break;
			}

			String[] parts = line.split("\\|");

			StudentClass studentClass = new StudentClass();
			studentClass.setName(parts[2]);
			studentClass.setSchoolYear(parts[3]);
			studentClass.setProfessor(professors.get(Integer.parseInt(parts[5])));
			studentClass.setDeputyProfessor(professors.get(Integer.parseInt(parts[6])));
			studentClass.setSchool(schools.get(Integer.valueOf(parts[7])));

			DAOProvider.getDAO().addStudentClass(studentClass);
			classes.add(studentClass);
		}
		
		JPAEMProvider.close();
	}

	private void initializeStudents(HttpServletRequest req) throws IOException {
		List<String> lines = Files.readAllLines(
				Paths.get(req.getServletContext().getRealPath("/WEB-INF/base/students.txt")), StandardCharsets.UTF_8);

		for (String line : lines) {
			if (line.trim().isEmpty()) {
				break;
			}

			String[] parts = line.split("\\|");

			Student student = new Student();
			student.setUsername(parts[1]);
			student.setPassword(parts[2]);
			student.setName(parts[3]);
			student.setSurname(parts[4]);
			student.setSex(parts[5]);
			student.setOIB(parts[6]);
			student.setDateOfBirth(parseDate(parts[7]));
			student.setPlaceOfBirth(parts[8]);
			student.setPicture("http://cdn0.iconfinder.com/data/icons/student-2/100/student-1-512.png");
			student.setCurrentStatus(parts[10]);
			student.setSchool(schools.get(Integer.valueOf(parts[12])));
			student.setClassOfStudent(classes.get(Integer.valueOf(parts[13])));

			DAOProvider.getDAO().addOrUpdateStudent(student);

			classes.get(Integer.valueOf(parts[13])).getStudents().add(student);
			DAOProvider.getDAO().updateStudentClass(classes.get(Integer.valueOf(parts[13])));
			students.add(student);
		}
		
		JPAEMProvider.close();
	}

	private void initializeParents(HttpServletRequest req) throws IOException {
		List<String> lines = Files.readAllLines(
				Paths.get(req.getServletContext().getRealPath("/WEB-INF/base/parents.txt")), StandardCharsets.UTF_8);

		for (String line : lines) {
			if (line.trim().isEmpty()) {
				break;
			}

			String[] parts = line.split("\\|");

			Parents parents = new Parents();
			parents.setUsername(parts[1]);
			parents.setPassword(parts[2]);
			parents.setChild(students.get(Integer.parseInt(parts[3])));
			parents.setMothersName(parts[4]);
			parents.setFathersName(parts[5]);

			DAOProvider.getDAO().addOrUpdateParents(parents);

			students.get(Integer.parseInt(parts[3])).setParents(parents);
			DAOProvider.getDAO().addOrUpdateStudent(students.get(Integer.parseInt(parts[3])));
		}
		
		JPAEMProvider.close();
	}

	private void initializeSubjects(HttpServletRequest req) throws IOException {
		List<String> lines = Files.readAllLines(
				Paths.get(req.getServletContext().getRealPath("/WEB-INF/base/subjects.txt")), StandardCharsets.UTF_8);

		for (String line : lines) {
			if (line.trim().isEmpty()) {
				break;
			}

			String[] parts = line.split("\\|");

			Subject subject = new Subject();
			subject.setName(parts[2]);
			subject.setDescription(parts[3]);

			DAOProvider.getDAO().addSubject(subject);
			subjects.add(subject);
		}
		
		JPAEMProvider.close();
	}

	private void initializeConsultations(HttpServletRequest req) throws IOException {
		List<String> lines = Files.readAllLines(
				Paths.get(req.getServletContext().getRealPath("/WEB-INF/base/consultations.txt")),
				StandardCharsets.UTF_8);

		for (String line : lines) {
			if (line.trim().isEmpty()) {
				break;
			}

			String[] parts = line.split("\\|");

			Consultation consultation = new Consultation();
			consultation.setStudentClass(classes.get(Integer.valueOf(parts[1])));
			consultation.setDateAndTime(parseDate(parts[2]));
			consultation.setRoom(rooms.get(Integer.valueOf(parts[3])));
			consultation.setParentMeeting(Boolean.parseBoolean(parts[4]));

			DAOProvider.getDAO().addOrUpdateConsultation(consultation);
		}
		
		JPAEMProvider.close();
	}

	private void initializeSubjectGrades(HttpServletRequest req) throws IOException {
		List<String> lines = Files.readAllLines(
				Paths.get(req.getServletContext().getRealPath("/WEB-INF/base/subjectGrades.txt")),
				StandardCharsets.UTF_8);

		for (String line : lines) {
			if (line.trim().isEmpty()) {
				break;
			}

			String[] parts = line.split("\\|");

			SubjectGradesForStudent subjectGradesForStudent = new SubjectGradesForStudent();
			subjectGradesForStudent.setStudent(students.get(Integer.valueOf(parts[1])));
			subjectGradesForStudent.setSubject(subjects.get(Integer.valueOf(parts[2])));
			subjectGradesForStudent.setSchoolYear(parts[3]);
			subjectGradesForStudent.setCurrentlyActive(Boolean.valueOf(parts[4]));

			if (parts[5].equals("/")) {
				subjectGradesForStudent.setFinalGrade(0);
			} else {
				subjectGradesForStudent.setFinalGrade(Integer.valueOf(parts[5]));
			}

			DAOProvider.getDAO().addOrUpdateSubjectGradesForStudent(subjectGradesForStudent);
			subjectGrades.add(subjectGradesForStudent);
		}
		
		JPAEMProvider.close();
	}

	private void initilaizeGrades(HttpServletRequest req) throws IOException {
		List<String> lines = Files.readAllLines(
				Paths.get(req.getServletContext().getRealPath("/WEB-INF/base/grades.txt")), StandardCharsets.UTF_8);
		int counter = -1;

		for (String line : lines) {
			if (line.trim().isEmpty()) {
				break;
			}

			String[] parts = line.split("\\|");

			if (lines.indexOf(line) % 3 == 0) {
				counter++;
			}

			Grade grade = new Grade();
			grade.setStudentSubject(subjectGrades.get(counter));
			grade.setDate(parseDate(parts[3]));
			grade.setValue(Integer.parseInt(parts[4]));
			grade.setComponent(parts[5]);

			DAOProvider.getDAO().addOrUpdateGrade(grade);
			subjectGrades.get(counter).getGrades().add(grade);
			DAOProvider.getDAO().addOrUpdateSubjectGradesForStudent(subjectGrades.get(counter));
		}
		
		JPAEMProvider.close();
	}

	private void initializeComments(HttpServletRequest req) throws IOException {
		List<String> lines = Files.readAllLines(
				Paths.get(req.getServletContext().getRealPath("/WEB-INF/base/comments.txt")), StandardCharsets.UTF_8);

		for (String line : lines) {
			if (line.trim().isEmpty()) {
				break;
			}

			String[] parts = line.split("\\|");

			Comment comment = new Comment();
			comment.setStudentSubject(subjectGrades.get(Integer.valueOf(parts[0])));
			comment.setDate(parseDate(parts[3]));
			comment.setText(parts[4]);

			DAOProvider.getDAO().addOrUpdateComment(comment);
			subjectGrades.get(Integer.valueOf(parts[0])).getComments().add(comment);
			DAOProvider.getDAO().addOrUpdateSubjectGradesForStudent(subjectGrades.get(Integer.valueOf(parts[0])));
		}
		
		JPAEMProvider.close();
	}

	private void initializeSubjectInfos(HttpServletRequest req) throws IOException {
		List<String> lines = Files.readAllLines(
				Paths.get(req.getServletContext().getRealPath("/WEB-INF/base/subjectsInfos.txt")),
				StandardCharsets.UTF_8);

		for (String line : lines) {
			if (line.trim().isEmpty()) {
				break;
			}

			String[] parts = line.split("\\|");

			SubjectInfoForProf subjectInfo = new SubjectInfoForProf();
			subjectInfo.setProfessor(professors.get(Integer.valueOf(parts[1])));
			subjectInfo.setSubject(subjects.get(Integer.valueOf(parts[2])));
			subjectInfo.setSchoolYear(parts[3]);
			subjectInfo.setCurriculum("http://www.azoo.hr/images/stories/dokumenti/Nacionalni_okvirni_kurikulum.pdf");
			subjectInfo.setTeachingLetter("http://dokumenti.ncvvo.hr/Nastavni_plan/pmg/etika.pdf");

			DAOProvider.getDAO().addOrUpdateSubjectInfoForProf(subjectInfo);
		}
		
		JPAEMProvider.close();
	}

	private void initializeTerms(HttpServletRequest req) throws IOException {
		List<String> lines = Files.readAllLines(
				Paths.get(req.getServletContext().getRealPath("/WEB-INF/base/terms.txt")), StandardCharsets.UTF_8);

		for (String line : lines) {
			if (line.trim().isEmpty()) {
				break;
			}

			String[] parts = line.split("\\|");

			TermOfLecture term = new TermOfLecture();
			term.setId(DAOProvider.getDAO().getNextTermId());
			term.setProfessor(professors.get(Integer.valueOf(parts[2])));
			term.setStudentClass(classes.get(Integer.valueOf(parts[3])));
			term.setSubject(subjects.get(Integer.valueOf(parts[4])));
			term.setRoom(rooms.get(Integer.valueOf(parts[5])));
			term.setDayOfWeek(Integer.parseInt(parts[6]));
			term.setOrdinalNumberOfHour(Integer.parseInt(parts[7]));

			DAOProvider.getDAO().addOrUpdateTermOfLecture(term);
			terms.add(term);
		}
		
		JPAEMProvider.close();
	}

	private void initializeAbsences(HttpServletRequest req) throws IOException {
		List<String> lines = Files.readAllLines(
				Paths.get(req.getServletContext().getRealPath("/WEB-INF/base/absences.txt")), StandardCharsets.UTF_8);

		for (String line : lines) {
			if (line.trim().isEmpty()) {
				break;
			}

			String[] parts = line.split("\\|");

			Absence absence = new Absence();
			absence.setStudent(students.get(Integer.valueOf(parts[1])));
			absence.setTerm(terms.get(Integer.valueOf(parts[2])));
			absence.setDate(parseDate(parts[3]));
			absence.setJustifiably(Boolean.parseBoolean(parts[4]));
			absence.setComment(parts[5]);

			DAOProvider.getDAO().addOrUpdateAbsence(absence);
		}
		
		JPAEMProvider.close();
	}

	private Date parseDate(String date) {
		DateFormat format = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy", Locale.ENGLISH);
		
		try {
			return format.parse(date);
		} catch (ParseException e) {
			throw new DAOException("Greška prilikom parsiranja datuma!");
		}
	}

}
