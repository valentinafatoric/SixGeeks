package web.servlets;

import java.io.File;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

public class UtilServlets {
	
	public static String pathToBase;
	
	public static ServletFileUpload uploader;
	
	public static void initialize(HttpServletRequest req) {
		pathToBase = req.getServletContext().getRealPath("/WEB-INF/base");

		DiskFileItemFactory fileFactory = new DiskFileItemFactory();
		File filesDir = new File(pathToBase);
		fileFactory.setRepository(filesDir);
		uploader = new ServletFileUpload(fileFactory);
	}

	public static String getCurrentSchoolYear() {
		Calendar c = Calendar.getInstance();
		int currentYear = c.get(Calendar.YEAR);

		if (c.get(Calendar.MONTH) < 9) {
			currentYear--;
		}

		String schoolYear = String.valueOf(currentYear) + "./" + String.valueOf(currentYear + 1) + ".";

		return schoolYear;
	}

	public static int getDayOfWeekFromDate(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);

		int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK) - 1;

		if (dayOfWeek == 0) {
			dayOfWeek = 7;
		}

		return dayOfWeek;
	}
}
