package web.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DAOProvider;
import model.Administrator;
import model.Parents;
import model.Professor;
import model.Student;
import model.User;

@WebServlet(name = "mainServlet", urlPatterns = { "/servlets/main" })
public class MainServlet extends HttpServlet {

	/** Serial version UID */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		User user = (User) req.getSession().getAttribute("user");

		if (user == null) {
			resp.sendRedirect(req.getServletContext().getContextPath());
		}

		if (user instanceof Student) {
			req.getSession().setAttribute("student", user);
			resp.sendRedirect(req.getServletContext().getContextPath() + "/servlets/studentmain");
		} else if (user instanceof Parents) {
			req.getSession().setAttribute("parents", user);
			req.getSession().setAttribute("student",
					DAOProvider.getDAO().getStudent(((Parents) user).getChild()));
			resp.sendRedirect(req.getServletContext().getContextPath() + "/servlets/studentmain");
		} else if (user instanceof Professor) {
			req.getSession().setAttribute("professor", user);
			resp.sendRedirect(req.getServletContext().getContextPath() + "/servlets/professormain");
		} else if (user instanceof Administrator) {
			req.getSession().setAttribute("administrator", user);
			resp.sendRedirect(req.getServletContext().getContextPath() + "/servlets/adminmain");
		}
	}

}
