package web.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DAOException;
import dao.DAOProvider;
import model.User;
import web.forms.LoginForm;

@WebServlet(name = "loginServlet", urlPatterns = { "/servlets/login" })
public class LogInServlet extends HttpServlet {

	/** Serial version UID */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		process(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		process(req, resp);
	}

	private void process(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException, DAOException {
		LoginForm f = new LoginForm();
		f.getFromHttpRequest(req);
		f.validate();

		if (f.hasErrors()) {
			req.setAttribute("loginform", f);
			req.getRequestDispatcher("/WEB-INF/pages/Home.jsp").forward(req, resp);
			return;
		}

		User user = DAOProvider.getDAO().getUser(f.getUsername());

		req.getSession().setAttribute("user", user);

		resp.sendRedirect(req.getServletContext().getContextPath());
	}

}
