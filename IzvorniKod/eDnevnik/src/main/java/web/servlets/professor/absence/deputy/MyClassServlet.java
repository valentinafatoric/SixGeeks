package web.servlets.professor.absence.deputy;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DAOProvider;
import model.Professor;
import model.StudentClass;

@WebServlet(name = "myClassServlet", urlPatterns = { "/servlets/myclass" })
public class MyClassServlet extends HttpServlet {

	/** Serial version UID */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Professor professor = (Professor) req.getSession().getAttribute("professor");

		if (professor == null) {
			req.setAttribute("message", "Nedopušteni pristup!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}
		
		StudentClass myClass = DAOProvider.getDAO().getClassOfDeputyProfessor(professor);

		if (myClass == null) {
			req.setAttribute("message", "Ne postoji razred od profesora " + professor.getName());
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}
		
		req.setAttribute("myclass", myClass);

		req.getRequestDispatcher("/WEB-INF/pages/professor/MyClass.jsp").forward(req, resp);
	}
}
