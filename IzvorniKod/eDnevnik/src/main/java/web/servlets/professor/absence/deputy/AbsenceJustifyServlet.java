package web.servlets.professor.absence.deputy;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DAOProvider;
import model.Absence;
import model.Professor;
import model.Student;
import model.StudentClass;

@WebServlet(name = "absenceJustifyServlet", urlPatterns = { "/servlets/absencejustify" })
public class AbsenceJustifyServlet extends HttpServlet {

	/** Serial version UID */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Professor professor = (Professor) req.getSession().getAttribute("professor");

		if (professor == null) {
			req.setAttribute("message", "Nedopušteni pristup!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}

		StudentClass myClass = DAOProvider.getDAO().getClassOfDeputyProfessor(professor);
		Map<Student, List<Absence>> absencesBefore = DAOProvider.getDAO().getAbsencesForClass(myClass);
		
		if (myClass == null || absencesBefore == null) {
			req.setAttribute("message", "Neispravan zahtjev!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}

		Map<Student, List<Absence>> absences = new HashMap<>();

		for (Entry<Student, List<Absence>> a : absencesBefore.entrySet()) {
			Student s = a.getKey();
			List<Absence> list = new LinkedList<>();
			
			for (Absence ab : a.getValue()) {
				if (!ab.isJustifiably()) {
					list.add(ab);
				}
			}
			
			absences.put(s, list);
		}

		req.setAttribute("absences", absences);
		req.getRequestDispatcher("/WEB-INF/pages/professor/AbsenceJustify.jsp").forward(req, resp);
	}

}
