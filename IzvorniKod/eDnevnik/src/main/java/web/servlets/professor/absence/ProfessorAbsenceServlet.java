package web.servlets.professor.absence;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DAOProvider;
import model.Absence;
import model.Professor;
import model.Room;
import model.StudentClass;
import model.Subject;
import model.SubjectInfoForProf;
import model.TermOfLecture;
import web.servlets.UtilServlets;

@WebServlet(name = "professorAbsenceServlet", urlPatterns = { "/servlets/professorabsence" })
public class ProfessorAbsenceServlet extends HttpServlet {

	/** Serial version UID */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Professor professor = (Professor) req.getSession().getAttribute("professor");

		if (professor == null) {
			req.setAttribute("message", "Nedopušteni pristup!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}

		String schoolYear = UtilServlets.getCurrentSchoolYear();

		List<SubjectInfoForProf> subjects = DAOProvider.getDAO().getSubjectInfoForProfessor(professor, schoolYear);
		Set<StudentClass> profClasses = new HashSet<>();

		for (SubjectInfoForProf sub : subjects) {
			profClasses.addAll(DAOProvider.getDAO().getClassesForProfessorSubject(sub));
		}

		req.setAttribute("subjectsInfos", subjects);
		req.setAttribute("profClasses", profClasses);

		req.getRequestDispatcher("/WEB-INF/pages/professor/ProfessorAbsence.jsp").forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Professor professor = (Professor) req.getSession().getAttribute("professor");

		if (professor == null) {
			req.setAttribute("message", "Nedopušteni pristup!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}

		String subjectString = req.getParameter("subject");
		String studentClassString = req.getParameter("studentClass");
		String dateString = req.getParameter("date");
		String hourString = req.getParameter("hour");
		String roomString = req.getParameter("room");

		if (subjectString == null) {
			req.setAttribute("message", "predmet");
			req.getRequestDispatcher("/WEB-INF/pages/ErrorInsert.jsp").forward(req, resp);
			return;
		} else if (studentClassString == null) {
			req.setAttribute("message", "razred");
			req.getRequestDispatcher("/WEB-INF/pages/ErrorInsert.jsp").forward(req, resp);
			return;
		} else if (dateString == null) {
			req.setAttribute("message", "datum");
			req.getRequestDispatcher("/WEB-INF/pages/ErrorInsert.jsp").forward(req, resp);
			return;
		} else if (roomString == null) {
			req.setAttribute("message", "učionica");
			req.getRequestDispatcher("/WEB-INF/pages/ErrorInsert.jsp").forward(req, resp);
			return;
		}

		Subject subject = null;

		for (Subject sub : DAOProvider.getDAO().getSubjects()) {
			if (sub.getName().equals(subjectString)) {
				subject = sub;
			}
		}

		if (subject == null) {
			req.setAttribute("message", "Ne postoji predmet s odabranim imenom!");
			doGet(req, resp);
			return;
		}

		String schoolYear = UtilServlets.getCurrentSchoolYear();

		StudentClass studentClass = DAOProvider.getDAO().getStudentClass(studentClassString, schoolYear);

		Room room = DAOProvider.getDAO().getRoom(roomString.trim());

		if (room == null) {
			req.setAttribute("message", "Ne postoji upisana učionica!");
			doGet(req, resp);
			return;
		}

		DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);

		Date date = null;
		try {
			date = sdf.parse(dateString);
		} catch (ParseException e) {
			req.setAttribute("message", "Neispravan datum!");
			doGet(req, resp);
			return;
		}

		int dayOfWeek = UtilServlets.getDayOfWeekFromDate(date);

		int hour = Integer.parseInt(hourString);

		TermOfLecture term = DAOProvider.getDAO().getTerm(professor, studentClass, subject, room, dayOfWeek, hour);

		if (term == null) {
			req.setAttribute("message", "Ne postojeći termin!");
			doGet(req, resp);
			return;
		}

		List<Absence> absences = DAOProvider.getDAO().getAbsencesForDate(term, date);
		req.setAttribute("studentClass", studentClass);
		req.setAttribute("absences", absences);

		resp.sendRedirect(req.getServletContext().getContextPath() + "/servlets/professorabsencesearch/" + term.getId()
				+ "+" + sdf.format(date) + "+" + studentClass.getId());
	}

}
