package web.servlets.professor.absence.deputy;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DAOProvider;
import model.Absence;
import model.Professor;
import model.Student;
import model.StudentClass;
import model.TermOfLecture;

@WebServlet(name = "absenceJustifyCommentServlet", urlPatterns = { "/servlets/absencejustifycomment/*" })
public class AbsenceJustifyCommentServlet extends HttpServlet {

	/** Serial version UID */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Professor professor = (Professor) req.getSession().getAttribute("professor");

		if (professor == null) {
			req.setAttribute("message", "Nedopušteni pristup!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}

		StudentClass myClass = DAOProvider.getDAO().getClassOfDeputyProfessor(professor);
		Map<Student, List<Absence>> absences = DAOProvider.getDAO().getAbsencesForClass(myClass);

		if(myClass == null || absences == null) {
			req.setAttribute("message", "Neispravan zahtjev!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}

		String pathInfo = req.getPathInfo().substring(1);
		String parts[] = pathInfo.split("\\+");

		if (parts.length != 3) {
			req.setAttribute("message", "Neispravan zahtjev!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}
		
		try {
			Integer.parseInt(parts[1]);
		} catch (NumberFormatException ex) {
			req.setAttribute("message", "Neispravan zahtjev!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}

		Student student = DAOProvider.getDAO().getStudentByOIB(parts[0]);
		TermOfLecture term = DAOProvider.getDAO().getTerm(Integer.parseInt(parts[1]));

		if (student == null || term == null) {
			req.setAttribute("message", "Neispravan zahtjev!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}

		DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
		Date date = null;

		try {
			date = format.parse(parts[2]);
		} catch (ParseException e) {
			req.setAttribute("message", "Neispravan zahtjev!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}

		req.setAttribute("absence", DAOProvider.getDAO().getAbsence(term, student, date));
		req.setAttribute("pathInfo", pathInfo);
		req.setAttribute("absences", absences);

		req.getRequestDispatcher("/WEB-INF/pages/professor/AbsenceJustifyComment.jsp").forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		Professor professor = (Professor) req.getSession().getAttribute("professor");
		String pathInfo = req.getPathInfo().substring(1);
		String parts[] = pathInfo.split("\\+");

		if (professor == null) {
			req.setAttribute("message", "Nedopušteni pristup!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		} else if (parts.length != 3) {
			req.setAttribute("message", "Neispravan zahtjev!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}

		try {
			Integer.parseInt(parts[1]);
		} catch (NumberFormatException ex) {
			req.setAttribute("message", "Neispravan zahtjev!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}

		Student student = DAOProvider.getDAO().getStudentByOIB(parts[0]);
		TermOfLecture term = DAOProvider.getDAO().getTerm(Integer.parseInt(parts[1]));

		if (student == null || term == null) {
			req.setAttribute("message", "Neispravan zahtjev!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}

		DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
		
		Date date = null;
		try {
			date = format.parse(parts[2]);
		} catch (ParseException e) {
			req.setAttribute("message", "Neispravan zahtjev!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}

		String comment = req.getParameter("comment");
		String justify = req.getParameter("justify").trim().toLowerCase();

		if(comment==null || comment.trim().equals("") || comment.trim().isEmpty()){
			req.setAttribute("message", "Niste unijeli komentar, opravdanje neće biti uvaženo!");
			req.setAttribute("pathInfo", pathInfo);
			req.getRequestDispatcher("/WEB-INF/pages/professor/AbsenceJustifyComment.jsp").forward(req, resp);
			return;
		}
		
		Absence absence = new Absence();
		absence.setComment(comment);
		absence.setDate(date);
		absence.setTerm(term);

		if (justify.equals("opravdan")) {
			absence.setJustifiably(true);
		} else {
			absence.setJustifiably(false);
		}
		
		absence.setStudent(student);

		try {
			DAOProvider.getDAO().addOrUpdateAbsence(absence);
		} catch (Exception e) {
			req.setAttribute("message", "Nemoguće opravdanje!");
			req.getRequestDispatcher("/WEB-INF/pages/ErrorInsert.jsp").forward(req, resp);
			return;
		}

		resp.sendRedirect(req.getServletContext().getContextPath() + "/servlets/absencejustify");
	}
}
