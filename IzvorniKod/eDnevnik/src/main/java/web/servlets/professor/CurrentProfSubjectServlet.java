package web.servlets.professor;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DAOProvider;
import model.Professor;
import model.StudentClass;
import model.SubjectInfoForProf;
import web.servlets.UtilServlets;

@WebServlet(name = "currentprofessorSubjectsServlet", urlPatterns = { "/servlets/currentprofsubject/*" })
public class CurrentProfSubjectServlet extends HttpServlet {

	/** Serial version UID */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		Professor professor = (Professor) req.getSession().getAttribute("professor");

		if (professor == null) {
			req.setAttribute("message", "Nedopušteni pristup!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}

		String schoolYear = UtilServlets.getCurrentSchoolYear();

		List<SubjectInfoForProf> subjects = DAOProvider.getDAO().getSubjectInfoForProfessor(professor, schoolYear);

		String pathInfo = req.getPathInfo().substring(1);

		SubjectInfoForProf subjectInfo = null;

		for (SubjectInfoForProf sub : subjects) {
			if (pathInfo.equals(sub.getSubject().getName())) {
				subjectInfo = sub;
				break;
			}
		}

		Set<StudentClass> classes = DAOProvider.getDAO().getClassesForProfessorSubject(subjectInfo);

		String classYearsString = "";

		Set<String> classYears = new HashSet<>();

		for (StudentClass cl : classes) {
			classYears.add(cl.getName().substring(0, 1));
		}

		for (String classYear : classYears) {
			classYearsString += classYear + ", ";
		}

		classYearsString = classYearsString.substring(0, classYearsString.length() - 2);

		req.setAttribute("subjectInfo", subjectInfo);
		req.setAttribute("classYears", classYearsString);
		req.setAttribute("classes", classes);

		req.getRequestDispatcher("/WEB-INF/pages/professor/CurrentProfessorSubjectsDetails.jsp").forward(req, resp);
	}
}
