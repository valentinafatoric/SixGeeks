package web.servlets.professor.archive;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DAOProvider;
import model.Professor;
import model.SubjectInfoForProf;

@WebServlet(name = "archiveMainServlet", urlPatterns = { "/servlets/professorarchive" })
public class ArchiveMainServlet extends HttpServlet {

	/** Serial version UID */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Professor professor = (Professor) req.getSession().getAttribute("professor");

		if (professor == null) {
			req.setAttribute("message", "Nedopušteni pristup!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}

		req.getRequestDispatcher("/WEB-INF/pages/professor/ArchiveMain.jsp").forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Professor professor = (Professor) req.getSession().getAttribute("professor");

		if (professor == null) {
			req.setAttribute("message", "Nedopušteni pristup!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}

		String schoolYear = req.getParameter("schoolYear");
		List<SubjectInfoForProf> subjectInfos = null;

		if (schoolYear==null || schoolYear.length() != 11 || !formatReturn(schoolYear).equals("xxxx./xxxx.")) {
			req.setAttribute("message", "Krivi format unesene godine, trebao bi biti: xxxx./xxxx.!");
			req.getRequestDispatcher("/WEB-INF/pages/professor/ArchiveMain.jsp").forward(req, resp);
			return;
		}

		subjectInfos = DAOProvider.getDAO().getSubjectInfoForProfessor(professor, schoolYear);

		req.setAttribute("subjectInfos", subjectInfos);
		req.setAttribute("schoolYear", schoolYear);

		req.getRequestDispatcher("/WEB-INF/pages/professor/ArchiveSubjects.jsp").forward(req, resp);
	}

	private static String formatReturn(String s) {
		char[] ch = s.toCharArray();
		
		for (int i = 0; i < s.length(); i++) {
			if (ch[i] >= '0' && ch[i] <= '9') {
				ch[i] = 'x';
			}
		}
		
		return String.valueOf(ch);
	}
}
