package web.servlets.professor.archive;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DAOProvider;
import model.Professor;
import model.StudentClass;
import model.Subject;

@WebServlet(name = "archiveClassesServlet", urlPatterns = { "/servlets/archiveprofclasses/*" })
public class ArchiveClassesServlet extends HttpServlet {

	/** Serial version UID */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Professor professor = (Professor) req.getSession().getAttribute("professor");

		if (professor == null) {
			req.setAttribute("message", "Nedopušteni pristup!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}

		String pathInfo = req.getPathInfo().substring(1);

		String[] parts = pathInfo.split("-");

		if (parts.length != 3) {
			req.setAttribute("message", "Neispravan zahtjev!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}

		Subject subject = null;
		for (Subject sub : DAOProvider.getDAO().getSubjects()) {
			if (sub.getName().equals(parts[0])) {
				subject = sub;
			}
		}
		
		try {
			Integer.parseInt(parts[1]);
		} catch (NumberFormatException ex) {
			req.setAttribute("message", "Neispravan zahtjev!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}

		StudentClass studentclass = DAOProvider.getDAO().getStudentClass(Integer.parseInt(parts[1]));

		req.setAttribute("schoolYear", parts[2]);
		req.setAttribute("subject", subject);
		req.setAttribute("studentclass", studentclass);

		req.getRequestDispatcher("/WEB-INF/pages/professor/ArchiveClasses.jsp").forward(req, resp);
	}
}
