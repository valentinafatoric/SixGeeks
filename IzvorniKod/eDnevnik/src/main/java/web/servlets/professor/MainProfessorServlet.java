package web.servlets.professor;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DAOProvider;
import model.Professor;

@WebServlet(name = "mainProfessorServlet", urlPatterns = { "/servlets/professormain" })
public class MainProfessorServlet extends HttpServlet {

	/** Serial version UID */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.getSession().setAttribute("notifications", DAOProvider.getDAO().getCurrentNotifications());

		Professor professor = (Professor) req.getSession().getAttribute("professor");

		if (professor == null) {
			req.setAttribute("message", "Nedopušteni pristup!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}

		req.getSession().setAttribute("deputy", professor.isDeputyProfessor());
		req.getSession().setAttribute("schedule", DAOProvider.getDAO().getSchedule(professor));

		req.getRequestDispatcher("/WEB-INF/pages/professor/ProfessorMain.jsp").forward(req, resp);
	}

}
