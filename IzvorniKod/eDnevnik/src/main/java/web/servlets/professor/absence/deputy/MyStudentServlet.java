package web.servlets.professor.absence.deputy;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DAOProvider;
import model.Professor;
import model.Student;
import model.StudentClass;
import model.SubjectGradesForStudent;

@WebServlet(name = "myStudentServlet", urlPatterns = { "/servlets/mystudent/*" })
public class MyStudentServlet extends HttpServlet {

	/** Serial version UID */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Professor professor = (Professor) req.getSession().getAttribute("professor");

		if (professor == null) {
			req.setAttribute("message", "Nedopušteni pristup!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}

		String pathInfo = req.getPathInfo().substring(1);
		
		String[] parts = pathInfo.split("-");

		if (parts.length != 2) {
			req.setAttribute("message", "Neispravan URL!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}

		String myClassString = parts[0];
		String myStudentString = parts[1];
		
		try {
			Integer.parseInt(myClassString);
		} catch (NumberFormatException ex) {
			req.setAttribute("message", "Neispravan zahtjev!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}

		StudentClass myClass = DAOProvider.getDAO().getStudentClass(Integer.parseInt(myClassString));
		Student myStudent = DAOProvider.getDAO().getStudentByOIB(myStudentString);

		if (myClass == null || myStudent == null) {
			req.setAttribute("message", "Neispravan zahtjev!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}

		List<SubjectGradesForStudent> subjects = DAOProvider.getDAO().getCurrentlyActiveSubjectsForStudent(myStudent);

		req.setAttribute("subjects", subjects);
		req.setAttribute("myclass", myClass);
		req.setAttribute("mystudent", myStudent);

		req.getRequestDispatcher("/WEB-INF/pages/professor/MyStudent.jsp").forward(req, resp);
	}
}