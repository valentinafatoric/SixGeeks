package web.servlets.professor.absence;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DAOProvider;
import model.Professor;
import model.Room;
import model.StudentClass;
import model.Subject;
import model.SubjectInfoForProf;
import model.TermOfLecture;
import web.servlets.UtilServlets;

@WebServlet(name = "professorAbsenceInsertServlet", urlPatterns = { "/servlets/professorabsenceinsert" })
public class ProfessorAbsenceInsertServlet extends HttpServlet {

	/** Serial version UID */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Professor professor = (Professor) req.getSession().getAttribute("professor");

		if (professor == null) {
			req.setAttribute("message", "Nedopušteni pristup!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}

		String schoolYear = UtilServlets.getCurrentSchoolYear();
		
		List<SubjectInfoForProf> subjects = DAOProvider.getDAO().getSubjectInfoForProfessor(professor, schoolYear);
		Set<StudentClass> profClasses = new HashSet<>();

		for (SubjectInfoForProf sub : subjects) {
			profClasses.addAll(DAOProvider.getDAO().getClassesForProfessorSubject(sub));
		}

		if (profClasses == null || subjects == null) {
			req.setAttribute("message", "Nema traženih podataka!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}

		req.setAttribute("subjectInfos", subjects);
		req.setAttribute("profClasses", profClasses);

		req.getRequestDispatcher("/WEB-INF/pages/professor/ProfessorAbsenceInsert.jsp").forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Professor professor = (Professor) req.getSession().getAttribute("professor");

		if (professor == null) {
			req.setAttribute("message", "Nedopušteni pristup!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}

		String subjectString = req.getParameter("subject");
		String studentClassString = req.getParameter("studentClass");
		String roomString = req.getParameter("room");

		if (studentClassString == null ) {
			req.setAttribute("message", "Niste unijeli razred!");
			error(req, resp);
			return;
		}

		Subject subject = null;
		for (Subject sub : DAOProvider.getDAO().getSubjects()) {
			if (sub.getName().equals(subjectString)) {
				subject = sub;
			}
		}

		if (subject == null) {
			req.setAttribute("message", "Nema podataka za odabran unos predmeta!");
			req.getRequestDispatcher("/WEB-INF/pages/ErrorInsert.jsp").forward(req, resp);
			return;
		}

		String schoolYear = UtilServlets.getCurrentSchoolYear();

		StudentClass studentClass = DAOProvider.getDAO().getStudentClass(studentClassString, schoolYear);

		String dateString = req.getParameter("date");
		
		DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);

		Date date = null;
		try {
			date = sdf.parse(dateString);
		} catch (Exception e) {
			req.setAttribute("message", "Datum krivo unesen!");
			error(req, resp);
			return;
		}

		String hourString = req.getParameter("hour");
		int hour = Integer.parseInt(hourString);

		Room room = null;
		try {
			room = DAOProvider.getDAO().getRoom(roomString);
		} catch (Exception e) {
			req.setAttribute("message", "Ne postoji upisana učionica!");
			error(req, resp);
			return;
		}
		
		if (room == null) {
			req.setAttribute("message", "Niste upisali učionicu!");
			error(req, resp);
			return;
		}
		
		int dayOfWeek = UtilServlets.getDayOfWeekFromDate(date);

		TermOfLecture term;
		
		try {
			term = DAOProvider.getDAO().getTerm(professor, studentClass, subject, room, dayOfWeek, hour);
		} catch (Exception ex) {
			req.setAttribute("message", "Nemoguć unos izostanaka!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}
		
		if (term == null) {
			req.setAttribute("message", "Nepostojeći termin!");
			error(req, resp);
			return;
		}

		resp.sendRedirect(req.getServletContext().getContextPath() + "/servlets/professorabsenceinsertlist/"
				+ term.getId() + "+" + sdf.format(date));
	}
	
	public void error(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{
		
		Professor professor = (Professor) req.getSession().getAttribute("professor");
		String schoolYear = UtilServlets.getCurrentSchoolYear();
		
		List<SubjectInfoForProf> subjects = DAOProvider.getDAO().getSubjectInfoForProfessor(professor, schoolYear);
		Set<StudentClass> profClasses = new HashSet<>();

		for (SubjectInfoForProf sub : subjects) {
			profClasses.addAll(DAOProvider.getDAO().getClassesForProfessorSubject(sub));
		}

		if (profClasses == null || subjects == null) {
			req.setAttribute("message", "Nema traženih podataka!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}

		req.setAttribute("subjectInfos", subjects);
		req.setAttribute("profClasses", profClasses);
		req.getRequestDispatcher("/WEB-INF/pages/professor/ProfessorAbsenceInsert.jsp").forward(req, resp);
		
	}
}
