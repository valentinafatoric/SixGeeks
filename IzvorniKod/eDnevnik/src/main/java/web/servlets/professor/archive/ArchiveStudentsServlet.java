package web.servlets.professor.archive;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DAOProvider;
import model.Professor;
import model.Student;
import model.Subject;
import model.SubjectGradesForStudent;
import model.SubjectInfoForProf;

@WebServlet(name = "archiveStudentsServlet", urlPatterns = { "/servlets/archiveprofstudents/*" })
public class ArchiveStudentsServlet extends HttpServlet {

	/** Serial version UID */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Professor professor = (Professor) req.getSession().getAttribute("professor");

		if (professor == null) {
			req.setAttribute("message", "Nedopušteni pristup!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}

		String pathInfo = req.getPathInfo().substring(1);
		String[] parts = pathInfo.split("-");

		if (parts.length != 3) {
			req.setAttribute("message", "Neispravan zahtjev!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}

		String schoolYear = parts[2];

		List<SubjectInfoForProf> subjects = DAOProvider.getDAO().getSubjectInfoForProfessor(professor, schoolYear);

		Subject subject = null;

		for (SubjectInfoForProf sub : subjects) {
			if (Integer.parseInt(parts[0]) == sub.getSubject().getId()) {
				subject = sub.getSubject();
				break;
			}
		}

		Student student = DAOProvider.getDAO().getStudentByOIB(parts[1]);
		
		SubjectGradesForStudent studentgrades = DAOProvider.getDAO().getSubjectGradesForStudent(student, subject, schoolYear);
		
		if(student == null || studentgrades == null) {
			req.setAttribute("message", "Neispravan zahtjev!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}

		req.setAttribute("schoolYear", schoolYear);
		req.setAttribute("studentgrades", studentgrades);

		req.getRequestDispatcher("/WEB-INF/pages/professor/ArchiveStudent.jsp").forward(req, resp);
	}
}