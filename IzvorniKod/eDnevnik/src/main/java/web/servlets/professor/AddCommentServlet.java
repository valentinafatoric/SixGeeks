package web.servlets.professor;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DAOProvider;
import model.Comment;
import model.Professor;
import model.Student;
import model.Subject;
import model.SubjectGradesForStudent;
import web.servlets.UtilServlets;

@WebServlet(name = "addCommentServlet", urlPatterns = { "/servlets/addcomment/*" })
public class AddCommentServlet extends HttpServlet {

	/** Serial version UID */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String pathInfo = req.getPathInfo().substring(1);
		req.setAttribute("pathInfo", pathInfo);
		req.getRequestDispatcher("/WEB-INF/pages/professor/AddComment.jsp").forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		process(req, resp);
	}

	private void process(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Professor professor = (Professor) req.getSession().getAttribute("professor");

		if (professor == null) {
			req.setAttribute("message", "Nedopušteni pristup!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}

		String pathInfo = req.getPathInfo().substring(1);

		String[] parts = pathInfo.split("-");

		if (parts.length != 2) {
			req.setAttribute("message", "Neispravan zahtjev!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}

		String schoolYear = UtilServlets.getCurrentSchoolYear();

		Subject subject = DAOProvider.getDAO().getSubject(Integer.parseInt(parts[1]));
		Student student = DAOProvider.getDAO().getStudentByOIB(parts[0]);

		if (subject == null || student == null) {
			req.setAttribute("message", "Neispravan zahtjev!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}

		SubjectGradesForStudent studentgrades = DAOProvider.getDAO().getSubjectGradesForStudent(student, subject,
				schoolYear);

		req.setAttribute("studentgrades", studentgrades);

		String commentText = req.getParameter("comment");

		if (commentText == null || commentText.trim().equals("") || commentText.trim().isEmpty()) {
			req.setAttribute("message", "Niste unijeli komentar!");
			req.setAttribute("pathInfo", pathInfo);
			req.getRequestDispatcher("/WEB-INF/pages/professor/AddComment.jsp").forward(req, resp);
			return;
		}
		if (commentText != null && commentText.trim().isEmpty()) {
			req.setAttribute("pathInfo", pathInfo);
			req.getRequestDispatcher("/WEB-INF/pages/professor/AddComment.jsp").forward(req, resp);
			return;
		}

		Date date = new Date();

		Comment comment = new Comment();
		comment.setStudentSubject(studentgrades);
		comment.setDate(date);
		comment.setText(commentText);

		DAOProvider.getDAO().addOrUpdateComment(comment);

		resp.sendRedirect(req.getServletContext().getContextPath() + "/servlets/currentprofstudents/" + student.getOIB()
				+ "-" + subject.getId());
	}
}
