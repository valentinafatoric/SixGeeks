package web.servlets.professor.absence;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DAOProvider;
import model.Absence;
import model.Professor;
import model.StudentClass;
import model.TermOfLecture;

@WebServlet(name = "AbsenceSearchServlet", urlPatterns = { "/servlets/professorabsencesearch/*" })
public class AbsenceSearchServlet extends HttpServlet {

	/** Serial version UID */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Professor professor = (Professor) req.getSession().getAttribute("professor");

		if (professor == null) {
			req.setAttribute("message", "Nedopušteni pristup!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}

		String pathInfo = req.getPathInfo().substring(1);
		String[] parts = pathInfo.split("\\+");
		
		if (parts.length != 3) {
			req.setAttribute("message", "Neispravan zahtjev!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}
		
		try {
			Integer.parseInt(parts[0]);
			Integer.parseInt(parts[2]);
		} catch (NumberFormatException ex) {
			req.setAttribute("message", "Neispravan zahtjev!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}
		
		TermOfLecture term = DAOProvider.getDAO().getTerm(Integer.parseInt(parts[0]));
		String dateString = parts[1];
		StudentClass studentClass = DAOProvider.getDAO().getStudentClass(Integer.parseInt(parts[2]));
		
		if(term == null || studentClass == null) {
			req.setAttribute("message", "Neispravan zahtjev!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}

		DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);

		Date date = null;
		try {
			date = sdf.parse(dateString);
		} catch (ParseException e) {
			req.setAttribute("message", "Neispravan zahtjev!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}

		List<Absence> absences = DAOProvider.getDAO().getAbsencesForClass(studentClass, date, term);
		
		if (absences == null) {
			req.setAttribute("message", "Neispravan zahtjev!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}
		
		if(!absences.isEmpty()) {
			Set<Absence> absencesSet = new HashSet<>();
			absencesSet.addAll(absences);
			req.setAttribute("absences", absencesSet);
		}

		req.setAttribute("studentClass", studentClass);
		req.setAttribute("date", date);
		req.setAttribute("hour", term.getOrdinalNumberOfHour());

		req.getRequestDispatcher("/WEB-INF/pages/professor/ProfessorAbsenceSearch.jsp").forward(req, resp);
	}
}