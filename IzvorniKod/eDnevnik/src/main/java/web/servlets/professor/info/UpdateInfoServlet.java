package web.servlets.professor.info;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DAOProvider;
import model.Professor;
import model.StudentClass;

@MultipartConfig
@WebServlet(name = "UpdateInfoServlet", urlPatterns = { "/servlets/updateInfo/*" })
public class UpdateInfoServlet extends HttpServlet {

	/** Serial version UID */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Professor professor = (Professor) req.getSession().getAttribute("professor");

		if (professor == null) {
			req.setAttribute("message", "Nedopušteni pristup!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}

		String pathInfo = req.getPathInfo().substring(1);
		String[] parts = pathInfo.split("-");

		if (parts.length != 1) {
			req.setAttribute("message", "Neispravan zahtjev!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}

		StudentClass classOfProf = DAOProvider.getDAO().getClassOfDeputyProfessor(professor);

		if (classOfProf == null) {
			req.setAttribute("message", "Neispravan zahtjev!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}

		req.setAttribute("classOfProf", classOfProf);

		req.getRequestDispatcher("/WEB-INF/pages/professor/UpdateInfo.jsp").forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		process(req, resp);
	}

	private void process(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Professor professor = (Professor) req.getSession().getAttribute("professor");

		if (professor == null) {
			req.setAttribute("message", "Nedopušteni pristup!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}

		String phoneNumber = req.getParameter("phoneNumber");

		if (phoneNumber != null && !phoneNumber.trim().isEmpty()) {
			professor.setPhoneNumber(phoneNumber);
		}
		
		String urlPicture = req.getParameter("pictureurl");
		
		if (urlPicture != null && !urlPicture.trim().isEmpty()) {
			professor.setPicture(urlPicture);
		}
		
		DAOProvider.getDAO().updateProfessor(professor);

		resp.sendRedirect(req.getServletContext().getContextPath() + "/servlets/professorinfo");

	}
}
