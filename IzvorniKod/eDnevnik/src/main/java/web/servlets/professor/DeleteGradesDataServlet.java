package web.servlets.professor;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DAOProvider;
import model.Professor;
import model.Student;
import model.Subject;
import model.SubjectGradesForStudent;
import web.servlets.UtilServlets;

@WebServlet(name = "DeleteGradesDataServlet", urlPatterns = { "/servlets/delgradesdata/*" })
public class DeleteGradesDataServlet extends HttpServlet {

	/** Serial version UID */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Professor professor = (Professor) req.getSession().getAttribute("professor");

		if (professor == null) {
			req.setAttribute("message", "Nedopušteni pristup!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}

		String pathInfo = req.getPathInfo().substring(1);
		String[] parts = pathInfo.split("-");
				
		String schoolYear = UtilServlets.getCurrentSchoolYear();
		
		try {
			Integer.parseInt(parts[1]);
		} catch (NumberFormatException ex) {
			req.setAttribute("message", "Neispravan zahtjev!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}
		
		Subject subject = DAOProvider.getDAO().getSubject(Integer.parseInt(parts[1]));

		Student student = DAOProvider.getDAO().getStudentByOIB(parts[0]);
		
		if(subject == null || student == null) {
			req.setAttribute("message", "Neispravan zahtjev!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}

		SubjectGradesForStudent grades = DAOProvider.getDAO().getSubjectGradesForStudent(student, subject, schoolYear);

		req.setAttribute("studentGrades", grades);
		req.setAttribute("pathInfo", pathInfo);

		req.getRequestDispatcher("/WEB-INF/pages/professor/DeleteGradesData.jsp").forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		process(req, resp);
	}

	private void process(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Professor professor = (Professor) req.getSession().getAttribute("professor");

		if (professor == null) {
			req.setAttribute("message", "Nedopušteni pristup!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}

		String pathInfo = req.getPathInfo().substring(1);
		String[] parts = pathInfo.split("-");

		if (parts.length != 2) {
			req.setAttribute("message", "Neispravan zahtjev!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}

		String schoolYear = UtilServlets.getCurrentSchoolYear();
		
		try {
			Integer.parseInt(parts[1]);
		} catch (NumberFormatException ex) {
			req.setAttribute("message", "Neispravan zahtjev!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}

		Subject subject = DAOProvider.getDAO().getSubject(Integer.parseInt(parts[1]));
		Student student = DAOProvider.getDAO().getStudentByOIB(parts[0]);

		if (subject == null || student == null) {
			req.setAttribute("message", "Neispravan zahtjev!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}

		SubjectGradesForStudent studentgrades = DAOProvider.getDAO().getSubjectGradesForStudent(student, subject,
				schoolYear);

		String[] checkedGrades = req.getParameterValues("checkedGrades");
		String[] checkedComments = req.getParameterValues("checkedComments");

		try {
			if (checkedGrades != null && checkedGrades.length != 0) {
				for (String ch : checkedGrades) {
					DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.ENGLISH);
					Date date = format.parse(ch);

					DAOProvider.getDAO().deleteGrade(studentgrades, date);
				}
			}

			if (checkedComments != null && checkedComments.length != 0) {
				for (String ch : checkedComments) {
					DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.ENGLISH);
					Date date = format.parse(ch);

					DAOProvider.getDAO().deleteComment(studentgrades, date);
				}
			}
		} catch (ParseException e) {
		}

		String checkedFinalGrade = req.getParameter("checkedFinalGrade");

		if (checkedFinalGrade != null) {
			studentgrades.setFinalGrade(0);
			DAOProvider.getDAO().addOrUpdateSubjectGradesForStudent(studentgrades);
		}

		resp.sendRedirect(req.getServletContext().getContextPath() + "/servlets/currentprofstudents/" + student.getOIB()
				+ "-" + subject.getId());
	}
}
