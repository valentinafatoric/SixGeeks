package web.servlets.professor;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DAOProvider;
import model.Professor;
import model.SubjectInfoForProf;
import web.servlets.UtilServlets;

@WebServlet(name = "professorSubjectsServlet", urlPatterns = { "/servlets/professorsubjects" })
public class ProfessorSubjectsServlet extends HttpServlet {

	/** Serial version UID */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Professor professor = (Professor) req.getSession().getAttribute("professor");

		if (professor == null) {
			req.setAttribute("message", "Nedopušteni pristup!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}

		String schoolYear = UtilServlets.getCurrentSchoolYear();

		List<SubjectInfoForProf> subjects = DAOProvider.getDAO().getSubjectInfoForProfessor(professor, schoolYear);

		req.setAttribute("subjectInfos", subjects);

		req.getRequestDispatcher("/WEB-INF/pages/professor/ProfessorSubjects.jsp").forward(req, resp);
	}
}
