package web.servlets.professor.info;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DAOProvider;
import model.Consultation;
import model.Professor;
import model.Room;
import model.StudentClass;

@WebServlet(name = "AddCosultationServlet", urlPatterns = { "/servlets/addconsultation/*" })
public class AddConsultationServlet extends HttpServlet {

	/** Serial version UID */
	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Professor professor = (Professor) req.getSession().getAttribute("professor");

		if (professor == null) {
			req.setAttribute("message", "Nedopušteni pristup!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}

		String pathInfo = req.getPathInfo().substring(1);
		String[] parts = pathInfo.split("-");

		if (parts.length != 1) {
			req.setAttribute("message", "Neispravan zahtjev!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}
		
		StudentClass classOfProf = DAOProvider.getDAO().getClassOfDeputyProfessor(professor);
		
		if (classOfProf == null) {
			req.setAttribute("message", "Neispravan zahtjev!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}
		
		req.setAttribute("pathInfo", pathInfo);
		req.setAttribute("classOfProf", classOfProf);
		
		req.getRequestDispatcher("/WEB-INF/pages/professor/AddConsultation.jsp").forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		process(req, resp);
	}

	private void process(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Professor professor = (Professor) req.getSession().getAttribute("professor");

		if (professor == null) {
			req.setAttribute("message", "Nedopušteni pristup!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}

		StudentClass classOfProf = DAOProvider.getDAO().getClassOfDeputyProfessor(professor);

		if (classOfProf != null) {
			String dateString = req.getParameter("datetime");
			String roomString = req.getParameter("room");
			String typeOfMeeting = req.getParameter("typeOfMeeting").trim().toLowerCase();


			if(dateString==null && roomString==null){
				req.getRequestDispatcher("/WEB-INF/pages/professor/ProfessorInfo.jsp").forward(req, resp);
				return;
			}
			
			if (dateString == null) {
				req.setAttribute("message", "Datum krivo označen!");
				doGet(req, resp);
				return;
			}

			DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");

			Date date = null;
			try {
				date = sdf.parse(dateString);
			} catch (Exception e) {
				req.setAttribute("message", "Datum krivo označen!");
				doGet(req, resp);
				return;
			}

			Room room = DAOProvider.getDAO().getRoom(roomString);

			if (room == null) {
				req.setAttribute("message", "Krivo unesena učionica!");
				doGet(req, resp);
				return;
			}

			Consultation consultation = new Consultation();
			consultation.setDateAndTime(date);
			consultation.setRoom(room);

			if (typeOfMeeting.equals("roditeljski sastanak")) {
				consultation.setParentMeeting(true);
			} else if (typeOfMeeting.equals("konzultacije")) {
				consultation.setParentMeeting(false);
			}
			consultation.setStudentClass(classOfProf);

			try {
				DAOProvider.getDAO().addOrUpdateConsultation(consultation);
			} catch (Exception e) {
				req.setAttribute("message", "Nemoguća promjena!");
				req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
				return;
			}

		}

		resp.sendRedirect(req.getServletContext().getContextPath() + "/servlets/professorinfo");
	}

}
