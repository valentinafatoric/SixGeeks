package web.servlets.professor.archive;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DAOProvider;
import model.Professor;
import model.StudentClass;
import model.SubjectInfoForProf;

@WebServlet(name = "archiveSubjectDetailsServlet", urlPatterns = { "/servlets/archivesubjectdetails/*" })
public class ArchiveSubjectDetailsServlet extends HttpServlet {

	/** Serial version UID */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		Professor professor = (Professor) req.getSession().getAttribute("professor");

		if (professor == null) {
			req.setAttribute("message", "Nedopušteni pristup!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}

		String pathInfo = req.getPathInfo().substring(1);

		String[] parts = pathInfo.split("-");

		if (parts.length != 2) {
			req.setAttribute("message", "Neispravan zahtjev!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}

		String schoolYear = parts[1];

		List<SubjectInfoForProf> subjectInfos = DAOProvider.getDAO().getSubjectInfoForProfessor(professor, schoolYear);
		
		SubjectInfoForProf subjectInfo = null;
		for (SubjectInfoForProf sub : subjectInfos) {
			if (parts[0].equals(sub.getSubject().getName())) {
				subjectInfo = sub;
				break;
			}
		}

		Set<StudentClass> classes = null;
		try {
			classes = DAOProvider.getDAO().getClassesForProfessorSubject(subjectInfo);
		} catch (Exception e) {
			req.setAttribute("message", "Ne postoje razredi za prikaz!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}
		
		String classYearsString = "";

		Set<String> classYears = new HashSet<>();

		for (StudentClass cl : classes) {
			classYears.add(cl.getName().substring(0, 1));
		}

		for (String classYear : classYears) {
			classYearsString += classYear + ", ";
		}

		if (classYearsString.length() > 2) {
			classYearsString = classYearsString.substring(0, classYearsString.length() - 2);
		} else {
			classYearsString = "";
		}

		req.setAttribute("schoolYear", schoolYear);
		req.setAttribute("subjectInfo", subjectInfo);
		req.setAttribute("classYears", classYearsString);
		req.setAttribute("classes", classes);

		req.getRequestDispatcher("/WEB-INF/pages/professor/ArchiveSubjectsDetails.jsp").forward(req, resp);
	}
}
