package web.servlets.professor.absence;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DAOProvider;
import model.Absence;
import model.Professor;
import model.Student;
import model.TermOfLecture;

@WebServlet(name = "professorAbsenceInsertListServlet", urlPatterns = { "/servlets/professorabsenceinsertlist/*" })
public class ProfessorAbsenceInsertListServlet extends HttpServlet {

	/** Serial version UID */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Professor professor = (Professor) req.getSession().getAttribute("professor");
		String pathInfo = req.getPathInfo().substring(1);

		if (professor == null) {
			req.setAttribute("message", "Nedopušteni pristup!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		} else if (pathInfo.split("\\+").length < 1) {
			req.setAttribute("message", "Pogrešan URL!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}

		String termDate = pathInfo;
		String termId = pathInfo.split("\\+")[0];

		TermOfLecture term = null;
		try {
			term = DAOProvider.getDAO().getTerm(Integer.parseInt(termId));
		} catch (Exception ex) {
			req.setAttribute("message", "Nevaljani termin!");
			req.getRequestDispatcher("/WEB-INF/pages/ErrorInsert.jsp").forward(req, resp);
			return;
		}

		req.setAttribute("studentClass", term.getStudentClass());
		req.setAttribute("termDate", termDate);
		req.getRequestDispatcher("/WEB-INF/pages/professor/ProfessorAbsenceInsertList.jsp").forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Professor professor = (Professor) req.getSession().getAttribute("professor");

		if (professor == null) {
			req.setAttribute("message", "Nedopušteni pristup!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}

		String[] checkedStudents = req.getParameterValues("justifiably");
		String pathInfo = req.getPathInfo().substring(1);
		
		String[] parts = pathInfo.split("\\+");

		if (parts.length < 2) {
			req.setAttribute("message", "Neispravan URL!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		} else if (checkedStudents.length == 0) {
			req.setAttribute("message", "Nema označenih studenata!");
			req.getRequestDispatcher("/WEB-INF/pages/ErrorInsert.jsp").forward(req, resp);
			return;
		}

		String termId = parts[0];
		String dateString = parts[1];

		DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);

		Date date;
		try {
			date = sdf.parse(dateString);
		} catch (ParseException e) {
			req.setAttribute("message", "datum pogrešnog oblika");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}
		
		try {
			Integer.parseInt(termId);
		} catch (NumberFormatException ex) {
			req.setAttribute("message", "Neispravan zahtjev!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}

		TermOfLecture term = DAOProvider.getDAO().getTerm(Integer.parseInt(termId));
		
		if(term == null) {
			req.setAttribute("message", "Neispravan zahtjev!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}
		
		for(String OIB : checkedStudents) {
			Student student = DAOProvider.getDAO().getStudentByOIB(OIB);
			
			Absence absence = new Absence();
			absence.setDate(date);
			absence.setStudent(student);
			absence.setTerm(term);
			absence.setComment("");
			absence.setJustifiably(false);

			try {
				DAOProvider.getDAO().addOrUpdateAbsence(absence);
			} catch (Exception ex) {
				req.setAttribute("message", "Nemoguć unos izostanaka!");
				req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
				return;
			}
		}
		
		resp.sendRedirect(req.getServletContext().getContextPath() + "/servlets/professorabsencemain");
	}

}