package web.servlets.professor.info;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DAOProvider;
import model.Professor;
import model.School;
import model.StudentClass;

@WebServlet(name = "PersonalInfoProfessorServlet", urlPatterns = { "/servlets/professorinfo" })
public class PersonalInfoProfessorServlet extends HttpServlet {

	/** Serial version UID */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Professor professor = (Professor) req.getSession().getAttribute("professor");

		if (professor == null) {
			req.setAttribute("message", "Nedopušteni pristup!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}
		
		try {
			StudentClass classOfProf = DAOProvider.getDAO().getClassOfDeputyProfessor(professor);
			School school = DAOProvider.getDAO().getSchool(professor.getSchool());
			
			req.setAttribute("classOfProf", classOfProf);
			req.setAttribute("school", school);
			req.setAttribute("consultations", classOfProf.getConsultations());
		} catch (Exception e) {
		}

		req.getRequestDispatcher("/WEB-INF/pages/professor/ProfessorInfo.jsp").forward(req, resp);

	}

}
