package web.servlets.professor;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DAOProvider;
import model.Grade;
import model.Professor;
import model.Student;
import model.Subject;
import model.SubjectGradesForStudent;
import web.servlets.UtilServlets;

@WebServlet(name = "addGradeServlet", urlPatterns = { "/servlets/addgrade/*" })
public class AddGradeServlet extends HttpServlet {

	/** Serial version UID */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String pathInfo = req.getPathInfo().substring(1);
		req.setAttribute("pathInfo", pathInfo);
		req.getRequestDispatcher("/WEB-INF/pages/professor/AddGrade.jsp").forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		process(req, resp);
	}

	private void process(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Professor professor = (Professor) req.getSession().getAttribute("professor");

		if (professor == null) {
			req.setAttribute("message", "Nedopušteni pristup!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}

		String pathInfo = req.getPathInfo().substring(1);
		String[] parts = pathInfo.split("-");

		if (parts.length != 2) {
			req.setAttribute("message", "Neispravan zahtjev!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}

		String schoolYear = UtilServlets.getCurrentSchoolYear();

		try {
			Integer.parseInt(parts[1]);
		} catch (NumberFormatException ex) {
			req.setAttribute("message", "Neispravan zahtjev!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}

		Subject subject = DAOProvider.getDAO().getSubject(Integer.parseInt(parts[1]));
		Student student = DAOProvider.getDAO().getStudentByOIB(parts[0]);

		if (subject == null || student == null) {
			req.setAttribute("message", "Neispravan zahtjev!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}

		SubjectGradesForStudent studentgrades = DAOProvider.getDAO().getSubjectGradesForStudent(student, subject,
				schoolYear);

		req.setAttribute("studentgrades", studentgrades);

		String value = req.getParameter("value");

		String component = req.getParameter("component");

		int gradeValue;

		try {
			gradeValue = Integer.parseInt(value);
		} catch (NumberFormatException | NullPointerException ex) {
			req.setAttribute("message", "Ocjena je neispravna ili nije unesena!");
			req.getRequestDispatcher("/WEB-INF/pages/professor/AddGrade.jsp").forward(req, resp);
			return;
		}

		if (gradeValue < 1 || gradeValue > 5) {
			req.setAttribute("message", "Unesena ocjena je neispravna!");
			req.getRequestDispatcher("/WEB-INF/pages/professor/AddGrade.jsp").forward(req, resp);
			return;
		}

		Date date = new Date();

		Grade grade = new Grade();
		grade.setDate(date);
		grade.setStudentSubject(studentgrades);
		grade.setValue(gradeValue);
		grade.setComponent(component.toLowerCase());
		DAOProvider.getDAO().addOrUpdateGrade(grade);

		resp.sendRedirect(req.getServletContext().getContextPath() + "/servlets/currentprofstudents/" + student.getOIB()
				+ "-" + subject.getId());
	}
}
