package web.servlets.professor;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DAOProvider;
import model.Professor;
import model.Student;
import model.Subject;
import model.SubjectGradesForStudent;
import web.servlets.UtilServlets;

@WebServlet(name = "currentProfessorStudentServlet", urlPatterns = { "/servlets/currentprofstudents/*" })
public class CurrentProfessorStudentServlet extends HttpServlet {

	/** Serial version UID */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		Professor professor = (Professor) req.getSession().getAttribute("professor");

		if (professor == null) {
			req.setAttribute("message", "Nedopušteni pristup!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}

		String pathInfo = req.getPathInfo().substring(1);
		String[] parts = pathInfo.split("-");

		if (parts.length != 2) {
			req.setAttribute("message", "Neispravan zahtjev!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}

		String schoolYear = UtilServlets.getCurrentSchoolYear();
		
		try {
			Integer.parseInt(parts[1]);
		} catch (NumberFormatException ex) {
			req.setAttribute("message", "Neispravan zahtjev!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}

		Subject subject = DAOProvider.getDAO().getSubject(Integer.parseInt(parts[1]));

		Student student = DAOProvider.getDAO().getStudentByOIB(parts[0]);
		
		if(subject == null || student == null) {
			req.setAttribute("message", "Neispravan zahtjev!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}

		SubjectGradesForStudent studentgrades = DAOProvider.getDAO().getSubjectGradesForStudent(student, subject,
				schoolYear);

		req.setAttribute("studentgrades", studentgrades);

		req.getRequestDispatcher("/WEB-INF/pages/professor/CurrentProfessorStudent.jsp").forward(req, resp);
	}
}
