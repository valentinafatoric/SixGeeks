package web.servlets.admin;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DAOProvider;
import model.Administrator;
import model.Subject;

@WebServlet(name = "editSubjectServlet", urlPatterns = { "/servlets/editsubject" })
public class EditSubjectListServlet extends HttpServlet {

	/** Serial version UID */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.getSession().setAttribute("subjects", DAOProvider.getDAO().getSubjects());
		Administrator admin = (Administrator) req.getSession().getAttribute("administrator");

		if (admin == null) {
			req.setAttribute("message", "Nedopušteni pristup!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}

		req.getRequestDispatcher("/WEB-INF/pages/admin/EditSubject.jsp").forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Administrator admin = (Administrator) req.getSession().getAttribute("administrator");

		if (admin == null) {
			req.setAttribute("message", "Nedopušteni pristup!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}

		String action = req.getParameter("action");
		int id = Integer.parseInt(req.getParameter("subjectselect"));
		Subject subject = DAOProvider.getDAO().getSubject(id);

		if (action.equals("edit")) {
			req.setAttribute("subject", subject);
			req.getRequestDispatcher("/WEB-INF/pages/admin/EditSubjectInfo.jsp").forward(req, resp);
		} else if (action.equals("delete")) {
			DAOProvider.getDAO().deleteSubject(subject);
		}

		resp.sendRedirect(req.getServletContext().getContextPath() + "/servlets/adminmain");

	}

}
