package web.servlets.admin;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DAOProvider;
import model.Administrator;
import model.Student;

@WebServlet(name="editStudentServlet", urlPatterns={"/servlets/studentlist"})
public class EditStudentListServlet extends HttpServlet {

	/** Serial version UID */
	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.getSession().setAttribute("students", DAOProvider.getDAO().getStudents());
		Administrator admin = (Administrator) req.getSession().getAttribute("administrator");
		
		if(admin == null) {
			req.setAttribute("message", "Nedopušteni pristup!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}
		
		req.getRequestDispatcher("/WEB-INF/pages/admin/EditStudents.jsp").forward(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{
		Administrator admin = (Administrator) req.getSession().getAttribute("administrator");
		
		if(admin == null) {
			req.setAttribute("message", "Nedopušteni pristup!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}
		
		String OIB = req.getParameter("studentselect");
		Student stud = DAOProvider.getDAO().getStudentByOIB(OIB);
		req.setAttribute("student1", stud);
		req.setAttribute("studentclasses", DAOProvider.getDAO().getClasses());
		
		req.getRequestDispatcher("/WEB-INF/pages/admin/EditStudentPage.jsp").forward(req, resp);

	}
	
	
}
