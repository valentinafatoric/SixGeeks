package web.servlets.admin;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DAOException;
import dao.DAOProvider;
import model.Administrator;
import model.Parents;
import model.Student;

@WebServlet(name="editStudentRespServlet", urlPatterns={"/servlets/editstudent"})
public class EditStudentServlet extends HttpServlet {

	/** Serial version UID */
	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Administrator admin = (Administrator) req.getSession().getAttribute("administrator");
		req.getSession().setAttribute("studentclasses", DAOProvider.getDAO().getClasses());
		
		if(admin == null) {
			req.setAttribute("message", "Nedopušteni pristup!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}
		
		req.getRequestDispatcher("/WEB-INF/pages/admin/EditStudentPage.jsp").forward(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{
		Administrator admin = (Administrator) req.getSession().getAttribute("administrator");
		
		if(admin == null) {
			req.setAttribute("message", "Nedopušteni pristup!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}
		
		Student student = DAOProvider.getDAO().getStudentByOIB(req.getParameter("studentid"));
		
		student.setUsername(convert(req.getParameter("studentusernameedit")));
		student.setPassword(convert(req.getParameter("studentpasswordedit")));
		student.setName(convert(req.getParameter("studentname")));
		student.setSurname(convert(req.getParameter("studentsurname")));
		
		String sex = req.getParameter("studentsexselect");
		
		if(sex.equals("M")) student.setSex("muško");
		else student.setSex("žensko");
		
		student.setDateOfBirth(parseDate(req.getParameter("studentdateofbirth")));
		student.setPlaceOfBirth(req.getParameter("studentplaceofbirth"));
		student.setCurrentStatus(req.getParameter("studentcurrentstatus"));
		student.setOIB(req.getParameter("studentoib"));
		student.setPicture(req.getParameter("studentpicture"));
		
		Parents parents = student.getParents();
		
		parents.setMothersName(req.getParameter("studentmothername"));
		parents.setFathersName(req.getParameter("studentfathername"));
		parents.setUsername(req.getParameter("parentsusername"));
		parents.setPassword(req.getParameter("parentspassword"));
		
		student.setClassOfStudent(DAOProvider.getDAO().getStudentClass(Integer.parseInt(req.getParameter("classselect"))));
		
		DAOProvider.getDAO().addOrUpdateStudent(student);
		DAOProvider.getDAO().addOrUpdateParents(parents);
		
		resp.sendRedirect(req.getServletContext().getContextPath() + "/servlets/adminmain");
		
	}
	
	private String convert(String s) {
		if(s == null) return "";
		return s.trim();
	}
	
	private Date parseDate(String date) {
		DateFormat format = new SimpleDateFormat("YYYY-MM-dd", Locale.ENGLISH);
		try {
			return format.parse(date);
		} catch (ParseException e) {
			throw new DAOException("Greška prilikom parsiranja datuma!");
		}
	}
}
