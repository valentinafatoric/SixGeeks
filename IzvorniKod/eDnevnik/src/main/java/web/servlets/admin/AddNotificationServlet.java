package web.servlets.admin;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DAOProvider;
import model.Administrator;
import model.EventDiary;
import model.Notification;

@WebServlet(urlPatterns="/servlets/addnotification")
public class AddNotificationServlet extends HttpServlet { 

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.getRequestDispatcher("/WEB-INF/pages/admin/AddNotification.jsp").forward(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Administrator admin = (Administrator) req.getSession().getAttribute("administrator");
		
		if(admin == null) {
			req.setAttribute("message", "Nedopušteni pristup!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}
		
		String title = req.getParameter("title"); 
		String text = req.getParameter("text");
	    String picture = req.getParameter("notificationpicture");
	    
	    Notification n = new Notification();
	    n.setCreator(admin);
	    Date date = new Date();
	    n.setDate(date);
	    n.setTitle(title);
	    n.setPicture("".equals(picture) ? null : picture);
	    n.setText(text);
	    n.setArchived(false);
	    
	 
	    
	    DAOProvider.getDAO().addOrUpdateNotification(n);
	    
	    String entry = String.format("Admin %s %s dodaje obavijest s naslovom %s.", admin.getName(), admin.getSurname(), title);
	    DAOProvider.getDAO().addEvent(new EventDiary(admin, entry, date));
	    
	    resp.sendRedirect(req.getServletContext().getContextPath() + "/servlets/manipulatenotifications");
	}
}
