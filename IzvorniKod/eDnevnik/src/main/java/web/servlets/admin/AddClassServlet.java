package web.servlets.admin;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DAOProvider;
import model.Administrator;
import model.Consultation;
import model.Student;
import model.StudentClass;

@WebServlet(name = "addClass", urlPatterns = { "/servlets/addstudentclass" })
public class AddClassServlet extends HttpServlet {

	/** Serial version UID */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Administrator admin = (Administrator) req.getSession().getAttribute("administrator");
		req.setAttribute("professors", DAOProvider.getDAO().getProfessors());

		if (admin == null) {
			req.setAttribute("message", "Nedopušteni pristup!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}

		req.getRequestDispatcher("/WEB-INF/pages/admin/AddClass.jsp").forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Administrator admin = (Administrator) req.getSession().getAttribute("administrator");

		if (admin == null) {
			req.setAttribute("message", "Nedopušteni pristup!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}

		StudentClass studentClass = new StudentClass();

		studentClass.setName(req.getParameter("classname"));
		studentClass.setSchoolYear(req.getParameter("classyear"));

		List<Student> students = new LinkedList<>();

		studentClass.setStudents(students);
		studentClass.setProfessor(DAOProvider.getDAO().getProfessorByOIB(req.getParameter("razrednikselect")).get(0));
		studentClass.setDeputyProfessor(
				DAOProvider.getDAO().getProfessorByOIB(req.getParameter("zamjenikrazrednikselect")).get(0));
		studentClass.setSchool(admin.getSchool());

		List<Consultation> consultations = new LinkedList<>();

		studentClass.setConsultations(consultations);

		DAOProvider.getDAO().addStudentClass(studentClass);

		resp.sendRedirect(req.getServletContext().getContextPath() + "/servlets/adminmain");

	}

}
