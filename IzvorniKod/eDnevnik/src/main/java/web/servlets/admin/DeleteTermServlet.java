package web.servlets.admin;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DAOProvider;
import model.Administrator;
import model.EventDiary;
import model.TermOfLecture;

@WebServlet(urlPatterns="/servlets/deleteterm")
public class DeleteTermServlet extends HttpServlet {


	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Administrator admin = (Administrator) req.getSession().getAttribute("administrator");
		
		if(admin == null) {
			req.setAttribute("message", "Nedopušteni pristup!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}
		
		
		int id = Integer.valueOf(req.getParameter("id"));
		

		TermOfLecture term = DAOProvider.getDAO().getTerm(id);
		String entry = String.format("Administartor %s %s briše predavanje iz predmeta %s razredu %s.",
				admin.getName(), admin.getSurname(), term.getSubject().getName(), term.getStudentClass().getName());

		DAOProvider.getDAO().addEvent(new EventDiary(admin, entry, new Date()));;
		
		DAOProvider.getDAO().deleteTermById(id);
		
		resp.setContentType("text/plain");
		resp.setCharacterEncoding("UTF-8");
		resp.getWriter().write("izbrisan");
		resp.flushBuffer();
		
	}
}
