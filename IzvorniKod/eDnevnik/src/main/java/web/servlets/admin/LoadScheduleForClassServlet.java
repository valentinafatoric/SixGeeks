package web.servlets.admin;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DAOProvider;
import model.Administrator;
import model.TermOfLecture;

@WebServlet(name="classscheduleloader",urlPatterns="/servlets/generateschedule")
public class LoadScheduleForClassServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Administrator admin = (Administrator) req.getSession().getAttribute("administrator");
		
		if(admin == null) {
			req.setAttribute("message", "Nedopušteni pristup!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}
		
		Map<String, Map<String, TermOfLecture>> schedule = DAOProvider.getDAO().getTermOfClass(DAOProvider.getDAO().getStudentClass(Integer.valueOf(req.getParameter("studentClass"))));

		req.setAttribute("schedule", schedule);
		req.getRequestDispatcher("/WEB-INF/pages/admin/EditableScheduleGenerator.jsp").forward(req, resp);;
	}
	

}
