package web.servlets.admin;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DAOProvider;

@WebServlet(name="checkusername", urlPatterns={"/servlets/checkusername"})
public class CheckUsernameServlet extends HttpServlet {

	/** Serial version UID */
	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String exists = "";
		String username = req.getParameter("username");
		
		if(DAOProvider.getDAO().getUser(username) != null) exists = "postoji";
		
		resp.setContentType("text/html");
		resp.setCharacterEncoding("UTF-8");
		resp.getWriter().write(exists);
		resp.flushBuffer();
	}
	
	
}
