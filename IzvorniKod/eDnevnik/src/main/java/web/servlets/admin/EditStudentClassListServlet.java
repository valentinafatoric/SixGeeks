package web.servlets.admin;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DAOProvider;
import model.Administrator;
import model.StudentClass;

@WebServlet(name="editStudentClassServlet", urlPatterns={"/servlets/studentclasslist"})
public class EditStudentClassListServlet extends HttpServlet {

	/** Serial version UID */
	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.getSession().setAttribute("studentclasses", DAOProvider.getDAO().getClasses());
		Administrator admin = (Administrator) req.getSession().getAttribute("administrator");
		
		if(admin == null) {
			req.setAttribute("message", "Nedopušteni pristup!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}
		
		req.getRequestDispatcher("/WEB-INF/pages/admin/EditStudentClass.jsp").forward(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{
		Administrator admin = (Administrator) req.getSession().getAttribute("administrator");
		
		if(admin == null) {
			req.setAttribute("message", "Nedopušteni pristup!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}
		
		String action = req.getParameter("action");
		StudentClass studentClass = DAOProvider.getDAO()
				.getStudentClass(Integer.parseInt(req.getParameter("studentclassselect")));
		
		
		if(action.equals("edit")) {
			req.setAttribute("studentclass", studentClass);
			req.setAttribute("studentclassstudents", studentClass.getStudents());
			req.setAttribute("students", DAOProvider.getDAO().getStudents());
			req.setAttribute("professors", DAOProvider.getDAO().getProfessors());
			
			req.getRequestDispatcher("/WEB-INF/pages/admin/EditStudentClassInfo.jsp").forward(req, resp);
			
		}
		
		else if(action.equals("delete")) {
			DAOProvider.getDAO().deleteStudentClass(studentClass);
		}
		
		resp.sendRedirect(req.getServletContext().getContextPath() + "/servlets/adminmain");

	}
	
	
}
