package web.servlets.admin;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DAOProvider;
import model.Administrator;
import model.EventDiary;
import model.Notification;

@WebServlet(name="modifynotification", urlPatterns="/servlets/modifynotification")
public class ModifyNotificationServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Administrator admin = (Administrator) req.getSession().getAttribute("administrator");
		
		if(admin == null) {
			req.setAttribute("message", "Nedopušteni pristup!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}
		
		String action = req.getParameter("action");
		Notification notification = DAOProvider.getDAO().getNotificationById(Integer.valueOf(req.getParameter("notifId")));
		
		if (action.equals("delete")) {
			DAOProvider.getDAO().deleteNotification(notification);
			
			String entry = String.format("Administartor %s %s briše obavijest s naslovom %s.",
						admin.getName(), admin.getSurname(), notification.getTitle());

			DAOProvider.getDAO().addEvent(new EventDiary(admin, entry, new Date()));
		} else if (action.equals("archive")) {
			notification.setArchived(true);
			DAOProvider.getDAO().addOrUpdateNotification(notification);
			
			String entry = String.format("Administartor %s %s arhivira obavijest s naslovom %s.",
					admin.getName(), admin.getSurname(), notification.getTitle());

			DAOProvider.getDAO().addEvent(new EventDiary(admin, entry, new Date()));;
		}

	    resp.sendRedirect(req.getServletContext().getContextPath() + "/servlets/manipulatenotifications");
	}
}
