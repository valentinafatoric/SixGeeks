package web.servlets.admin;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DAOProvider;
import model.Administrator;
import web.servlets.UtilServlets;

@WebServlet(urlPatterns="/servlets/editlectures")
public class EditLecturesServlet  extends HttpServlet{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Administrator admin = (Administrator) req.getSession().getAttribute("administrator");
		
		if(admin == null) {
			req.setAttribute("message", "Nedopušteni pristup!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}

		req.setAttribute("subjProfs", DAOProvider.getDAO().getProfsOfSubjectsInYearAndSchool(admin.getSchool(), UtilServlets.getCurrentSchoolYear()));
		req.setAttribute("studentclasses", DAOProvider.getDAO().getClassesForSchool(admin.getSchool()));
		req.getRequestDispatcher("/WEB-INF/pages/admin/ChooseLectureToEdit.jsp").forward(req, resp);
	}
}
