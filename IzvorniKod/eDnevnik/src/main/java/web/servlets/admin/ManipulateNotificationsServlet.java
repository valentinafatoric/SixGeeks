package web.servlets.admin;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DAOProvider;
import model.Administrator;
import model.Notification;

@WebServlet(name = "notificationmodify", urlPatterns = { "/servlets/manipulatenotifications" })
public class ManipulateNotificationsServlet extends HttpServlet {

	/** Serial version UID */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Administrator admin = (Administrator) req.getSession().getAttribute("administrator");
		
		if(admin == null) {
			req.setAttribute("message", "Nedopušteni pristup!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}
		
		List<Notification> notifications = DAOProvider.getDAO().getAllNotificationsForSchool(admin.getSchool());
		notifications.sort((n1,n2)->n2.getDate().compareTo(n1.getDate()));
		
		req.setAttribute("notifications", notifications);
		req.getRequestDispatcher("/WEB-INF/pages/admin/ManipulateNotifications.jsp").forward(req, resp);
	}

}
