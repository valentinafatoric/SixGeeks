package web.servlets.admin;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DAOProvider;
import model.Administrator;
import model.EventDiary;
import model.Professor;
import model.Room;
import model.StudentClass;
import model.Subject;
import model.TermOfLecture;

@WebServlet(urlPatterns="/servlets/addlecture")
public class AddLectureServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Administrator admin = (Administrator) req.getSession().getAttribute("administrator");
		
		if(admin == null) {
			req.setAttribute("message", "Nedopušteni pristup!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}
		String studentClass = req.getParameter("studentClassId");
		String day = req.getParameter("day");
		String hour = req.getParameter("hour");
		
		if (studentClass == null || day==null || hour==null) {
			req.setAttribute("message", "Neispravna putanja!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}
		
		req.setAttribute("studentClass", DAOProvider.getDAO().getStudentClass(Integer.valueOf(studentClass)).getName());
		req.setAttribute("subjects", DAOProvider.getDAO().getSubjects());
		req.setAttribute("rooms", DAOProvider.getDAO().getRoomsInSchool(admin.getSchool()));
		req.getRequestDispatcher("/WEB-INF/pages/admin/AddLecture.jsp").forward(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Administrator admin = (Administrator) req.getSession().getAttribute("administrator");
		
		if(admin == null) {
			req.setAttribute("message", "Nedopušteni pristup!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}
		
		StudentClass studentClass = DAOProvider.getDAO().getStudentClass(Integer.parseInt(req.getParameter("classselect")));
		Subject subject = DAOProvider.getDAO().getSubject(Integer.parseInt(req.getParameter("subjectselect")));
		Professor professor = DAOProvider.getDAO().getProfessorByOIB(req.getParameter("profselect")).get(0);
		Room room = DAOProvider.getDAO().getRoomById(Integer.parseInt(req.getParameter("roomselect")));
		int dayOfWeek = Integer.parseInt(req.getParameter("dayselect"));
		int hourOfDay = Integer.parseInt(req.getParameter("hourselect"));
		
		resp.setContentType("text/plain");
		resp.setCharacterEncoding("UTF-8");
		List<TermOfLecture> profLectures = DAOProvider.getDAO().getLecturesForProfessorAtTime(professor, dayOfWeek, hourOfDay);
		if (!profLectures.isEmpty()) {
			TermOfLecture term = profLectures.get(0);
			resp.getWriter().write(String.format("Profesor %s %s je utom terminu zauzet, odražava predavanje razredu %s u učionici %s.",
							professor.getName(), professor.getSurname(), term.getStudentClass().getName(), term.getRoom().getName()));
			resp.flushBuffer();
			return;
		}
		
		List<TermOfLecture> lectureForClass = DAOProvider.getDAO().getLectureForClassAt(studentClass, dayOfWeek, hourOfDay);
		if (!lectureForClass.isEmpty()) {
			resp.getWriter().write(String.format("Razred %s u tom terminu već ima predavanje iz predmeta %s u učionici %s.",
								lectureForClass.get(0).getStudentClass().getName(), lectureForClass.get(0).getSubject().getName(), 
								lectureForClass.get(0).getRoom().getName()));
			resp.flushBuffer();
			return;
		}
		
		List<TermOfLecture> lecturesAtThatTime = DAOProvider.getDAO().getLecturesAtTerm(dayOfWeek, hourOfDay)
																	.stream()
																	.filter(l -> l.getRoom().getName().equals(room.getName()))
																	.collect(Collectors.toList());


		if (!lecturesAtThatTime.isEmpty()) {
			TermOfLecture term = lecturesAtThatTime.get(0);
			resp.getWriter().write(String.format("U odabranom terminu se u toj učionici već održava predavanje iz predmeta %s razredu %s.",
							term.getSubject().getName(), term.getStudentClass().getName()));

			resp.flushBuffer();
			return;
		}
		

		
		TermOfLecture term = new TermOfLecture();
		term.setId(DAOProvider.getDAO().getNextTermId());
		term.setStudentClass(studentClass);
		term.setProfessor(professor);
		term.setSubject(subject);
		term.setRoom(room);
		term.setDayOfWeek(dayOfWeek);
		term.setOrdinalNumberOfHour(hourOfDay);
		
		DAOProvider.getDAO().addOrUpdateTermOfLecture(term);
		String entry = String.format("Administartor %s %s dodaje razredu %s predavanje za %d.sat iz predmeta %s u učionici %s.",
										admin.getName(), admin.getSurname(), studentClass.getName(), hourOfDay, subject.getName(),
										room.getName());

		DAOProvider.getDAO().addEvent(new EventDiary(professor, entry, new Date()));;
		resp.getWriter().write("added");

		resp.flushBuffer();
	}
	
	
}
