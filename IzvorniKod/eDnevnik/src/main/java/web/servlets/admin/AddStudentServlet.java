package web.servlets.admin;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DAOException;
import dao.DAOProvider;
import model.Administrator;
import model.Parents;
import model.Student;
import model.StudentClass;

@WebServlet(name="addStudent", urlPatterns={"/servlets/addstudent"})
public class AddStudentServlet extends HttpServlet {

	/** Serial version UID */
	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.getSession().setAttribute("schools", DAOProvider.getDAO().getSchools());
		req.getSession().setAttribute("studentclasses", DAOProvider.getDAO().getClasses());
		Administrator admin = (Administrator) req.getSession().getAttribute("administrator");
		
		if(admin == null) {
			req.setAttribute("message", "Nedopušteni pristup!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}
		
		req.getRequestDispatcher("/WEB-INF/pages/admin/AddNewStudent.jsp").forward(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{
		Administrator admin = (Administrator) req.getSession().getAttribute("administrator");
		
		if(admin == null) {
			req.setAttribute("message", "Nedopušteni pristup!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}
		
		Student stud = new Student();
		stud.setUsername(req.getParameter("studentusername"));
		stud.setPassword(req.getParameter("studentpassword"));
		stud.setName(req.getParameter("studentname"));
		stud.setSurname(req.getParameter("studentsurname"));
		
		String sex = req.getParameter("studentsexselect");

		if(sex.equals("M")) stud.setSex("muško");
		else stud.setSex("žensko");
		
		stud.setDateOfBirth(parseDate(req.getParameter("studentdateofbirth")));
		stud.setPlaceOfBirth(req.getParameter("studentplaceofbirth"));
		stud.setCurrentStatus(req.getParameter("studentcurrentstatus"));
		stud.setOIB(req.getParameter("studentoib"));
		
		Parents parents = new Parents();
		parents.setUsername(req.getParameter("parentusername"));
		parents.setPassword(req.getParameter("parentpassword"));
		parents.setMothersName(req.getParameter("studentmothername"));
		parents.setFathersName(req.getParameter("studentfathername"));
		parents.setChild(stud);
		
		StudentClass classOfStudent = DAOProvider.getDAO().getStudentClass(Integer.parseInt(req.getParameter("classselect")));
		
		stud.setParents(parents);
		stud.setClassOfStudent(classOfStudent);
		stud.setPicture(req.getParameter("studentpicture"));
		stud.setSchool(admin.getSchool());
		
		DAOProvider.getDAO().addOrUpdateStudent(stud);
		DAOProvider.getDAO().addOrUpdateParents(parents);
		
		resp.sendRedirect(req.getServletContext().getContextPath() + "/servlets/adminmain");
	}
	
	private Date parseDate(String date) {
		DateFormat format = new SimpleDateFormat("YYYY-MM-dd", Locale.ENGLISH);
		try {
			return format.parse(date);
		} catch (ParseException e) {
			throw new DAOException("Greška prilikom parsiranja datuma!");
		}
	} 
	
}
