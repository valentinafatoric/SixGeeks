package web.servlets.admin;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DAOProvider;
import model.Administrator;
import model.Professor;
import model.SubjectInfoForProf;

@WebServlet(name="editProfessorRespServlet", urlPatterns={"/servlets/editprofesoraction"})
public class EditProfessorRespServlet extends HttpServlet {

	/** Serial version UID */
	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.getSession().setAttribute("professors", DAOProvider.getDAO().getProfessors());
		Administrator admin = (Administrator) req.getSession().getAttribute("administrator");
		
		if(admin == null) {
			req.setAttribute("message", "Nedopušteni pristup!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}
		
		req.getRequestDispatcher("/WEB-INF/pages/admin/EditProfessor.jsp").forward(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{
		Administrator admin = (Administrator) req.getSession().getAttribute("administrator");
		
		if(admin == null) {
			req.setAttribute("message", "Nedopušteni pristup!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}
		
		String OIB = req.getParameter("profid");
		List<Professor> profList = DAOProvider.getDAO().getProfessorByOIB(OIB);
		Professor prof = profList.get(0);
		
		SubjectInfoForProf subjectInfo = DAOProvider.getDAO().getSubjectInfoForProfessor(prof, req.getParameter("schoolyearedit")).get(0);
		
		prof.setName(req.getParameter("profnameedit"));
		prof.setSurname(req.getParameter("profsurnameedit"));
		prof.setUsername(req.getParameter("profusernameedit"));
		prof.setPassword(req.getParameter("profpasswordedit"));
		prof.setOIB(req.getParameter("profoibedit"));
		prof.setPhoneNumber(req.getParameter("profphonenumberedit"));
		prof.setPicture(req.getParameter("profpictureedit"));
		prof.setSchool(admin.getSchool());
		 
		subjectInfo.setProfessor(prof);
		subjectInfo.setSubject(DAOProvider.getDAO().getSubject(Integer.parseInt(req.getParameter("subjectselectedit"))));
		subjectInfo.setSchoolYear(req.getParameter("schoolyearedit"));
		subjectInfo.setTeachingLetter(req.getParameter("profteachingletter"));
		subjectInfo.setCurriculum(req.getParameter("profcurriculum"));
		
		DAOProvider.getDAO().updateProfessor(prof);
		DAOProvider.getDAO().addOrUpdateSubjectInfoForProf(subjectInfo);
		
		resp.sendRedirect(req.getServletContext().getContextPath() + "/servlets/adminmain");

	}
	
	
}
