package web.servlets.admin;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DAOProvider;
import model.Administrator;
import model.Student;
import model.StudentClass;

@WebServlet(name="editStudentClassAction", urlPatterns={"/servlets/studentclassedit"})
public class EditStudentClassInfoServlet extends HttpServlet {

	/** Serial version UID */
	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Administrator admin = (Administrator) req.getSession().getAttribute("administrator");
		
		if(admin == null) {
			req.setAttribute("message", "Nedopušteni pristup!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}
		
		req.getRequestDispatcher("/WEB-INF/pages/admin/EditStudentClassInfo.jsp").forward(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{
		Administrator admin = (Administrator) req.getSession().getAttribute("administrator");
		
		if(admin == null) {
			req.setAttribute("message", "Nedopušteni pristup!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}
		
		String action = req.getParameter("action");
		StudentClass studentClass = DAOProvider.getDAO().getStudentClass(Integer.parseInt(req.getParameter("studentclassid")));
		
		if(action.equals("addstudent")) {
			List<Student> students = studentClass.getStudents();
			Student student = DAOProvider.getDAO().getStudentByOIB(req.getParameter("studentselectadd"));
			students.add(student);
			studentClass.setStudents(students);
			DAOProvider.getDAO().updateStudentClass(studentClass);
			
			student.setClassOfStudent(studentClass);
			DAOProvider.getDAO().addOrUpdateStudent(student);
		}
		
		if(action.equals("deletestudent")) {
			List<Student> students = studentClass.getStudents();
			Student student = DAOProvider.getDAO().getStudentByOIB(req.getParameter("studentselectdelete"));
			students.remove(student);
			studentClass.setStudents(students);
			
			DAOProvider.getDAO().updateStudentClass(studentClass);
		}
		
		if(action.equals("razrednik")) {
			studentClass.setProfessor(DAOProvider.getDAO().getProfessorByOIB(req.getParameter("razrednikselect")).get(0));
			
			DAOProvider.getDAO().updateStudentClass(studentClass);
		}
		
		if(action.equals("zamjenikrazrednik")) {
			studentClass.setDeputyProfessor(DAOProvider.getDAO().getProfessorByOIB(req.getParameter("zamjenikrazrednikselect")).get(0));
			
			DAOProvider.getDAO().updateStudentClass(studentClass);
		}
		
		req.setAttribute("studentclass", studentClass);
		req.setAttribute("studentclassstudents", studentClass.getStudents());
		req.setAttribute("students", DAOProvider.getDAO().getStudents());
		req.setAttribute("professors", DAOProvider.getDAO().getProfessors());
		req.getRequestDispatcher("/WEB-INF/pages/admin/EditStudentClassInfo.jsp").forward(req, resp);
		
	}
	
	
}
