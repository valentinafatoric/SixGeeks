package web.servlets.student;

import java.io.IOException;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DAOProvider;
import model.Student;
import model.SubjectGradesForStudent;

@WebServlet(name = "studentEDiaryServlet", urlPatterns = { "/servlets/student" })
public class StudentEDiaryServlet extends HttpServlet {

	/** Serial version UID */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Student student = (Student) req.getSession().getAttribute("student");

		if (student == null) {
			req.setAttribute("message", "Nedopušteni pristup!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}

		Map<String, List<SubjectGradesForStudent>> schoolYears = new LinkedHashMap<>();

		String schoolYear;

		Calendar c = Calendar.getInstance();
		int currentYear = c.get(Calendar.YEAR);

		if (c.get(Calendar.MONTH) < 9) {
			currentYear--;
		}

		while (true) {
			schoolYear = String.valueOf(currentYear) + "./" + String.valueOf(currentYear + 1) + ".";
			List<SubjectGradesForStudent> subjects = DAOProvider.getDAO().getSubjectsOfSchoolYearForStudent(student,
					schoolYear);

			if (subjects == null || subjects.isEmpty()) {
				break;
			}

			schoolYears.put(schoolYear, subjects);
			currentYear--;
		}

		req.setAttribute("schoolYears", schoolYears);

		req.getRequestDispatcher("/WEB-INF/pages/student/StudentEDiary.jsp").forward(req, resp);
	}

}
