package web.servlets.student;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DAOProvider;
import model.Consultation;
import model.Parents;
import model.Student;
import model.StudentClass;

@WebServlet(name = "ContactInformationProfServlet", urlPatterns = { "/servlets/contactinfoprof" })
public class ContactInformationProfServlet extends HttpServlet {

	/** Serial version UID */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Student student = (Student) req.getSession().getAttribute("student");

		if (student == null) {
			req.setAttribute("message", "Nedopušteni pristup!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}

		if ((Parents) req.getSession().getAttribute("parents") != null) {
			List<Consultation> consultations = DAOProvider.getDAO().getConsultations(student.getClassOfStudent(), false);
			List<Consultation> parentsMeetings = DAOProvider.getDAO().getConsultations(student.getClassOfStudent(), true);
	
			req.setAttribute("consultations", consultations);
			req.setAttribute("parentsMeetings", parentsMeetings);
		}

		StudentClass studentClass = DAOProvider.getDAO().getStudentClass(student.getClassOfStudent());
		
		if (studentClass == null) {
			req.setAttribute("message", "Neispravan zahtjev!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}
		
		req.setAttribute("classProfessor", studentClass.getProfessor());
		req.setAttribute("deputyProfessor", studentClass.getDeputyProfessor());

		req.getRequestDispatcher("/WEB-INF/pages/student/ContactInfoProf.jsp").forward(req, resp);
	}
}
