package web.servlets.student;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DAOProvider;
import model.Absence;
import model.Student;

@WebServlet(name = "studentAbsenceServlet", urlPatterns = { "/servlets/studentabsence" })
public class StudentAbsenceServlet extends HttpServlet {

	/** Serial version UID */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Student student = (Student) req.getSession().getAttribute("student");

		if (student == null) {
			req.setAttribute("message", "Nedopušteni pristup!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}

		List<Absence> absences = DAOProvider.getDAO().getAbsencesForStudent(student);

		if (absences != null) {
			req.setAttribute("absences", absences);
			req.setAttribute("numberOfAbsences", absences.size());

			int numberOfNonJustifiedAbsences = (int) absences.stream().filter(a -> !a.isJustifiably()).count();
			req.setAttribute("numberOfNonJustifiedAbsences", numberOfNonJustifiedAbsences);
			req.setAttribute("numberOfJustifiedAbsences", absences.size() - numberOfNonJustifiedAbsences);
		}

		req.getRequestDispatcher("/WEB-INF/pages/student/StudentAbsence.jsp").forward(req, resp);
	}

}
