package web.servlets.student;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DAOProvider;
import model.Absence;
import model.Comment;
import model.Student;
import model.Subject;
import model.SubjectGradesForStudent;
import model.TermOfLecture;

@WebServlet(name = "subjectForStudentServlet", urlPatterns = { "/servlets/subjectstudent/*" })
public class SubjectForStudentServlet extends HttpServlet {

	/** Serial version UID */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String pathInfo = req.getPathInfo().substring(1);

		String[] parts = pathInfo.split("/");

		if (parts.length != 3) {
			req.setAttribute("message", "Neispravan zahtjev!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}

		Student student = (Student) req.getSession().getAttribute("student");

		if (student == null) {
			req.setAttribute("message", "Nedopušteni pristup!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}

		String schoolYear = parts[0] + "/" + parts[1];
		
		try {
			Integer.parseInt(parts[2]);
		} catch (NumberFormatException ex) {
			req.setAttribute("message", "Neispravan zahtjev!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}

		Subject subject = DAOProvider.getDAO().getSubject(Integer.parseInt(parts[2]));
		
		if (subject == null) {
			req.setAttribute("message", "Neispravan zahtjev!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}

		SubjectGradesForStudent subjectGrades = DAOProvider.getDAO().getSubjectGradesForStudent(student, subject, schoolYear);

		List<TermOfLecture> terms = DAOProvider.getDAO().getTermsForSubject(subject, student.getClassOfStudent());
		
		if (terms == null || terms.isEmpty()) {
			req.setAttribute("message", "Neispravan zahtjev!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}

		req.setAttribute("subjectGrades", subjectGrades);
		req.setAttribute("terms", terms);
		req.setAttribute("termsSize", terms.size());
		req.setAttribute("termProfessor", terms.get(0).getProfessor());

		List<String> days = new LinkedList<>();

		days.add("ponedjeljak");
		days.add("utorak");
		days.add("srijedu");
		days.add("četvrtak");
		days.add("petak");
		days.add("subotu");
		days.add("nedjelju");

		req.setAttribute("days", days);

		List<Absence> absences = DAOProvider.getDAO().getAbsencesForStudent(student);

		req.setAttribute("absences", absences);

		List<Comment> comments = DAOProvider.getDAO().getCommentsForSubject(subjectGrades);

		req.setAttribute("comments", comments);

		req.getRequestDispatcher("/WEB-INF/pages/student/SubjectForStudent.jsp").forward(req, resp);
	}

}
