package web.servlets.student;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DAOProvider;
import model.Student;

@WebServlet(name = "mainStudentServlet", urlPatterns = { "/servlets/studentmain" })
public class MainStudentServlet extends HttpServlet {

	/** Serial version UID */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.getSession().setAttribute("notifications", DAOProvider.getDAO().getCurrentNotifications());

		Student student = (Student) req.getSession().getAttribute("student");

		if (student == null) {
			req.setAttribute("message", "Nedopušteni pristup!");
			req.getRequestDispatcher("/WEB-INF/pages/Error.jsp").forward(req, resp);
			return;
		}

		req.getSession().setAttribute("schedule", DAOProvider.getDAO().getSchedule(student));

		req.getRequestDispatcher("/WEB-INF/pages/student/StudentMain.jsp").forward(req, resp);
	}

}
