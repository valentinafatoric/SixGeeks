package web.forms;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import dao.DAOProvider;

public class LoginForm {

	private String username;

	private String password;

	private Map<String, String> errors = new HashMap<>();

	public LoginForm() {
	}

	public String getUsername() {
		return username;
	}

	public String getError(String name) {
		return errors.get(name);
	}

	
	public boolean hasErrors() {
		return !errors.isEmpty();
	}

	
	public boolean hasError(String name) {
		return errors.containsKey(name);
	}

	
	public void getFromHttpRequest(HttpServletRequest req) {
		this.username = convert(req.getParameter("username"));
		this.password = convert(req.getParameter("password"));
	}

	
	private String convert(String s) {
		if (s == null)
			return "";
		return s.trim();
	}

	
	public void validate() {
		errors.clear();

		if (this.username.isEmpty()) {
			errors.put("username", "Nedostaje korisničko ime!");
		}

		if (this.password.isEmpty()) {
			errors.put("password", "Nedostaje lozinka!");
		}

		if (DAOProvider.getDAO().getUser(username) == null) {
			errors.put("username", "Neispravno korisničko ime!");
			return;
		}

		if (passwordIsInvalid()) {
			errors.put("password", "Neispravna lozinka!");
		}
	}

	
	private boolean passwordIsInvalid() {
		return !password.equals(DAOProvider.getDAO().getUser(username).getPassword());
	}

}
