<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
	<head>
		<title>Pogreška</title>
		
		<style type="text/css">
		body {
        	margin-top:100px;
			font: 16px/1.4 "Helvetica Neue", Helvetica, Arial, sans-serif;
			color: red;
            text-align:center;
		}
		</style>
	</head>

	<body>
		<h1>Dogodila se pogreška!</h1>
		<p><c:out value="${message}"/></p>

		<p><a href="/eDnevnik">Povratak na početnu stanicu</a></p>
	</body>
</html>