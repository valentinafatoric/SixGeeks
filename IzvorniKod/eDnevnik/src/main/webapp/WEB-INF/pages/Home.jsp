<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
	<head>
		<title>eDnevnik</title>
		
		<style type="text/css">
		.error {
		   font: 13px/1.4 "Helvetica Neue", Helvetica, Arial, sans-serif;
		   color: #FF0000;
		}
		body {
			font: 18px/1.4 "Helvetica Neue", Helvetica, Arial, sans-serif;
			color: #395870;
		}
		.login {
		  padding: 20px 20px 0 20px;
		}
		.login label {
		  color: #395870;
		  display: block;
		  font-weight: bold;
		  font-size: 18px;
		  margin-bottom: 20px;
		}
		.login input {
		  background: #fff;
		  border: 1px solid #c6c7cc;
		   box-shadow: inset 0 1px 1px rgba(0, 0, 0, .1);
		  color: #636466;
		  padding: 6px;
		  margin-top: 6px;
		  width: 100%;
		}
		.login textarea {
		  background: #fff;
		  border: 1px solid #c6c7cc;
		   box-shadow: inset 0 1px 1px rgba(0, 0, 0, .1);
		  color: #636466;
		  padding: 6px;
		  margin-top: 6px;
		  width: 100%;
		}
		.login-action {
		  background: #f0f0f2;
		  border-top: 1px solid #c6c7cc;
		  padding: 20px;
		}
		.login-action .btn {
		  background: linear-gradient(#49708f, #293f50);
		  border: 0;
		  color: #fff;
		  cursor: pointer;
		  font-weight: bold;
		  float: left;
		  padding: 8px 16px;
		  margin-right: 30px;
		}
		</style>
	</head>

	<body>
		<h1>Prijava za eDnevnik</h1>
		
		<form action="/eDnevnik/servlets/login" method="post">
		<fieldset class="login">
		
		<label>
  			Korisničko ime
  			<input name="username" type="text" placeholder="korisničko ime" value="${loginform.username}">
  			<c:if test="${loginform.hasError('username')}">
				<div class="error"><c:out value="${loginform.getError('username')}"/></div>
			</c:if>
		</label>
		
		<label>
  			Lozinka
  			<input name="password" type="password" placeholder="lozinka" value="">
  			<c:if test="${loginform.hasError('password')}">
				<div class="error"><c:out value="${loginform.getError('password')}"/></div>
			</c:if>
		</label>
		</fieldset>
		  
		<fieldset class="login-action">
		    <input class="btn" type="submit" name="method" value="Prijava">
		</fieldset>
		
		</form>
		
	</body>
</html>