<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
	<head>
		<title>eDnevnik</title>
		
		<style type="text/css">
			<%@include file="/WEB-INF/style/style.css"%>
		h2,h3,h4{
			color:#001f4d;
			display: block;
			font-weight: bold;
		}
		
		.container1 {
		    border-radius: 5px;
		    background-color: #f2f2f2;
		    padding: 20px;
		}
		
		</style>
	</head>

	<body>
		<h1>eDnevnik</h1>
		
		<ul>
		<li><a href="/eDnevnik/servlets/studentmain">Početna stranica</a></li>
		<li><a href="/eDnevnik/servlets/student">Pregled eDnevnika</a></li>
		<li><a href="/eDnevnik/servlets/studentabsence">Pregled izostanaka</a></li>
		<li><a href="/eDnevnik/servlets/studentinfo">Pregled osobnih podataka</a></li>
		<li><a href="/eDnevnik/servlets/contactinfoprof">Pregled kontaktnih podataka profesora</a></li>
		<li><a href="/eDnevnik/servlets/log-out">Odjava</a></li>
		</ul>
		
		<h2>Osobni podaci učenika:</h2>
		
		<div class="container1">
		<p><b>Ime:</b> ${student.name}</p>
		<p><b>Prezime:</b> ${student.surname}</p>
		<p><b>OIB:</b> ${student.OIB}</p>
		<p><b>Spol:</b> ${student.sex}</p>
		<p><b>Datum rođenja: </b><fmt:formatDate value="${student.dateOfBirth}" pattern="dd.MM.yyyy." /></p>
		<p><b>Mjesto rođenja:</b> ${student.placeOfBirth}</p>
		<p><b>Fotografija:</b> <img src="${student.picture}" /></p>
		<p><b>Ime i prezime majke:</b> ${student.parents.mothersName}</p>
		<p><b>Ime i prezime oca:</b> ${student.parents.fathersName}</p>
		
		</div>
	</body>
</html>