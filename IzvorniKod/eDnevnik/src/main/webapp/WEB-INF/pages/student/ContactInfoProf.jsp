<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
	<head>
		<title>eDnevnik</title>
		
		<style type="text/css">
			<%@include file="/WEB-INF/style/style.css"%>
			
		h2,h3,h4{
			color:#001f4d;
			display: block;
			font-weight: bold;
		}
		.container1 {
		    border-radius: 5px;
		    background-color: #f2f2f2;
		    padding: 20px;
		}
		</style>
	</head>

	<body>
		<h1>eDnevnik</h1>
		
		<ul>
		<li><a href="/eDnevnik/servlets/studentmain">Početna stranica</a></li>
		<li><a href="/eDnevnik/servlets/student">Pregled eDnevnika</a></li>
		<li><a href="/eDnevnik/servlets/studentabsence">Pregled izostanaka</a></li>
		<li><a href="/eDnevnik/servlets/studentinfo">Pregled osobnih podataka</a></li>
		<li><a href="/eDnevnik/servlets/contactinfoprof">Pregled kontaktnih podataka profesora</a></li>
		<li><a href="/eDnevnik/servlets/log-out">Odjava</a></li>
		</ul>
		
		<h2>Kontaktni podatci profesora:</h2>
		
		<div class="container1">
		<h3>Razrednik:</h3>
		<p><b>Ime:</b> ${classProfessor.name}</p>
		<p><b>Prezime:</b> ${classProfessor.surname}</p>
		
		<br>
		
		<h3>Zamjenik razrednika:</h3>
		<p><b>Ime:</b> ${deputyProfessor.name}</p>
		<p><b>Prezime:</b> ${deputyProfessor.surname}</p>
		
		<br>
		
		<c:if test="${parents != null}">
			<c:if test="${parentsMeetings != null && !parentsMeetings.isEmpty()}">
				<h3>Roditeljski sastanci:</h3>
				
				<c:forEach var="meeting" items="${parentsMeetings}">
					<p><fmt:formatDate value="${meeting.dateAndTime}" pattern="dd.MM.yyyy. 'u' HH:mm" /> u prostoriji ${meeting.room.name}</p>
				</c:forEach>
				
				<br>
			</c:if>
			
			<c:if test="${consultations != null && !consultations.isEmpty()}">
				<h3>Konzultacije s razrednikom:</h3>
				
				<c:forEach var="meeting" items="${consultations}">
					<p><fmt:formatDate value="${meeting.dateAndTime}" pattern="dd.MM.yyyy. 'u' HH:mm" /> u prostoriji ${meeting.room.name}</p>
				</c:forEach>
				
				<br>
			</c:if>
			
			<p><b>Kontaktni broj razrednika:</b> ${classProfessor.phoneNumber}</p>
			<p><b>Kontaktni broj zamjenika razrednika:</b> ${deputyProfessor.phoneNumber}</p>
		</c:if>
		</div>
	</body>
</html>