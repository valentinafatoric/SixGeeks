<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
	<head>
		<title>eDnevnik</title>
		
		<style type="text/css">
			<%@include file="/WEB-INF/style/style.css"%>
			
		.listStudent {
		    background:#ffeb99;
		    padding: 20px;
		    display: block
		}
		
		
		
		.listStudent2 {
		    padding: 5px;
		    margin-left: 35px;
		    background: #ebfaeb;
		    margin: 5px;
		    display: block
		}
		
		/* unvisited link */
		.a1:link {
		    color:#395870;
		}
		
		/* visited link */
		.a1:visited {
		   color:#395870;
		}
		
		/* mouse over link */
		.a1:hover {
		    color:#000066;
		}
		
		/* selected link */
		.a1:active {
		    color:#395870;
		}
		</style>
	</head>

	<body>
		<h1>eDnevnik</h1>
		
		<ul>
		<li><a href="/eDnevnik/servlets/studentmain">Početna stranica</a></li>
		<li><a href="/eDnevnik/servlets/student">Pregled eDnevnika</a></li>
		<li><a href="/eDnevnik/servlets/studentabsence">Pregled izostanaka</a></li>
		<li><a href="/eDnevnik/servlets/studentinfo">Pregled osobnih podataka</a></li>
		<li><a href="/eDnevnik/servlets/contactinfoprof">Pregled kontaktnih podataka profesora</a></li>
		<li><a href="/eDnevnik/servlets/log-out">Odjava</a></li>
		</ul>
		
		<h2>Pregled nastavnih godina:</h2>
		
		<div class="listStudent">
		<c:forEach var="year" items="${schoolYears}">
			<h3>${year.key}</h3>
			
			<c:forEach var="value" items="${year.value}">
				<p class="listStudent2"><a class="a1" href="/eDnevnik/servlets/subjectstudent/${year.key}/${value.subject.id}">${value.subject.name}</a> :   
					<c:choose>
						<c:when test="${value.currentlyActive}">
							Upisan
						</c:when>
						<c:otherwise>
							${value.finalGrade}
						</c:otherwise>
					</c:choose>	
				</p>
			</c:forEach>
		</c:forEach>
		</div>
	</body>
</html>