<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
	<head>
		<title>eDnevnik</title>
		
		<style type="text/css">
			<%@include file="/WEB-INF/style/style.css"%>
		</style>
	</head>

	<body>
		<h1>eDnevnik</h1>
		
		<ul>
		<li><a href="/eDnevnik/servlets/studentmain">Početna stranica</a></li>
		<li><a href="/eDnevnik/servlets/student">Pregled eDnevnika</a></li>
		<li><a href="/eDnevnik/servlets/studentabsence">Pregled izostanaka</a></li>
		<li><a href="/eDnevnik/servlets/studentinfo">Pregled osobnih podataka</a></li>
		<li><a href="/eDnevnik/servlets/contactinfoprof">Pregled kontaktnih podataka profesora</a></li>
		<li><a href="/eDnevnik/servlets/log-out">Odjava</a></li>
		</ul>
		
		<h2>Izostanci učenika:</h2>
		
		<c:choose>
				<c:when test="${absences == null || absences.isEmpty()}">
					<p>Učenik nema izostanaka!</p>
				</c:when>
				<c:otherwise>
				
					<table>
						<colgroup>
							<col span="1" style="background-color:#80ff80">
						</colgroup>
						<tr>
						<th>Datum</th>
						<th>Sat</th>
						<th>Predmet</th>
						<th>Opravdano</th>
						</tr>
					
						<c:forEach var="absence" items="${absences}">
						
							<tr>
							<td><fmt:formatDate value="${absence.date}" pattern="dd.MM.yyyy." /></td>
							<td>${absence.term.ordinalNumberOfHour}.</td>
							<td>${absence.term.subject.name}</td>
							<c:choose>
								<c:when test="${absence.justifiably}">
									<td>opravdano</td>
								</c:when>
								<c:otherwise>
									<td>neopravdano</td>
								</c:otherwise>
							</c:choose>
							</tr>
							
						</c:forEach>
					
					</table>
					
					<p><b>Broj neopravdanih sati:</b> ${numberOfNonJustifiedAbsences} </p>
					<p><b>Broj opravdanih sati:</b> ${numberOfJustifiedAbsences} </p>
					<p><b>Ukupan broj izostanaka:</b> ${numberOfAbsences} </p>
		
			</c:otherwise>
		</c:choose>
		
	</body>
</html>