<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
	<head>
		<title>eDnevnik</title>
		
		<style type="text/css">
		body {
			font: 18px/1.4 "Helvetica Neue", Helvetica, Arial, sans-serif;
			color: #395870;
		}
		
		ul{
		list-style-type: none;
		margin:0;
		padding:0;
		overflow:hidden;
		background-color: #003A8E
		}
		
		li{
		float:left
		}
		
		li a{
		display: block;
		color:white;
		text-align: center;
		padding: 14px 16px;
		text-decoration: none;
		}
		
		li a:hover{
			background-color:#ffff66;
            color: #395870;
		}
		
		.active{
		background-color: #708090
		}
		
		table{
			border-collapse: collapse;
			width:100%;
		}
		
		th,td{
			text-alighn:left;
			padding: 8px;
		}
		
		tr:nth-child(even){
			background-color:#f2f2f2;
		}
		
		<!-- novo -->
		
		
		<%@include file="/WEB-INF/style/style.css"%>
		
		</style>
	</head>

	<body>
		
		<h1>eDnevnik</h1>
		
		<ul>
		<li><a href="/eDnevnik/servlets/admin">Početna stranica</a></li>
		
		<li class="dropdown"> 
			<a href="/eDnevnik/servlets/manipulatenotifications" class="dropbtn">Obavijesti</a>
		</li>
		
		<li class="dropdown"> 
			<a href="/eDnevnik/servlets/editlectures" class="dropbtn">Raspored sati</a>
		</li>
		
		<li class="dropdown"> 
			<a href="javascript:void(0)" class="dropbtn">Predmet</a>
				<div class="dropdown-content">
					<a href="/eDnevnik/servlets/editsubject">Prikaži listu predmeta</a>
					<a href="/eDnevnik/servlets/addsubject">Dodaj predmet</a>
				</div>
		</li>
		<li class="dropdown"> 
			<a href="javascript:void(0)" class="dropbtn">Profesor</a>
				<div class="dropdown-content">
					<a href="/eDnevnik/servlets/editprofesor">Prikaži listu profesora</a>
					<a href="/eDnevnik/servlets/addprofesor">Dodaj profesora</a>
				</div>
		</li>
		<li class="dropdown"> 
			<a href="javascript:void(0)" class="dropbtn">Razred/grupa</a>
				<div class="dropdown-content">
					<a href="/eDnevnik/servlets/studentclasslist">Prikaži i uredi razred/grupu</a>
					<a href="/eDnevnik/servlets/addstudentclass">Dodaj razred/grupu</a>
				</div>
		</li>
		<li class="dropdown"> 
			<a href="javascript:void(0)" class="dropbtn">Učenik</a>
				<div class="dropdown-content">
					<a href="/eDnevnik/servlets/studentlist">Prikaži listu učenika</a>
					<a href="/eDnevnik/servlets/addstudent">Dodaj učenika</a>
				</div>
		</li>
		<li> <a href="/eDnevnik/servlets/eventdiary">Dnevnik događaja</a></li>
		<li><a href="/eDnevnik/servlets/log-out">Odjava</a></li>
		</ul>	
		
		<br>
		
		<form action="/eDnevnik/servlets/addstudentclass" method="POST">
			<fieldset>
			<legend>Podaci o razredu:</legend>
			<p><label class="field" for="classname">Naziv razreda:</label> <input type="text" name="classname" required></p>
			<p><label class="field" for="classyear">Školska godina:</label> 
			<select name="classyear">
				<option value="2017./2018.">2017./2018.</option>
				<option value="2018./2019.">2018./2019.</option>
				<option value="2019./2020.">2019./2020.</option>
				<option value="2020./2021.">2020./2021.</option>
				<option value="2021./2022.">2021./2022.</option>
			</select>
			
			<p><label class="field" for="razrednikselect">Izaberi razrednika:</label>
			<select name="razrednikselect">
				<c:forEach var="professor" items="${professors}">
					<option value="${professor.OIB}">${professor.name} ${professor.surname}</option>
				</c:forEach>
			</select></p>
			
			<p><label class="field" for="zamjenikrazrednikselect">Izaberi zamjenika razrednika:</label>
			<select name="zamjenikrazrednikselect">
				<c:forEach var="professor" items="${professors}">
					<option value="${professor.OIB}">${professor.name} ${professor.surname}</option>
				</c:forEach>
			</select><br><br><br>
			<button type="submit" class="button" name="action" value="edit">Dodaj</button>
			</fieldset>
		</form>
			
	</body>
</html>