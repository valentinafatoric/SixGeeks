<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
	<head>
		<title>eDnevnik</title>
		
		<style type="text/css">
		body {
			font: 18px/1.4 "Helvetica Neue", Helvetica, Arial, sans-serif;
			color: #395870;
		}
		
		ul{
		list-style-type: none;
		margin:0;
		padding:0;
		overflow:hidden;
		background-color: #003A8E
		}
		
		li{
		float:left
		}
		
		li a{
		display: block;
		color:white;
		text-align: center;
		padding: 14px 16px;
		text-decoration: none;
		}
		
		li a:hover{
			background-color:#ffff66;
            color: #395870;
		}
		
		.active{
		background-color: #708090
		}
		
		table{
			border-collapse: collapse;
			width:100%;
		}
		
		th,td{
			text-alighn:left;
			padding: 8px;
		}
		
		tr:nth-child(even){
			background-color:#f2f2f2;
		}
		
		<%@include file="/WEB-INF/style/style.css"%>
		
		</style>
		
		<script>
			function checkOIB(){
				var OIB = document.getElementById("profoibedit").value;

				if(OIB.length != 11){
					document.getElementById("errormessage").innerHTML = "OIB mora sadržavati 11 znamenaka!";
					return false;
				}

				return true;
			}
		</script>
	</head>

	<body>
		
		<h1>eDnevnik</h1>
		
		<ul>
		<li><a href="/eDnevnik/servlets/admin">Početna stranica</a></li>
		
		<li class="dropdown"> 
			<a href="/eDnevnik/servlets/manipulatenotifications" class="dropbtn">Obavijesti</a>
		</li>
		
		<li class="dropdown"> 
			<a href="/eDnevnik/servlets/editlectures" class="dropbtn">Raspored sati</a>
		</li>
		
		<li class="dropdown"> 
			<a href="javascript:void(0)" class="dropbtn">Predmet</a>
				<div class="dropdown-content">
					<a href="/eDnevnik/servlets/editsubject">Prikaži listu predmeta</a>
					<a href="/eDnevnik/servlets/addsubject">Dodaj predmet</a>
				</div>
		</li>
		<li class="dropdown"> 
			<a href="javascript:void(0)" class="dropbtn">Profesor</a>
				<div class="dropdown-content">
					<a href="/eDnevnik/servlets/editprofesor">Prikaži listu profesora</a>
					<a href="/eDnevnik/servlets/addprofesor">Dodaj profesora</a>
				</div>
		</li>
		<li class="dropdown"> 
			<a href="javascript:void(0)" class="dropbtn">Razred/grupa</a>
				<div class="dropdown-content">
					<a href="/eDnevnik/servlets/studentclasslist">Prikaži i uredi razred/grupu</a>
					<a href="/eDnevnik/servlets/addstudentclass">Dodaj razred/grupu</a>
				</div>
		</li>
		<li class="dropdown"> 
			<a href="javascript:void(0)" class="dropbtn">Učenik</a>
				<div class="dropdown-content">
					<a href="/eDnevnik/servlets/studentlist">Prikaži listu učenika</a>
					<a href="/eDnevnik/servlets/addstudent">Dodaj učenika</a>
				</div>
		</li>
		<li> <a href="/eDnevnik/servlets/eventdiary">Dnevnik događaja</a></li>
		<li><a href="/eDnevnik/servlets/log-out">Odjava</a></li>
		</ul>
		
		<br>
		
		<form action="/eDnevnik/servlets/editprofesoraction" method="POST">
		<fieldset>
		<legend>Unesite podatke o profesoru:</legend>
			<p><label class="field" for="profnameedit">Ime:</label> <input type="text" name="profnameedit" value="${professor1.name}" maxlength="45" required><br>
			<p><label class="field" for="profsurnameedit">Prezime:</label> <input type="text" name="profsurnameedit" value="${professor1.surname}" maxlength="45" required><br>
			<p><label class="field" for="profusernameedit">Korisničko ime:</label> <input type="text" name="profusernameedit" value="${professor1.username}" maxlength="25" required><br>
			<p><label class="field" for="profpasswordedit">Lozinka:</label> <input type="password" name="profpasswordedit" value="${professor1.password }" maxlength="45" required><br>
			<p><label class="field" for="profoibedit">OIB:</label> <input type="text" id="profoibedit" name="profoibedit" value="${professor1.OIB }" maxlength="11" required><br>
			<p><label class="field" for="profphonenumberedit">Broj telefona:</label> <input type="text" name="profphonenumberedit" value="${professor1.phoneNumber }" maxlength="15" required><br>
			<p><label class="field" for="profpictureedit">URL slike:</label> <input type="text" name="profpictureedit" value="${professor1.picture }" maxlength="500" required><br>
			
			<p><label class="field" for="subjectselectedit">Predmet:</label> 
			<select name="subjectselectedit">
				<c:forEach var="subject" items="${subjects}">
					<option value="${subject.id}">${subject.name}</option>
				</c:forEach>
			</select>
			
			<p><label class="field" for="profteachingletter">URL nastavnog pisma:</label> <input type="text" name="profteachingletter" value="${professorsubinfo.teachingLetter}" maxlength="500" required><br>
			<p><label class="field" for="profcurriculum">URL nastavnog plana:</label> <input type="text" name="profcurriculum" value="${professorsubinfo.curriculum}" maxlength="500" required><br>
			
			<p><label class="field" for="schoolyearedit">Školska godina:</label> 
			<select name="schoolyearedit">
				<option value="2017./2018.">2017./2018.</option>
				<option value="2018./2019.">2018./2019.</option>
				<option value="2019./2020.">2019./2020.</option>
				<option value="2020./2021.">2020./2021.</option>
				<option value="2021./2022.">2021./2022.</option>
			</select>
			<p class="error" id="errormessage"></p>
		</fieldset>
			<input type="hidden" class="button" name="profid" value="${professor1.OIB}">
			<button type="submit" class="button" name="action" value="edit" onClick="return checkOIB()">Uredi</button>
		</form>
			
	</body>
</html>