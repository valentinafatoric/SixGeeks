﻿<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
	<head>
		<title>eDnevnik</title>
		
		<style type="text/css">
		body {
			font: 18px/1.4 "Helvetica Neue", Helvetica, Arial, sans-serif;
			color: #395870;
		}
		
		ul{
		list-style-type: none;
		margin:0;
		padding:0;
		overflow:hidden;
		background-color: #003A8E
		}
		
		li{
		float:left
		}
		
		li a{
		display: block;
		color:white;
		text-align: center;
		padding: 14px 16px;
		text-decoration: none;
		}
		
		li a:hover{
			background-color:#ffff66;
            color: #395870;
		}
		
		.active{
		background-color: #708090
		}
		
		table{
			border-collapse: collapse;
			width:100%;
		}
		
		th,td{
			text-align:left;
			padding: 8px;
		}
		
		tr:nth-child(even){
			background-color:#f2f2f2;
		}
		
/*  		.container1{
			background-color: #f2f2f2;
		    color:grey;
		    width:700px;
		    height:250px;
		    border-style:solid;
		    border-color:grey;
		}  */
		
		.container1{
			position:relative;
		    width:700px;
		    height:250px;
		}
		hr {
		    width: 700px;
		    float: left;
		    border: 0;
		    height: 2px;
		    background: orange;
		    border-color: orange;
		    opacity: 0.5;
		}
		.p1{
			bottom: 0;
			right: 0; 
			position: absolute;
		}
		
		#ItemPreview {
			float:right;
		    border:2px solid gray;
		}
		
		<%@include file="/WEB-INF/style/style.css"%>
		
		</style>
	</head>

	<body>
		
		<h1>eDnevnik</h1>
		
		<ul>
		<li><a href="/eDnevnik/servlets/admin">Početna stranica</a></li>
		
		<li class="dropdown"> 
			<a href="/eDnevnik/servlets/manipulatenotifications" class="dropbtn">Obavijesti</a>
		</li>
		
		<li class="dropdown"> 
			<a href="/eDnevnik/servlets/editlectures" class="dropbtn">Raspored sati</a>
		</li>
		
		<li class="dropdown"> 
			<a href="javascript:void(0)" class="dropbtn">Predmet</a>
				<div class="dropdown-content">
					<a href="/eDnevnik/servlets/editsubject">Prikaži listu predmeta</a>
					<a href="/eDnevnik/servlets/addsubject">Dodaj predmet</a>
				</div>
		</li>
		<li class="dropdown"> 
			<a href="javascript:void(0)" class="dropbtn">Profesor</a>
				<div class="dropdown-content">
					<a href="/eDnevnik/servlets/editprofesor">Prikaži listu profesora</a>
					<a href="/eDnevnik/servlets/addprofesor">Dodaj profesora</a>
				</div>
		</li>
		<li class="dropdown"> 
			<a href="javascript:void(0)" class="dropbtn">Razred/grupa</a>
				<div class="dropdown-content">
					<a href="/eDnevnik/servlets/studentclasslist">Prikaži i uredi razred/grupu</a>
					<a href="/eDnevnik/servlets/addstudentclass">Dodaj razred/grupu</a>
				</div>
		</li>
		<li class="dropdown"> 
			<a href="javascript:void(0)" class="dropbtn">Učenik</a>
				<div class="dropdown-content">
					<a href="/eDnevnik/servlets/studentlist">Prikaži listu učenika</a>
					<a href="/eDnevnik/servlets/addstudent">Dodaj učenika</a>
				</div>
		</li>
		<li> <a href="/eDnevnik/servlets/eventdiary">Dnevnik događaja</a></li>
		<li><a href="/eDnevnik/servlets/log-out">Odjava</a></li>
		</ul>	
		<br>
		<div style="width: 750px">
		<h2 style="display: inline;">Aktualne obavijesti:</h2>
		</div>
				<br>
		
		<c:choose>
					<c:when test="${notifications == null || notifications.isEmpty()}">
					
						<p>Nema trenutnih obavijesti.</p>
					
					</c:when>
					<c:otherwise>
						<c:forEach var="notification" items="${notifications}">
		
							<hr>
							<div class="container1">
								<c:if test="${notification.picture != null}">
									<img id="ItemPreview" src="${notification.picture}" />
								</c:if>		
								<h3 style="float: top: ;">${notification.title}</h3>
								
								<c:if test="${notification.text != null}">
									<p style="">${notification.text}</p>
								</c:if>
								<br><br>
								
								<div style="display: inline;">
								
								<p class="p1"><fmt:formatDate value="${notification.date}" pattern="dd.MM.yyyy. HH:mm" /> ${notification.creator.name} ${notification.creator.surname}</p>
								</div>
								
							</div>
							
						</c:forEach>

						<hr>

					</c:otherwise>
				</c:choose>
	
	</body>
	
</html>