<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
	<form id="lectureaddform" action="/eDnevnik/servlets/addlecture" method="POST">
			
			<fieldset>
			
			<legend>Predavanje za razred ${studentClass} u ${param.daytext} ${param.hour}. sat.</legend>
			<input hidden id="classselect" name="classselect" value="${param.studentClassId}" >
			<input hidden id="dayselect" name="dayselect" value="${param.day}">  
			<input hidden id="hourselect" name="hourselect" value="${param.hour}">
			
			<p><label class="field" for="subjectselect">Predmet:</label>   
			<select id="subjectselect" name="subjectselect" onchange="updateProfs()" >
			
				<option value="" selected disabled >Odaberite predmet</option>
				<c:forEach var="subject" items="${subjects}">
					<option value="${subject.id}">${subject.name}</option>
				</c:forEach>
			</select>
			<br>
			
			<p><label class="field" for="profselect">Profesor:</label> 
			<select id="profselect" name="profselect" >
				
			</select>
			<br>
			
			
			<p><label class="field" for="roomselect">Učionica:</label>   
			<select id="roomselect" name="roomselect" >
				<option value="" selected disabled >Odaberite učionicu</option>
				<c:forEach var="room" items="${rooms}">
					<option value="${room.id}">${room.name}</option>
				</c:forEach>
			</select>
			<br>
			</fieldset>
			<button type="submit" class="button" name="action" onclick="return checkForm()">Dodaj</button>
			<!-- <button class="button" type="button">Odustani</button> -->

		</form>