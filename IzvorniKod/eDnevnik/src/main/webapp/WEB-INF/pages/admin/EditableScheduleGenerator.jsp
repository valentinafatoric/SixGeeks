<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import = "java.io.*,java.util.*" %>
<%@ page import = "javax.servlet.*,java.text.*" %>
<%@ page import="dao.*" %>
<%@ page import="model.*" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%! 
	public String showTerms(Map<String, TermOfLecture> terms, String day, int dayInt) {
		String out = "";
		
		for(int i = 1; i <= 7; i++) {
			
			
			TermOfLecture term = terms.get(String.valueOf(i));
			if(term != null) {
				out += String.format("<td  class=\"populated\" onclick=\"deleteTerm('%d')\">", term.getId());
				out += term.getSubject().getName() + "<br>" + term.getRoom().getName() + "<br>" + term.getProfessor().getName() + " " +
						term.getProfessor().getSurname();

			} else {
				out += String.format("<td  class=\"empty\" onclick=\"addTerm('%s',%d,%d)\">",day, dayInt, i);
				out += "&nbsp;";
			}
			out += "</td>";
		}
		
		return out;
	}

%>

<table id="scheduletable">
			<colgroup>
				<col class="schedulecol"span="1" style="background-color:#80ff80">
				<col class="schedulecol">
				<col class="schedulecol">
				<col class="schedulecol">
				<col class="schedulecol">
				<col class="schedulecol">
				<col class="schedulecol">
				<col class="schedulecol">
				
			</colgroup>
			<tr>
				<th>Dan u tjednu</th>
				<th>1.sat</th>
				<th>2.sat</th>
				<th>3.sat</th>
				<th>4.sat</th>
				<th>5.sat</th>
				<th>6.sat</th>
				<th>7.sat</th>
			</tr>
			
			<tr>
				<td><font color="#595959">Ponedjeljak</font></td>
				
				<% 
					Map<String, Map<String, TermOfLecture>> schedule = (Map<String, Map<String, TermOfLecture>>) request.getAttribute("schedule");
					out.print(showTerms(schedule.get("1"), "ponedjeljak",1));
				%>
			</tr>
			<tr>
				<td><font color="#595959">Utorak</font></td>
				
				<% 
					out.print(showTerms(schedule.get("2"), "utorak",2));
				%>
			</tr>
			<tr>
				<td><font color="#595959">Srijeda</font></td>
				<% 
					out.print(showTerms(schedule.get("3"), "srijeda",3));
				%>
			</tr>
			<tr>
				<td><font color="#595959">Četvrtak</font></td>
				<% 
					out.print(showTerms(schedule.get("4"), "četvrtak",4));
				%>
			</tr>
			<tr>
				<td><font color="#595959">Petak</font></td>
				
				<% 
					out.print(showTerms(schedule.get("5"), "petak", 5));
				%>
			</tr>
		</table>>