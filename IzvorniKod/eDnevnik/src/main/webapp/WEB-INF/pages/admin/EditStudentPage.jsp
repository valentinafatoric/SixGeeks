<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
	<head>
		<title>eDnevnik</title>
		
		<style type="text/css">
		body {
			font: 18px/1.4 "Helvetica Neue", Helvetica, Arial, sans-serif;
			color: #395870;
		}
		
		ul{
		list-style-type: none;
		margin:0;
		padding:0;
		overflow:hidden;
		background-color: #003A8E
		}
		
		li{
		float:left
		}
		
		li a{
		display: block;
		color:white;
		text-align: center;
		padding: 14px 16px;
		text-decoration: none;
		}
		
		li a:hover{
			background-color:#ffff66;
            color: #395870;
		}
		
		.active{
		background-color: #708090
		}
		
		table{
			border-collapse: collapse;
			width:100%;
		}
		
		th,td{
			text-alighn:left;
			padding: 8px;
		}
		
		tr:nth-child(even){
			background-color:#f2f2f2;
		}
		
		<!-- Novo -->
		fieldset {
			width: 500px;
		}
		
		legend{
			font-size: 20px;
		}
		
		label.field{
			text-align: left;
			width: 200px;
			float: left;
		}
		
		p.error{
			color: red;
		}
		
		
		fieldset p {
			clear: both;
		}
		
		<%@include file="/WEB-INF/style/style.css"%>
		
		</style>
		
		<script>

			function validateInput(){
				var oib = document.getElementById("studentoib").value;
				if(oib.length != 11){ 
					document.getElementById("errormessage").innerHTML = "OIB mora imati 11 znamenaka!";
					return false;
				}
			}
			
		</script>
	</head>

	<body>
		
		<h1>eDnevnik</h1>
		
		<ul>
		<li><a href="/eDnevnik/servlets/admin">Početna stranica</a></li>
		
		<li class="dropdown"> 
			<a href="/eDnevnik/servlets/manipulatenotifications" class="dropbtn">Obavijesti</a>
		</li>
		
		<li class="dropdown"> 
			<a href="/eDnevnik/servlets/editlectures" class="dropbtn">Raspored sati</a>
		</li>
		
		<li class="dropdown"> 
			<a href="javascript:void(0)" class="dropbtn">Predmet</a>
				<div class="dropdown-content">
					<a href="/eDnevnik/servlets/editsubject">Prikaži listu predmeta</a>
					<a href="/eDnevnik/servlets/addsubject">Dodaj predmet</a>
				</div>
		</li>
		<li class="dropdown"> 
			<a href="javascript:void(0)" class="dropbtn">Profesor</a>
				<div class="dropdown-content">
					<a href="/eDnevnik/servlets/editprofesor">Prikaži listu profesora</a>
					<a href="/eDnevnik/servlets/addprofesor">Dodaj profesora</a>
				</div>
		</li>
		<li class="dropdown"> 
			<a href="javascript:void(0)" class="dropbtn">Razred/grupa</a>
				<div class="dropdown-content">
					<a href="/eDnevnik/servlets/studentclasslist">Prikaži i uredi razred/grupu</a>
					<a href="/eDnevnik/servlets/addstudentclass">Dodaj razred/grupu</a>
				</div>
		</li>
		<li class="dropdown"> 
			<a href="javascript:void(0)" class="dropbtn">Učenik</a>
				<div class="dropdown-content">
					<a href="/eDnevnik/servlets/studentlist">Prikaži listu učenika</a>
					<a href="/eDnevnik/servlets/addstudent">Dodaj učenika</a>
				</div>
		</li>
		<li> <a href="/eDnevnik/servlets/eventdiary">Dnevnik događaja</a></li>
		<li><a href="/eDnevnik/servlets/log-out">Odjava</a></li>
		</ul>	
		
		<br>
		
		<form action="/eDnevnik/servlets/editstudent" method="POST">
			<fieldset>
			<legend>Uređivanje podataka: ${student1.name} ${student1.surname} </legend>
			<p><label class="field" for="studentusernameedit">Korisničko ime:</label> <input type="text" name="studentusernameedit" value="${student1.username}" required maxlength="25"></p>
			<p><label class="field" for="studentpasswordedit">Lozinka</label> <input type="password" name="studentpasswordedit" value="${student1.password}" required maxlength="45"></p>
			<p><label class="field" for="studentnameedit">Ime:</label><input type="text" name="studentname" value="${student1.name}" required maxlength="45"></p>
			<p><label class="field" for="studentsurnameedit">Prezime:</label><input type="text" name="studentsurname" value="${student1.surname}"required maxlength="45"></p>
			<p><label class="field" for="studentsexselect">Spol:</label>
			<select name="studentsexselect">
				<option value="M">Muško</option>
				<option value="F">Žensko</option>
			</select></p>
			<p><label class="field" for="studentdateofbirth">Datum rođenja:</label> <input type="date" name="studentdateofbirth" value="${student1.dateOfBirth}" required></p>
			<p><label class="field" for="studentplaceofbirth">Mjesto rođenja:</label><input type="text" name="studentplaceofbirth" value="${student1.placeOfBirth}" required  maxlength="45"></p>
			<p><label class="field" for="studentcurrentstatus">Trenutni status:</label><input type="text" name="studentcurrentstatus" value="${student1.currentStatus}" required maxlength="45"><br>
			<p><label class="field" for="studentoib">OIB:</label><input type="text" id="studentoib" name="studentoib" value="${student1.OIB}" required maxlength="11"></p>
			<p><label class="field" for="studentpicture">URL slike:</label> <input type="text" name="studentpicture" value="${student1.picture}" maxlength="500" required><br>
			<p><label class="field" for="studentmothername">Ime majke:</label><input type="text" name="studentmothername" value="${student1.parents.mothersName}" required maxlength="100"></p>
			<p><label class="field" for="studentfathername">Ime oca:</label><input type="text" name="studentfathername" value="${student1.parents.fathersName}" required maxlength="100"></p>
			<p><label class="field" for="parentsusername">Korisničko ime roditelja:</label><input type="text" name="parentsusername" value="${student1.parents.username}" required maxlength="25"></p>
			<p><label class="field" for="parentspassword">Lozinka roditelja:</label><input type="password" name="parentspassword" value="${student1.parents.password }" required maxlength="45"></p>
			<p><label class="field" for="classselect">Razred:</label>
			<select name="classselect">
				<c:forEach var="studentclass" items="${studentclasses}">
					<option value="${studentclass.id}">${studentclass.name}</option>
				</c:forEach>
			</select></p>
			
			<input type="hidden" name="studentid" value="${student1.OIB}">
			<p class="error" id="errormessage"></p>
		</fieldset>
			<button type="submit" class="button" name="action" value="edit" onClick="return validateInput()">Uredi</button>
			
		</form>
			
	</body>
</html>