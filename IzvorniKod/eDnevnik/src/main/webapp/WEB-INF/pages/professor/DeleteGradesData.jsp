<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
	<head>
		<title>eDnevnik</title>
		
		<style type="text/css">
		
				.container1 {
			    border-radius: 5px;
			    background-color: #f2f2f2;
			    padding: 20px;
			}
			
			h2,h3,h4{
				color:#001f4d;
				display: block;
				font-weight: bold;
			}
		body {
			font: 18px/1.4 "Helvetica Neue", Helvetica, Arial, sans-serif;
			color: #395870;
		}
		
		ul{
		list-style-type: none;
		margin:0;
		padding:0;
		overflow:hidden;
		background-color: #003A8E
		}
		
		li{
		float:left
		}
		
		li a{
		display: block;
		color:white;
		text-align: center;
		padding: 14px 16px;
		text-decoration: none;
		}
		
		li a:hover{
			background-color:#ffff66;
            color: #395870;
		}
		
		.active{
		background-color: #708090
		}
		
		table{
			border-collapse: collapse;
			width:100%;
		}
		
		th,td{
			text-alighn:left;
			padding: 8px;
		}
		
		tr:nth-child(even){
			background-color:#f2f2f2;
		}
		
		.container {
    		display: block;
		    position: relative;
		    padding-left: 35px;
		    margin-bottom: 12px;
		    cursor: pointer;
		    font-size: 22px;
		    -webkit-user-select: none;
		    -moz-user-select: none;
		    -ms-user-select: none;
		    user-select: none;
		}
		
		/* Hide the browser's default checkbox */
		.container input {
		    position: absolute;
		    opacity: 0;
		    cursor: pointer;
		}
		
		/* Create a custom checkbox */
		.checkmark {
		    position: absolute;
		    top: 0;
		    left: 0;
		    height: 25px;
		    width: 25px;
		    background-color: #eee;
		}
		
		/* On mouse-over, add a grey background color */
		.container:hover input ~ .checkmark {
		    background-color: #ccc;
		}
		
		/* When the checkbox is checked, add a blue background */
		.container input:checked ~ .checkmark {
		    background-color: #00ff00;
		}
		
		/* Create the checkmark/indicator (hidden when not checked) */
		.checkmark:after {
		    content: "";
		    position: absolute;
		    display: none;
		}
		
		/* Show the checkmark when checked */
		.container input:checked ~ .checkmark:after {
		    display: block;
		}
		
		/* Style the checkmark/indicator */
		.container .checkmark:after {
		    left: 9px;
		    top: 5px;
		    width: 5px;
		    height: 10px;
		    border: solid white;
		    border-width: 0 3px 3px 0;
		    -webkit-transform: rotate(45deg);
		    -ms-transform: rotate(45deg);
		    transform: rotate(45deg);
		}
		.button {
		    background-color: #1f3d7a; /* Green background */
		    border: 2px solid white; /* Green border */
		    color: white; /* White text */
		    padding: 10px 24px; /* Some padding */
		    cursor: pointer; /* Pointer/hand icon */
		    float: left; /* Float the buttons side by side */
		}
		
		/* Clear floats (clearfix hack) */
		.button:after {
		    content: "";
		    clear: both;
		    display: table;
		}
		
		.button:not(:last-child) {
		    border-right: none; /* Prevent double borders */
		}

		/* Add a background color on hover */
		.button:hover {
		    background-color: #4976d0;
		}
		
		<%@include file="/WEB-INF/style/style.css"%>
	
		</style>
	</head>

	<body>
		
		<h1>eDnevnik</h1>
		
		<ul>
		<li><a href="/eDnevnik/servlets/professormain">Početna stranica</a></li>
		<li><a href="/eDnevnik/servlets/professorsubjects">Pregled nastavnih predmeta</a></li>
		<li><a href="/eDnevnik/servlets/professorabsencemain">Izostanci</a></li>
		<li><a href="/eDnevnik/servlets/professorinfo">Pregled osobnih podataka</a></li>
		<li> <a href="/eDnevnik/servlets/professorarchive">Arhiva</a></li>
		<c:choose>
			<c:when test="${deputy == true}">
				<li class="dropdown"> 
				<a href="javascript:void(0)" class="dropbtn">Razredništvo</a>
					<div class="dropdown-content">
						<a href="/eDnevnik/servlets/absencejustify">Opravdavanje izostanaka</a>
						<a href="/eDnevnik/servlets/myclass">Moj razred</a>
					</div>
				</li>
			</c:when>
		</c:choose>
		<li><a href="/eDnevnik/servlets/log-out">Odjava</a></li>
		</ul>	
		
		<form action="/eDnevnik/servlets/delgradesdata/${pathInfo}" method="post">
		
			<h3>${studentGrades.student.name} iz predmeta ${studentGrades.subject.name}</h3>
			
			<div class="container1">
			<h4>Označite ocjene koje želite izbrisati</h4>
			
			<c:choose>
			<c:when test="${studentGrades.grades == null || studentGrades.grades.isEmpty()}">
				
				<p>Student nema ocjena.</p>			
			
			</c:when>
			<c:otherwise>
				
				<c:forEach var="grade" items="${studentGrades.grades}">
				<label class="container">Ocjena: <b>${grade.value}</b>, komponenta: ${grade.component}, datum: <fmt:formatDate value="${grade.date}" pattern="dd.MM.yyyy. HH:mm" /> 
			  		<input type="checkbox" name="checkedGrades" value="<fmt:formatDate value="${grade.date}" pattern="yyyy-MM-dd HH:mm:ss.SSS" />">
			  		<span class="checkmark"></span>
				</label>
				</c:forEach>
			
			</c:otherwise>
			</c:choose>
			
			<h4>Označite komentare koje želite izbrisati</h4>
			
			<c:choose>
			<c:when test="${studentGrades.comments == null || studentGrades.comments.isEmpty()}">
				
				<p>Student nema komentara.</p>			
			
			</c:when>
			<c:otherwise>
				
				<c:forEach var="comment" items="${studentGrades.comments}">
					<label class="container">Komentar: <b>${comment.text}</b>, datum: <fmt:formatDate value="${comment.date}" pattern="dd.MM.yyyy. HH:mm" /> 
				  		<input type="checkbox"  name="checkedComments" value="<fmt:formatDate value="${comment.date}" pattern="yyyy-MM-dd HH:mm:ss.SSS" />">
				  		<span class="checkmark"></span>
					</label>
				</c:forEach>
			
			</c:otherwise>
			</c:choose>
			
			<h4>Označite ako želite obrisati zaključnu ocjenu</h4>
			
			<label class="container"><b>brisanje zaključne ocjene</b>
				  <input type="checkbox"  name="checkedFinalGrade" value="check">
				  <span class="checkmark"></span>
			</label>

			 <input type="submit" value="Završi" class=button style="width:15%">
			 </div>
		</form>
		
	</body>
	</html>