<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
	<head>
		<title>eDnevnik</title>
		
		<style type="text/css">
		body {
			font: 18px/1.4 "Helvetica Neue", Helvetica, Arial, sans-serif;
			color: #395870;
		}
		
		ul{
		list-style-type: none;
		margin:0;
		padding:0;
		overflow:hidden;
		background-color: #003A8E
		}
		
		li{
		float:left
		}
		
		li a{
		display: block;
		color:white;
		text-align: center;
		padding: 14px 16px;
		text-decoration: none;
		}
		
		li a:hover{
			background-color:#ffff66;
            color: #395870;
		}
		
		.active{
		background-color: #708090
		}
		
		table{
			border-collapse: collapse;
			width:100%;
		}
		
		th,td{
			text-alighn:left;
			padding: 8px;
		}
		
		tr:nth-child(even){
			background-color:#f2f2f2;
		}
		
		.btn-group button {
		    background-color: #1f3d7a; /* background */
		    border: 2px solid white; /* border */
		    color: white; /* White text */
		    border-radius: 4px;
		    padding: 10px 24px; /* Some padding */
		    cursor: pointer; /* Pointer/hand icon */
		    float: left; /* Float the buttons side by side */
		}
		
		/* Clear floats (clearfix hack) */
		.btn-group:after {
		    content: "";
		    clear: both;
		    display: table;
		}
		
		.btn-group button:not(:last-child) {
		    border-right: none; /* Prevent double borders */
		}
		
		/* Add a background color on hover */
		.btn-group button:hover {
		    background-color: #4976d0;
		}
		
		table {
		    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
		    border-collapse: collapse;
		    width: 60%;
		}
		
		td, th {
		    border: 1px solid #ddd;
		    padding: 20px;
		}
		tr:nth-child(even){background-color: #f2f2f2;}
		
		tr:hover {background-color: #ddd;}
		
		th {
		    padding-top: 12px;
		    padding-bottom: 12px;
		    text-align: left;
		    background-color: #4CAF50;
		    color: white;
		    }
		    
		    <%@include file="/WEB-INF/style/style.css"%>
		    
		</style>
	</head>

	<body>
		
		<h1>eDnevnik</h1>
		
		<ul>
		<li><a href="/eDnevnik/servlets/professormain">Početna stranica</a></li>
		<li><a href="/eDnevnik/servlets/professorsubjects">Pregled nastavnih predmeta</a></li>
		<li><a href="/eDnevnik/servlets/professorabsencemain">Izostanci</a></li>
		<li><a href="/eDnevnik/servlets/professorinfo">Pregled osobnih podataka</a></li>
		<li> <a href="/eDnevnik/servlets/professorarchive">Arhiva</a></li>
		<c:choose>
			<c:when test="${deputy == true}">
				<li class="dropdown"> 
				<a href="javascript:void(0)" class="dropbtn">Razredništvo</a>
					<div class="dropdown-content">
						<a href="/eDnevnik/servlets/absencejustify">Opravdavanje izostanaka</a>
						<a href="/eDnevnik/servlets/myclass">Moj razred</a>
					</div>
				</li>
			</c:when>
		</c:choose>
		<li><a href="/eDnevnik/servlets/log-out">Odjava</a></li>
		</ul>	
		
		<table>
			<tr>
			    <td width="20%">Zalaganje</td>
			    
			    <td>
			    <c:forEach var="grade" items="${studentgrades.grades}">
				<c:choose>
					<c:when test="${grade.component == 'zalaganje'}">
						${grade.value}   
					</c:when>
				</c:choose>			    
			    </c:forEach>
			    </td>
			 </tr>
			
			 <tr>
			    <td>Pismeno ispitivanje</td>
			    <td>
			    <c:forEach var="grade" items="${studentgrades.grades}">
				<c:choose>
					<c:when test="${grade.component == 'pismeno ispitivanje'}">
						${grade.value}   
					</c:when>
				</c:choose>			    
			    </c:forEach>
			    </td>
			 </tr>
			 <tr>
			    <td>Usmeno ispitivanje</td>
			    <td>
			    <c:forEach var="grade" items="${studentgrades.grades}">
				<c:choose>
					<c:when test="${grade.component == 'usmeno ispitivanje'}">
						${grade.value}   
					</c:when>
				</c:choose>			    
			    </c:forEach>
			    </td>
			   
			  </tr>
			  <tr>
			    <td><div style=" height: 80px;">Komentari</div></td>
			    <td colspan= "8">
			    <c:forEach var="comment" items="${studentgrades.comments}">
					<p><fmt:formatDate value="${comment.date}" pattern="dd.MM.yyyy." /> ${comment.text}</p>			    
			   	</c:forEach>	
			    </td> 
			  </tr>
			 
			</table>
		
		<h4>Zaključna ocjena:
		<c:choose>
			<c:when test="${studentgrades.finalGrade != 0}">
				${studentgrades.finalGrade}
			</c:when>
			<c:otherwise> - </c:otherwise>
		</c:choose>
		</h4>
		
		<div class="btn-group" style="width:100%">
		  <a href="/eDnevnik/servlets/archivemain"><button style="width:15%">Završi pregled</button></a>
		</div>
		
	</body> 
	</html>