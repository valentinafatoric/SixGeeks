<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
	<head>
		<title>eDnevnik</title>
		
		<style type="text/css">
		body {
			font: 18px/1.4 "Helvetica Neue", Helvetica, Arial, sans-serif;
			color: #395870;
		}
		
		.container1 {
		    border-radius: 5px;
		    background-color: #f2f2f2;
		    padding: 20px;
		}		
		h2{
			font: 18px/1.4 "Helvetica Neue", Helvetica, Arial, sans-serif;
			color: #395870;
			text-align:center;
		}
		
		ul{
		list-style-type: none;
		margin:0;
		padding:0;
		overflow:hidden;
		background-color: #003A8E
		}
		
		li{
		float:left
		}
		
		li a{
		display: block;
		color:white;
		text-align: center;
		padding: 14px 16px;
		text-decoration: none;
		}
		
		li a:hover{
			background-color:#ffff66;
            color: #395870;
		}
		
		.active{
		background-color: #708090
		}
		
		.col-75 {
		    float:left;
		    width: 75%;
		    margin-top: 6px;
		}
		
        textarea{
        display:block;
        	
            margin-left:auto;
            margin-right:auto;
        }
        .container{
        display:flex;
        justify-content:center;
        margin-top:80px;
        }
        .container2{
        display:flex;
        justify-content:center;
        margin-top:100px;
        }
        
		input[type=submit] {
		    background-color:#1f3d7a;
		    color: white;
		    padding: 12px 20px;
		    border: none;
		    border-radius: 4px;
		    cursor: pointer;
		    float: right;
		}
		
		input[type=submit]:hover {
		    background-color: #4976d0;
		}
		
		<%@include file="/WEB-INF/style/style.css"%>
		
		</style>
	</head>

	<body>
		
		<h1>eDnevnik</h1>
		
		<ul>
		<li><a href="/eDnevnik/servlets/professormain">Početna stranica</a></li>
		<li><a href="/eDnevnik/servlets/professorsubjects">Pregled nastavnih predmeta</a></li>
		<li><a href="/eDnevnik/servlets/professorabsencemain">Izostanci</a></li>
		<li><a href="/eDnevnik/servlets/professorinfo">Pregled osobnih podataka</a></li>
		<li> <a href="/eDnevnik/servlets/professorarchive">Arhiva</a></li>
		<c:choose>
			<c:when test="${deputy == true}">
				<li class="dropdown"> 
				<a href="javascript:void(0)" class="dropbtn">Razredništvo</a>
					<div class="dropdown-content">
						<a href="/eDnevnik/servlets/absencejustify">Opravdavanje izostanaka</a>
						<a href="/eDnevnik/servlets/myclass">Moj razred</a>
					</div>
				</li>
			</c:when>
		</c:choose>
		<li><a href="/eDnevnik/servlets/log-out">Odjava</a></li>
		</ul>	
		
		<div class="container1">
		<c:if test="${message!=null}"><p class="err">${message}</p></c:if>
		<h2>Upišite godinu</h2>
		<form action="/eDnevnik/servlets/professorarchive" method="post">
		<div class="container">
		<div class="col-75">
				<textarea id="schoolYear" name="schoolYear" placeholder="npr. 2016./2017."></textarea>
			</div>
		</div>
        <div class="container2">
		<input type="submit" value="Pretraži">
      	</div>
        
		</form>
		</div>
		
	</body> 
	</html>