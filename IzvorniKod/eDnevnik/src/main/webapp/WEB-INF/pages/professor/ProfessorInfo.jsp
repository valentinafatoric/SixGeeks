<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import = "java.io.*,java.util.*" %>
<%@ page import = "javax.servlet.*,java.text.*" %>
<%@ page import="dao.*" %>
<%@ page import="model.*" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
	<head>
		<title>eDnevnik</title>
		
		<style type="text/css">
			.container1 {
			    border-radius: 5px;
			    background-color: #f2f2f2;
			    padding: 20px;
			}
			
			h2,h3,h4{
				color:#001f4d;
				display: block;
				font-weight: bold;
			}
			
			<%@include file="/WEB-INF/style/style.css"%>
		</style>
	</head>

	<body>
		<h1>eDnevnik</h1>
		
		<ul>
		<li><a href="/eDnevnik/servlets/professormain">Početna stranica</a></li>
		<li><a href="/eDnevnik/servlets/professorsubjects">Pregled nastavnih predmeta</a></li>
		<li><a href="/eDnevnik/servlets/professorabsencemain">Izostanci</a></li>
		<li><a href="/eDnevnik/servlets/professorinfo">Pregled osobnih podataka</a></li>
		<li><a href="/eDnevnik/servlets/professorarchive">Arhiva</a></li>
		<c:choose>
			<c:when test="${deputy == true}">
				<li class="dropdown"> 
				<a href="javascript:void(0)" class="dropbtn">Razredništvo</a>
					<div class="dropdown-content">
						<a href="/eDnevnik/servlets/absencejustify">Opravdavanje izostanaka</a>
						<a href="/eDnevnik/servlets/myclass">Moj razred</a>
					</div>
				</li>
			</c:when>
		</c:choose>
		<li><a href="/eDnevnik/servlets/log-out">Odjava</a></li>
		</ul>	
		
		<h2>Osobni podaci profesora:</h2>
		
		<div class="container1">
		<p><b>Ime</b>: ${professor.name}</p>
		<p><b>Prezime</b>: ${professor.surname}</p>
		<p><b>OIB</b>: ${professor.OIB}</p>
		<p><b>Naziv škole</b>: ${school.name}</p>
		<p><b>Adresa škole</b>: ${school.address}</p>
		<p><b>Fotografija:</b> <img src="${professor.picture}" /></p>
		<p><b>Broj telefona</b>: ${professor.phoneNumber}</p>
		
		<c:choose>
			<c:when test="${classOfProf != null}">

				<c:choose>
					<c:when test="${consultations == null || consultations.isEmpty()}">
						<p>Trenutno nema postavljenih konzultacija!</p>
					</c:when>
					<c:otherwise>
						<p><b>Roditeljski sastanci: </b></p>
						<c:forEach var="consultation" items="${consultations}">
							<c:choose>
							<c:when test="${consultation.parentMeeting == true}">
								 <fmt:formatDate value="${consultation.dateAndTime}" pattern="dd.MM.yyyy. HH:mm" /> u ${consultation.room.name}
								 <p></p>
							</c:when>
							<c:otherwise></c:otherwise>
							</c:choose>
						</c:forEach>
						
						<p><b>Konzulatacije: </b></p>
						<c:forEach var="consultation" items="${consultations}">
							<c:choose>
							<c:when test="${consultation.parentMeeting == false}">
								 <fmt:formatDate value="${consultation.dateAndTime}" pattern="dd.MM.yyyy. HH:mm" /> u ${consultation.room.name}
								 <p></p>
							</c:when>
							<c:otherwise></c:otherwise>
							</c:choose>
						</c:forEach>
					</c:otherwise>
				</c:choose>			
			</c:when>
		</c:choose>
		<p></p>
		
		<div>
		<div class="container1">
		<a href="/eDnevnik/servlets/updateInfo/${professor.OIB}"><button class=button style="width:15%">Uredi podatke</button></a>
		<c:if test="${classOfProf != null}">
			<a href="/eDnevnik/servlets/addconsultation/${professor.OIB}"><button class=button style="width:15%">Dodaj konzultacije</button></a>
		</c:if>
		</div>
		</div>
		</div>
	</body>
</html>