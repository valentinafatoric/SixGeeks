<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import = "java.io.*,java.util.*" %>
<%@ page import = "javax.servlet.*,java.text.*" %>
<%@ page import="dao.*" %>
<%@ page import="model.*" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>eDnevnik</title>
		
		<style type="text/css">
		body {
			font: 18px/1.4 "Helvetica Neue", Helvetica, Arial, sans-serif;
			color: #395870;
		}
		
		ul{
		list-style-type: none;
		margin:0;
		padding:0;
		background-color: #003A8E
		}
		
		li{
		float:left
		}
		
		li a{
		display: block;
		color:white;
		text-align: center;
		padding: 14px 16px;
		text-decoration: none;
		}
		
		li a:hover{
			background-color:#ffff66;
            color: #395870;
		}
		
		.active{
		background-color: #708090
		}
		
		table{
			border-collapse: collapse;
			width:100%;
		}
		
		th,td{
			text-align:left;
			padding: 8px;
		}
		
		tr:nth-child(even){
			background-color:#f2f2f2;
		}
		
		#upcontainer {
			display : inline;
			width: 400px;
			height: 300px;
			
		}
		
		hr {
		    width: 100%;
		    border: 0;
		    height: 2px;
		    background: #000;
		    opacity: 0.2;
		    float: left;
		}
		
		
		<%@include file="/WEB-INF/style/style.css"%>
		
		#choosecontainer {
			float: left;
			height: 260px;
		}
		#additioncontainer {
			float: left;
			height: 260px;
		}s
		
		.populated:hover {
		    border:2px outset;
		    padding:0;
		    cursor:pointer;
		    background-color: #d63131;
		}
		.empty:hover {
		    border:2px inset;
		    padding:0;
		    cursor:pointer;
		}
		
		
		
/* 		#scheduletable td {
			height: 50px;
			overflow: hidden;
		} */
		
		#schedulecol {
			width: 12%;
		}
		
		
		</style>
		
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script type="text/javascript">
		
		var professors = {
				<c:forEach  var="subjProf" items="${subjProfs}">
				"${subjProf.professor.OIB}" : {
					name : "${subjProf.professor.name}",
					surname : "${subjProf.professor.surname}",
					subject : "${subjProf.subject.id}"
				},
				</c:forEach>
		};
			function clickButton() {
				loadSchedule();

				document.getElementById("formcontainer").innerHTML = "";
				document.getElementById("invalidForm").innerHTML = "";
			}
			function loadSchedule() {
				var _studentClass = document.getElementById("classselect").value;
				if (_studentClass=="") return;
				
				$.ajax({
		            url: '/eDnevnik/servlets/generateschedule',
		            data: { 
		            	'studentClass' : _studentClass
		            },
		            type: 'GET',
		            success: function(res) {

						document.getElementById("scheduletable").innerHTML = res;
		            }
		        });
			}
			
			
			function deleteTerm(_id) {
				$.ajax({
			           type: "POST",
			           url : "/eDnevnik/servlets/deleteterm",
			           data: { 'id' : _id},
			           success: function(data) {
			        	  console.log("uspjesno obrisano");
			        	  loadSchedule();
			           },
			           error : function(data) {
						console.log("nije uspjelo "+data);
					}
						
			    });
			}
			
			function addTerm(day_string, day, hour) {
				var _studentClass = document.getElementById("classselect").value;
				if (_studentClass=="") return;
				console.log("idem dodati predavanje za "+_studentClass+day_string);
				$.ajax({
		            url: '/eDnevnik/servlets/addlecture',
		            data: { 
		            	'studentClassId' : _studentClass, "day" : day, "hour" : hour, "daytext" : day_string
		            },
		            type: 'GET',
		            success: function(res) {

						document.getElementById("formcontainer").innerHTML = res;
						document.getElementById("invalidForm").innerHTML = "";
						updateProfs();
		            }
		        });
			}
			
			function updateProfs() {
				var subjID = document.getElementById("subjectselect").value;
				var profSelection = document.getElementById("profselect");
				profSelection.innerHTML="<option value=\"\" selected disabled >Odaberite profesora</option>";
				for (var prof in window.professors) {
					if (professors[prof].subject == subjID) {
						profSelection.innerHTML += "<option value="+prof+">"+professors[prof].name+" "+professors[prof].surname+"</option>"
					}
				} 
			}
			
			function checkForm() {
				var subj = document.getElementById("subjectselect").value;
				var studclass = document.getElementById("classselect").value;
				var prof = document.getElementById("profselect").value;
				var room = document.getElementById("roomselect").value;
				var day = document.getElementById("dayselect").value;
				var hour = document.getElementById("hourselect").value;
				
				if (subj == "" || studclass == "" || prof == "" || room == "" || day == "" || hour == "") {
					document.getElementById("invalidForm").innerHTML = "Sva polja moraju biti odabrana!"; 
					return false;
				}

				$.ajax({
			           type: "POST",
			           url : "/eDnevnik/servlets/addlecture",
			           data: $("#lectureaddform").serialize(), // serializes the form's elements.
			           success: function(data) {
			        	   console.log("uspjesan odgovor od post");
			        	   console.log(data);
			               if (data=="added") {
			            	   loadSchedule();
			            	   document.getElementById("invalidForm").innerHTML="";
			            	   document.getElementById("formcontainer").innerHTML="<p>Predavanje uspješno dodano.</p>";
			               } else {
			            	   document.getElementById("invalidForm").innerHTML=data;
			               }
			           },
			           error : function(data) {
			        	   console.log("neuspjelo");
			        	   console.log("data");
			           }
						
			    });
				return false;
			} 
			
		</script>


</head>
<body>
<h1>eDnevnik</h1>
		
		<ul>
		<li><a href="/eDnevnik/servlets/admin">Početna stranica</a></li>
		
		<li class="dropdown"> 
			<a href="/eDnevnik/servlets/manipulatenotifications" class="dropbtn">Obavijesti</a>
		</li>
		
		<li class="dropdown"> 
			<a href="/eDnevnik/servlets/editlectures" class="dropbtn">Raspored sati</a>
		</li>
		
		<li class="dropdown"> 
			<a href="javascript:void(0)" class="dropbtn">Predmet</a>
				<div class="dropdown-content">
					<a href="/eDnevnik/servlets/editsubject">Prikaži listu predmeta</a>
					<a href="/eDnevnik/servlets/addsubject">Dodaj predmet</a>
				</div>
		</li>
		<li class="dropdown"> 
			<a href="javascript:void(0)" class="dropbtn">Profesor</a>
				<div class="dropdown-content">
					<a href="/eDnevnik/servlets/editprofesor">Prikaži listu profesora</a>
					<a href="/eDnevnik/servlets/addprofesor">Dodaj profesora</a>
				</div>
		</li>
		<li class="dropdown"> 
			<a href="javascript:void(0)" class="dropbtn">Razred/grupa</a>
				<div class="dropdown-content">
					<a href="/eDnevnik/servlets/studentclasslist">Prikaži i uredi razred/grupu</a>
					<a href="/eDnevnik/servlets/addstudentclass">Dodaj razred/grupu</a>
				</div>
		</li>
		<li class="dropdown"> 
			<a href="javascript:void(0)" class="dropbtn">Učenik</a>
				<div class="dropdown-content">
					<a href="/eDnevnik/servlets/studentlist">Prikaži listu učenika</a>
					<a href="/eDnevnik/servlets/addstudent">Dodaj učenika</a>
				</div>
		</li>
		<li> <a href="/eDnevnik/servlets/eventdiary">Dnevnik događaja</a></li>
		<li><a href="/eDnevnik/servlets/log-out">Odjava</a></li>
		</ul>

		<div class="upcontainer">	
			<div id="choosecontainer">
			<fieldset id="fieldchooser">
					<legend>Upravljanje rasporedom:</legend>
					<p> Za upravljanje rasporedom prvo odaberite razred te kilkom mišem po njemu dodavajte i brišite predavanja</p>
					<p><label>Razred:</label></p>   
					<select style="float: left" id="classselect" name="classselect">
						<option value="" selected disabled >Odaberite razred</option>
						<c:forEach var="studentclass" items="${studentclasses}">
							
							<option value="${studentclass.id}">${studentclass.name}</option>
						</c:forEach>
					</select>
					
			</fieldset>
				<button id="classchooser" class="button" onclick="clickButton()">Odaberi</button>
			</div>
			<div  id="additioncontainer">
				<div id="formcontainer">&nbsp</div>
				<br>
			</div>
		</div>
		<div id="errorcontainer"><p id="invalidForm"></p></div>
		<hr>

		<div id="scheduletable">
		
		</div>
		
</body>
</html>