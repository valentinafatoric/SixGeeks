<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import = "java.io.*,java.util.*" %>
<%@ page import = "javax.servlet.*,java.text.*" %>
<%@ page import="dao.*" %>
<%@ page import="model.*" %>

<html>
	<head>
		<title>eDnevnik</title>
		
		<style type="text/css">
		h4{
			font: 18px/1.4 "Helvetica Neue", Helvetica, Arial, sans-serif;
			color:#000033
		}
		body {
			font: 16px/1.4 "Helvetica Neue", Helvetica, Arial, sans-serif;
			color: #395870;
		}
		ul{
			list-style-type: none;
			margin:0;
			padding:0;
			overflow:hidden;
			background-color: #003A8E
		}
		
		li{
			float:left
		}
		
		li a{
			display: block;
			color:white;
			text-align: center;
			padding: 14px 16px;
			text-decoration: none;
		}
		
		li a:hover{
			background-color:#ffff66;
            color: #395870;
		}
		
		.active{
			background-color: #708090
		}
		.container {
		    border-radius: 5px;
		    background-color: #f2f2f2;
		    padding: 20px;
		}
		
		.col-25 {
		    float: left;
		    width: 25%;
		    margin-top: 10px;
		}
		
		.col-75 {
		    float: left;
		    width: 75%;
		    margin-top: 10px;
		}
		
		/* Clear floats after the columns */
		.row:after {
		    content: "";
		    display: table;
		    clear: both;
		}
		
		/* Responsive layout - when the screen is less than 600px wide, make the two columns stack on top of each other instead of next to each other */
		@media (max-width: 600px) {
		    .col-25, .col-75, input[type=submit] {
		        width: 100%;
		        margin-top: 0;
		    }
		}
		.btn-group button {
		    background-color: #1f3d7a; /* background */
		    border: 2px solid white; /* border */
		    border-radius: 4px;
		    color: white; /* White text */
		    padding: 10px 24px; /* Some padding */
		    cursor: pointer; /* Pointer/hand icon */
		    float: right; /* Float the buttons side by side */
		}
		
		/* Clear floats (clearfix hack) */
		.btn-group:after {
		    content: "";
		    clear: both;
		    display: table;
		}
		
		.btn-group button:not(:last-child) {
		    border-right: none; /* Prevent double borders */
		}
		
		/* Add a background color on hover */
		.btn-group button:hover {
		    background-color: #4976d0;
		}
		.container1 {
			    border-radius: 5px;
			    background-color: #f2f2f2;
			    padding: 20px;
			}
		<%@include file="/WEB-INF/style/style.css"%>
		
		</style>
	</head>

	<body>
		<h1>eDnevnik</h1>
		
		<ul>
		<li><a href="/eDnevnik/servlets/professormain">Početna stranica</a></li>
		<li><a href="/eDnevnik/servlets/professorsubjects">Pregled nastavnih predmeta</a></li>
		<li><a href="/eDnevnik/servlets/professorabsencemain">Izostanci</a></li>
		<li><a href="/eDnevnik/servlets/professorinfo">Pregled osobnih podataka</a></li>
		<li> <a href="/eDnevnik/servlets/professorarchive">Arhiva</a></li>
		<c:choose>
			<c:when test="${deputy == true}">
				<li class="dropdown"> 
				<a href="javascript:void(0)" class="dropbtn">Razredništvo</a>
					<div class="dropdown-content">
						<a href="/eDnevnik/servlets/absencejustify">Opravdavanje izostanaka</a>
						<a href="/eDnevnik/servlets/myclass">Moj razred</a>
					</div>
				</li>
			</c:when>
		</c:choose>
		<li><a href="/eDnevnik/servlets/log-out">Odjava</a></li>
		</ul>	
		
		<c:if test="${message!=null}"><p class="err">${message}</p></c:if>
		<div class="container">
		  <form action="/eDnevnik/servlets/updateInfo" method="post" enctype="multipart/form-data">
		    <div class="row">
		      <div class="col-75">
		        <h4>Broj telefona:</h4>
		      </div>
		      <div class="col-75">
		        <input type="text" id="value" name="phoneNumber" placeholder="" >
		      </div>
		    </div>
            <p></p>
            
            <div class="row">
		      <div class="col-75">
		        <h4>URL slike:</h4>
		      </div>
		      <div class="col-75">
		        <input type="text" id="value" name="pictureurl" placeholder="" >
		      </div>
		    </div>
		    <p></p>
		    
		    <div class="container1">
		    <div class="btn-gruop">
		     <input class=button type="submit" value="Završi">
          	 <a href="/eDnevnik/servlets/professorinfo"><input class=button type="button" value="Odustani"></a>
          </div>
          </div>
          </form>
          
        </div>
		
	</body>
</html>