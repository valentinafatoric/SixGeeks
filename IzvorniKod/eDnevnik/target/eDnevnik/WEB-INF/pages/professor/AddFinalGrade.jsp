<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
	<head>
		<title>eDnevnik</title>
		
		<style type="text/css">
		body {
			font: 18px/1.4 "Helvetica Neue", Helvetica, Arial, sans-serif;
			color: #395870;
		}
		
		ul{
		list-style-type: none;
		margin:0;
		padding:0;
		overflow:hidden;
		background-color: #003A8E
		}
		
		li{
		float:left
		}
		
		li a{
		display: block;
		color:white;
		text-align: center;
		padding: 14px 16px;
		text-decoration: none;
		}
		
		li a:hover{
			background-color:#ffff66;
            color: #395870;
		}
		
		.active{
		background-color: #708090
		}
		
		* {
    		box-sizing: border-box;
		}
		
		input[type=text], select, textarea{
		    width: 70%;
		    padding: 12px;
		    border: 1px solid #ccc;
		    border-radius: 4px;
		    box-sizing: border-box;
		    resize: vertical;
		}
		
		label {
		    padding: 12px 12px 12px 0;
		    display: inline-block;
		}
		
		input[type=submit] {
		    background-color:#1f3d7a;
		    color: white;
		    padding: 12px 20px;
		    border: none;
		    border-radius: 4px;
		    cursor: pointer;
		    float: right;
		}
		
		input[type=submit]:hover {
		    background-color: #4976d0;
		}
		
		input[type=button] {
		    background-color:#1f3d7a;
		    color: white;
		    padding: 12px 20px;
		    border: 4px;
		    border-radius: 4px;
		    cursor: pointer;
		    float: right;
		}
		
		input[type=button]:hover {
		    background-color: #4976d0;
		}
		
		.container {
		    border-radius: 5px;
		    background-color: #f2f2f2;
		    padding: 20px;
		}
		
		.col-25 {
		    float: left;
		    width: 25%;
		    margin-top: 6px;
		}
		
		.col-75 {
		    float: left;
		    width: 75%;
		    margin-top: 6px;
		}
		
		/* Clear floats after the columns */
		.row:after {
		    content: "";
		    display: table;
		    clear: both;
		}
		
		/* Responsive layout - when the screen is less than 600px wide, make the two columns stack on top of each other instead of next to each other */
		@media (max-width: 600px) {
		    .col-25, .col-75, input[type=submit] {
		        width: 100%;
		        margin-top: 0;
		    }
		}
		
		<%@include file="/WEB-INF/style/style.css"%>
		
		</style>
	</head>

	<body>
		
		<h1>eDnevnik</h1>
		
		<ul>
		<li><a href="/eDnevnik/servlets/professormain">Početna stranica</a></li>
		<li><a href="/eDnevnik/servlets/professorsubjects">Pregled nastavnih predmeta</a></li>
		<li><a href="/eDnevnik/servlets/professorabsencemain">Izostanci</a></li>
		<li><a href="/eDnevnik/servlets/professorinfo">Pregled osobnih podataka</a></li>
		<li> <a href="/eDnevnik/servlets/professorarchive">Arhiva</a></li>
		<c:choose>
			<c:when test="${deputy == true}">
				<li class="dropdown"> 
				<a href="javascript:void(0)" class="dropbtn">Razredništvo</a>
					<div class="dropdown-content">
						<a href="/eDnevnik/servlets/absencejustify">Opravdavanje izostanaka</a>
						<a href="/eDnevnik/servlets/myclass">Moj razred</a>
					</div>
				</li>
			</c:when>
		</c:choose>
		<li><a href="/eDnevnik/servlets/log-out">Odjava</a></li>
		</ul>	
		
		<c:if test="${message!=null}"><p class="err">${message}</p></c:if>
		<div class="container">
		  <form action="/eDnevnik/servlets/addfinalgrade/${pathInfo}" method="post">
		    <div class="row">
		      <div class="col-25">
		        <label for="value">Zaključna ocjena</label>
		      </div>
		      <div class="col-75">
		        <input type="text" id="value" name="value" placeholder="" >
		      </div>
		    </div>
		    <div class="row">
		      <input type="submit" value="Dodaj zaključnu ocjenu">
		    </div>
		  </form>
		  
		  <div class="row">
		  	<a href="/eDnevnik/servlets/currentprofstudents/${pathInfo}"><input type="button" value="Odustani"></a>
		  </div>
		</div>
	</body> 
	</html>