<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
	<head>
		<title>eDnevnik</title>
		
		<style type="text/css">
		body {
			font: 18px/1.4 "Helvetica Neue", Helvetica, Arial, sans-serif;
			color: #395870;
		}
		
		ul{
		list-style-type: none;
		margin:0;
		padding:0;
		overflow:hidden;
		background-color: #003A8E
		}
		
		li{
		float:left
		}
		
		li a{
		display: block;
		color:white;
		text-align: center;
		padding: 14px 16px;
		text-decoration: none;
		}
		
		li a:hover{
			background-color:#ffff66;
            color: #395870;
		}
		
		.active{
		background-color: #708090
		}
		
		table{
			border-collapse: collapse;
			width:100%;
		}
		
		th,td{
			text-alighn:left;
			padding: 8px;
		}
		
		tr:nth-child(even){
			background-color:#f2f2f2;
		}
		
		a:link {
		    text-decoration: none;
		    color: #395870;
		}
		
		a:visited {
		    text-decoration: none;
		}
		
		a:hover {
		    text-decoration: underline;
		}
		
		a:active {
		    text-decoration: underline;
		}
		
		<%@include file="/WEB-INF/style/style.css"%>
		
		.listStudent {
		    background:#ffeb99;
		    padding: 20px;
		    display: block
		}
		
		
		
		.listStudent2 {
		    padding: 5px;
		    margin-left: 35px;
		    background: #ebfaeb;
		    margin: 5px;
		    display: block
		}
		
		/* unvisited link */
		.a1:link {
		    color:#395870;
		}
		
		/* visited link */
		.a1:visited {
		   color:#395870;
		}
		
		/* mouse over link */
		.a1:hover {
		    color:#000066;
		}
		
		/* selected link */
		.a1:active {
		    color:#395870;
		}
		
		</style>
	</head>

	<body>
		
		<h1>eDnevnik</h1>
		
		<ul>
		<li><a href="/eDnevnik/servlets/professormain">Početna stranica</a></li>
		<li><a href="/eDnevnik/servlets/professorsubjects">Pregled nastavnih predmeta</a></li>
		<li><a href="/eDnevnik/servlets/professorabsencemain">Izostanci</a></li>
		<li><a href="/eDnevnik/servlets/professorinfo">Pregled osobnih podataka</a></li>
		<li> <a href="/eDnevnik/servlets/professorarchive">Arhiva</a></li>
		<c:choose>
			<c:when test="${deputy == true}">
				<li class="dropdown"> 
				<a href="javascript:void(0)" class="dropbtn">Razredništvo</a>
					<div class="dropdown-content">
						<a href="/eDnevnik/servlets/absencejustify">Opravdavanje izostanaka</a>
						<a href="/eDnevnik/servlets/myclass">Moj razred</a>
					</div>
				</li>
			</c:when>
		</c:choose>
		<li><a href="/eDnevnik/servlets/log-out">Odjava</a></li>
		</ul>	
		
		<h2>Popis učenika: ${myclass.name}</h2>
		
		<div class="listStudent">
		<c:choose>
			<c:when test="${myclass.students == null || myclass.students.isEmpty()}">
				<p class="listStudent2">Nema učenika!</p>
			</c:when>
			<c:otherwise>
				<c:forEach var="student" items="${myclass.students}" >
		
				<p class="listStudent2"><a class="a1" href="/eDnevnik/servlets/mystudent/${myclass.id}-${student.OIB}">${student.name} ${student.surname}</a></p>
				
				</c:forEach>
			</c:otherwise>
		</c:choose>
		</div>
		
	</body> 
	</html>