<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
	<head>
		<title>eDnevnik</title>
		
		<style type="text/css">
		body {
			font: 18px/1.4 "Helvetica Neue", Helvetica, Arial, sans-serif;
			color: #395870;
		}
		
		ul{
		list-style-type: none;
		margin:0;
		padding:0;
		overflow:hidden;
		background-color: #003A8E
		}
		
		li{
		float:left
		}
		
		li a{
		display: block;
		color:white;
		text-align: center;
		padding: 14px 16px;
		text-decoration: none;
		}
		
		li a:hover{
			background-color:#ffff66;
            color: #395870;
		}
		
		.active{
		background-color: #708090
		}
		
		table{
			border-collapse: collapse;
			width:100%;
		}
		
		th,td{
			text-alighn:left;
			padding: 8px;
		}
		
		tr:nth-child(even){
			background-color:#f2f2f2;
		}
		
		.btn-group .button {
		    background-color:white; /* Blue */
		    border: 2px solid #ffa31a;
		    color: black;
		    padding: 15px 32px;
		    text-align: center;
		    text-decoration: none;
		    display: inline-block;
		    font-size: 16px;
		    cursor: pointer;
		    width: 500px;
		    height:200px
		    display: block;
		}
		
		.btn-group .button:not(:last-child) {
		    border-bottom: none; /* Prevent double borders */
		}
		
		.btn-group .button:hover {
		    background-color: #ffa31a;
            color:white;
		}
        a{
			text-decoration: none;        
        }
        .container{ display: flex; justify-content: center;
        margin-top:150px}

        <%@include file="/WEB-INF/style/style.css"%>
		
		</style>
	</head>

	<body>
		
		<h1>eDnevnik</h1>
		
		<ul>
		<li><a href="/eDnevnik/servlets/professormain">Početna stranica</a></li>
		<li><a href="/eDnevnik/servlets/professorsubjects">Pregled nastavnih predmeta</a></li>
		<li><a href="/eDnevnik/servlets/professorabsencemain">Izostanci</a></li>
		<li><a href="/eDnevnik/servlets/professorinfo">Pregled osobnih podataka</a></li>
		<li> <a href="/eDnevnik/servlets/professorarchive">Arhiva</a></li>
		<c:choose>
			<c:when test="${deputy == true}">
				<li class="dropdown"> 
				<a href="javascript:void(0)" class="dropbtn">Razredništvo</a>
					<div class="dropdown-content">
						<a href="/eDnevnik/servlets/absencejustify">Opravdavanje izostanaka</a>
						<a href="/eDnevnik/servlets/myclass">Moj razred</a>
					</div>
				</li>
			</c:when>
		</c:choose>
		<li><a href="/eDnevnik/servlets/log-out">Odjava</a></li>
		</ul>	
		
		<div class="container">
		<div class="btn-group">
		
		<a href="/eDnevnik/servlets/professorabsence"><button class="button">Pregled izostanaka</button></a>
		<a href="/eDnevnik/servlets/professorabsenceinsert"><button class="button">Unos za izostanke</button></a>
		
		</div>
		</div>
	</body>
	</html>