<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
	<head>
		<title>eDnevnik</title>
		
		<style type="text/css">
		body {
			font: 18px/1.4 "Helvetica Neue", Helvetica, Arial, sans-serif;
			color: #395870;
		}
		
		ul{
		list-style-type: none;
		margin:0;
		padding:0;
		overflow:hidden;
		background-color: #003A8E
		}
		
		li{
		float:left
		}
		
		li a{
		display: block;
		color:white;
		text-align: center;
		padding: 14px 16px;
		text-decoration: none;
		}
		
		li a:hover{
			background-color:#ffff66;
            color: #395870;
		}
		
		.active{
		background-color: #708090;
		}
		
		table{
			border-collapse: collapse;
			width:100%;
		}
		
		th,td{
			text-alighn:left;
			padding: 8px;
		}
		
		tr:nth-child(even){
			background-color:#f2f2f2;
		}
		
		.btn-group .button {
		    background-color: #000080; /* Blue */
		    border: 1px solid white;
		    color: white;
		    padding: 15px 32px;
		    text-align: center;
		    border-radius: 4px;
		    text-decoration: none;
		    display: inline-block;
		    font-size: 16px;
		    cursor: pointer;
		    width: 150px;
		    display: block;
		}
		
		.btn-group .button:not(:last-child) {
		    border-bottom: none; /* Prevent double borders */
		}
		
		.btn-group .button:hover {
		    background-color:  #ffa31a;
		}
		
		<%@include file="/WEB-INF/style/style.css"%>
		
		</style>
	</head>

	<body>
		
		<h1>eDnevnik</h1>
		
		<ul>
		<li><a href="/eDnevnik/servlets/professormain">Početna stranica</a></li>
		<li><a href="/eDnevnik/servlets/professorsubjects">Pregled nastavnih predmeta</a></li>
		<li><a href="/eDnevnik/servlets/professorabsencemain">Izostanci</a></li>
		<li><a href="/eDnevnik/servlets/professorinfo">Pregled osobnih podataka</a></li>
		<li> <a href="/eDnevnik/servlets/professorarchive">Arhiva</a></li>
		<c:choose>
			<c:when test="${deputy == true}">
				<li class="dropdown"> 
				<a href="javascript:void(0)" class="dropbtn">Razredništvo</a>
					<div class="dropdown-content">
						<a href="/eDnevnik/servlets/absencejustify">Opravdavanje izostanaka</a>
						<a href="/eDnevnik/servlets/myclass">Moj razred</a>
					</div>
				</li>
			</c:when>
		</c:choose>
		<li><a href="/eDnevnik/servlets/log-out">Odjava</a></li>
		</ul>
		
		<h2>Popis Predmeta</h2>
		
		<c:choose>
			<c:when test="${subjectInfos == null || subjectInfos.isEmpty()}">
				<p>Nema predmeta za prikaz!</p>
				<a href="/eDnevnik/servlets/professorarchive"><button class="button">Izađi</button></a>
			</c:when>
			<c:otherwise>
				<div class="btn-group">
				
				<c:forEach var="subjectInfo" items="${subjectInfos}" >
				
				<a href="/eDnevnik/servlets/archivesubjectdetails/${subjectInfo.subject.name}-${schoolYear}"><button class="button">${subjectInfo.subject.name}</button></a>
				
				</c:forEach>
				</div>
			</c:otherwise>
		</c:choose>

	</body> 
	</html>