<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
	<head>
		<title>eDnevnik</title>
		
		<style type="text/css">
		body {
			font: 18px/1.4 "Helvetica Neue", Helvetica, Arial, sans-serif;
			color: #395870;
		}
		
		ul{
		list-style-type: none;
		margin:0;
		padding:0;
		overflow:hidden;
		background-color: #003A8E
		}
		
		li{
		float:left
		}
		
		li a{
		display: block;
		color:white;
		text-align: center;
		padding: 14px 16px;
		text-decoration: none;
		}
		
		li a:hover{
			background-color:#ffff66;
            color: #395870;
		}
		
		.active{
		background-color: #708090
		}
		
		table{
			border-collapse: collapse;
			width:100%;
		}
		
		th,td{
			text-alighn:left;
			padding: 8px;
		}
		
		tr:nth-child(even){
			background-color:#f2f2f2;
		}
		
		a:link {
		    text-decoration: none;
		    color: #395870;
		}
		
		a:visited {
		    text-decoration: none;
		    color:none;
		}
		
		a:hover {
		    text-decoration: underline;
		}
		
		a:active {
		    text-decoration: underline;
		    color:none;
		}
		
		.listStudent {
		    background:#ffeb99;
		    padding: 20px;
		    display: block
		}
		
		
		
		.listStudent2 {
		    padding: 5px;
		    margin-left: 35px;
		    background: #ebfaeb;
		    margin: 5px;
		    display: block
		}
		
		/* unvisited link */
		.a1:link {
		    color:#395870;
		}
		
		/* visited link */
		.a1:visited {
		   color:#395870;
		}
		
		/* mouse over link */
		.a1:hover {
		    color:#000066;
		}
		
		/* selected link */
		.a1:active {
		    color:#395870;
		}
		
		<%@include file="/WEB-INF/style/style.css"%>
		
		</style>
	</head>

	<body>
		
		<h1>eDnevnik</h1>
		
		<ul>
		<li><a href="/eDnevnik/servlets/professormain">Početna stranica</a></li>
		<li><a href="/eDnevnik/servlets/professorsubjects">Pregled nastavnih predmeta</a></li>
		<li><a href="/eDnevnik/servlets/professorabsencemain">Izostanci</a></li>
		<li><a href="/eDnevnik/servlets/professorinfo">Pregled osobnih podataka</a></li>
		<li> <a href="/eDnevnik/servlets/professorarchive">Arhiva</a></li>
		<c:choose>
			<c:when test="${deputy == true}">
				<li class="dropdown"> 
				<a href="javascript:void(0)" class="dropbtn">Razredništvo</a>
					<div class="dropdown-content">
						<a href="/eDnevnik/servlets/absencejustify">Opravdavanje izostanaka</a>
						<a href="/eDnevnik/servlets/myclass">Moj razred</a>
					</div>
				</li>
			</c:when>
		</c:choose>
		<li><a href="/eDnevnik/servlets/log-out">Odjava</a></li>
		</ul>	
		
	<div class="listStudent">
		<c:choose>
			<c:when test="${studentclass == null}">
				<p class="listStudent2">Razred nema trenutno upisanih učenika!</p>
			</c:when>
			<c:otherwise>
				<h2>Popis učenika: ${studentclass.name}</h2>
				
				<c:forEach var="student" items="${studentclass.students}" >
				
				<p class="listStudent2"><a class="a1" href="/eDnevnik/servlets/archiveprofstudents/${subject.id}-${student.OIB}-${schoolYear}">${student.name} ${student.surname}</a></p>
				
				</c:forEach>
			</c:otherwise>			
		</c:choose>
	</div>
		
	</body> 
	</html>