﻿<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import = "java.io.*,java.util.*" %>
<%@ page import = "javax.servlet.*,java.text.*" %>
<%@ page import="dao.*" %>
<%@ page import="model.*" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%! 
	public String showTerms(Map<String, TermOfLecture> terms) {
		String out = "";
		
		for(int i = 1; i <= 7; i++) {
			out += "<td>";
			
			TermOfLecture term = terms.get(String.valueOf(i));
			if(term != null) {
				out += term.getSubject().getName() + " " + term.getStudentClass().getName() + " u " + term.getRoom().getName();
			} else {
				out += "&nbsp;";
			}
			
			out += "</td>";
		}
		
		return out;
	}

%>

<html>
	<head>
		<title>eDnevnik</title>
		
		<style type="text/css">
			.container1{
				position:relative;
			    width:700px;
			    height:250px;
			}
			hr {
		    	   width: 700px;
		    	   float: left;
		    	   border: 0;
		    	   height: 2px;
		    	   background: orange;
		    	   border-color: orange;
		    	   opacity: 0.5;
			}
			.p1{
				bottom: 0;
				right: 0; 
				position: absolute;
			}
			
			#ItemPreview {
				float:right;
			    border:2px solid gray;
			}
			
			<%@include file="/WEB-INF/style/style.css"%>
		</style>
	</head>

	<body>
		
		<h1>eDnevnik</h1>
		
		<ul>
		<li><a href="/eDnevnik/servlets/professormain">Početna stranica</a></li>
		<li><a href="/eDnevnik/servlets/professorsubjects">Pregled nastavnih predmeta</a></li>
		<li><a href="/eDnevnik/servlets/professorabsencemain">Izostanci</a></li>
		<li><a href="/eDnevnik/servlets/professorinfo">Pregled osobnih podataka</a></li>
		<li><a href="/eDnevnik/servlets/professorarchive">Arhiva</a></li>
		<c:choose>
			<c:when test="${deputy == true}">
				<li class="dropdown"> 
				<a href="javascript:void(0)" class="dropbtn">Razredništvo</a>
					<div class="dropdown-content">
						<a href="/eDnevnik/servlets/absencejustify">Opravdavanje izostanaka</a>
						<a href="/eDnevnik/servlets/myclass">Moj razred</a>
					</div>
				</li>
			</c:when>
		</c:choose>
		<li><a href="/eDnevnik/servlets/log-out">Odjava</a></li>
		</ul>
			
		
		<h2>Aktualne obavijesti:</h2>
		
		
			<c:choose>
				<c:when test="${notifications == null || notifications.isEmpty()}">
				
					<p>Nema trenutnih obavijesti.</p>
				
				</c:when>
				<c:otherwise>
				
					<c:forEach var="notification" items="${notifications}">
						<hr>
						<div class="container1">
							<c:if test="${notification.picture != null}">
								<img id="ItemPreview" src="${notification.picture}" />
							</c:if>		
							<h3 style="float: top: ;">${notification.title}</h3>
							
							<c:if test="${notification.text != null}">
								<p style="">${notification.text}</p>
							</c:if>
							<br><br>
							
							<div style="display: inline;">
							
							<p class="p1"><fmt:formatDate value="${notification.date}" pattern="dd.MM.yyyy. HH:mm" /> ${notification.creator.name} ${notification.creator.surname}</p>
							</div>
					
						</div>
						
					
					</c:forEach>

					<hr>
				</c:otherwise>
			</c:choose>
		
		
		<h3>Satnica:</h3>
		
		<table>
			<colgroup>
				<col span="1" style="background-color:#80ff80">
			</colgroup>
			<tr>
				<th>Dan u tjednu</th>
				<th>1.sat</th>
				<th>2.sat</th>
				<th>3.sat</th>
				<th>4.sat</th>
				<th>5.sat</th>
				<th>6.sat</th>
				<th>7.sat</th>
			</tr>
			
			<tr>
				<td><font color="#595959">Ponedjeljak</font></td>
				
				<% 
					Map<String, Map<String, TermOfLecture>> schedule = (Map<String, Map<String, TermOfLecture>>) request.getSession().getAttribute("schedule");
					out.print(showTerms(schedule.get("1")));
				%>
			</tr>
			<tr>
				<td><font color="#595959">Utorak</font></td>
				
				<% 
					out.print(showTerms(schedule.get("2")));
				%>
			</tr>
			<tr>
				<td><font color="#595959">Srijeda</font></td>
				<% 
					out.print(showTerms(schedule.get("3")));
				%>
			</tr>
			<tr>
				<td><font color="#595959">Četvrtak</font></td>
				<% 
					out.print(showTerms(schedule.get("4")));
				%>
			</tr>
			<tr>
				<td><font color="#595959">Petak</font></td>
				
				<% 
					out.print(showTerms(schedule.get("5")));
				%>
			</tr>
		</table>
	
	</body>
</html>