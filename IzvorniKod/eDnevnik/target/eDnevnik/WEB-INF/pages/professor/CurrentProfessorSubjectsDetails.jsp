<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
	<head>
		<title>eDnevnik</title>
		
		<style type="text/css">
		body {
			font: 18px/1.4 "Helvetica Neue", Helvetica, Arial, sans-serif;
			color: #395870;
		}
		
		ul{
		list-style-type: none;
		margin:0;
		padding:0;
		overflow:hidden;
		background-color: #003A8E
		}
		
		li{
		float:left
		}
		
		li a{
		display: block;
		color:white;
		text-align: center;
		padding: 14px 16px;
		text-decoration: none;
		}
		
		li a:hover{
			background-color:#ffff66;
            color: #395870;
		}
		
		.active{
		background-color: #708090
		}
		
		table{
			border-collapse: collapse;
			width:100%;
		}
		
		th,td{
			text-alighn:left;
			padding: 8px;
		}
		
		tr:nth-child(even){
			background-color:#f2f2f2;
		}
		
		<!-- h1 h2 h3 h4 nesredjeni -->
		
		h1{
			color: #395870;
			display: block;
			font-weight: bold;
		}
		
		h2,h3,h4{
			color:#001f4d;
			display: block;
			font-weight: bold;
		}
		
		
		.container1 {
		    border-radius: 10px;
		    background-color: #f2f2f2;
		    padding: 20px;
		}

		.btn-group .button {
		    background-color: #000080; /* Blue */
		    border: 1px solid white;
		    color: white;
		    padding: 15px 32px;
		    text-align: center;
		    border-radius: 4px;
		    text-decoration: none;
		    display: inline-block;
		    font-size: 16px;
		    cursor: pointer;
		    width: 150px;
		    display: block;
		}
		
		.btn-group .button:not(:last-child) {
		    border-bottom: none; /* Prevent double borders */
		}
		
		.btn-group .button:hover {
		    background-color: #ffa31a;
		}
		
		/* unvisited link */
		.a1:link {
		    color: #800000;
		}
		
		/* visited link */
		.a1:visited {
		    color: #800000;
		}
		
		/* mouse over link */
		.a1:hover {
		    color:red;
		}
		
		/* selected link */
		.a1:active {
		    color: #800000;
		}

		
		<%@include file="/WEB-INF/style/style.css"%>
		
		</style>
	</head>

	<body>
		
		<h1>eDnevnik</h1>
		
		<ul>
		<li><a href="/eDnevnik/servlets/professormain">Početna stranica</a></li>
		<li><a href="/eDnevnik/servlets/professorsubjects">Pregled nastavnih predmeta</a></li>
		<li><a href="/eDnevnik/servlets/professorabsencemain">Izostanci</a></li>
		<li><a href="/eDnevnik/servlets/professorinfo">Pregled osobnih podataka</a></li>
		<li> <a href="/eDnevnik/servlets/professorarchive">Arhiva</a></li>
		<c:choose>
			<c:when test="${deputy == true}">
				<li class="dropdown"> 
				<a href="javascript:void(0)" class="dropbtn">Razredništvo</a>
					<div class="dropdown-content">
						<a href="/eDnevnik/servlets/absencejustify">Opravdavanje izostanaka</a>
						<a href="/eDnevnik/servlets/myclass">Moj razred</a>
					</div>
				</li>
			</c:when>
		</c:choose>
		<li><a href="/eDnevnik/servlets/log-out">Odjava</a></li>
		</ul>	
		
		<h2>${subjectInfo.subject.name}</h2>
		
		<div class="container1">
		<p><b>Godina na kojoj se predmet sluša:</b> ${classYears}</p>
		
		<p><b>Opis predmeta:</b> ${subjectInfo.subject.description}</p>
		
		<p><b>Nastavni plan:</b></p>
		<p><a class="a1" href="${subjectInfo.curriculum}">Nastavni plan</a></p>
		
		<p><b>Nastavno pismo:</b></p>
		<p><a class="a1" href="${subjectInfo.teachingLetter}">Nastavno pismo</a></p>
		
		<p><b>Razredi koji pohađaju predmet:</b></p>
		
		<c:choose>
			
			<c:when test="${classes == null}">
				<p>Nema trenutnih razreda koji pohađaju predmet!</p>
			</c:when>
			
			<c:otherwise>
				<div class="btn-group">
				
				<c:forEach var="studentclass" items="${classes}" >
				
				<a href="/eDnevnik/servlets/currentprofclasses/${subjectInfo.subject.name}-${studentclass.id}"><button class="button">${studentclass.name}</button></a>
				
				</c:forEach>
				</div>
			</c:otherwise>

		</c:choose>
		</div>
		
	</body> 
	</html>