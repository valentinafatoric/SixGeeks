<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
	<head>
		<title>eDnevnik</title>
		
		<style type="text/css">
			<%@include file="/WEB-INF/style/style.css"%>
					h2,h3,h4{
			color:#001f4d;
			display: block;
			font-weight: bold;
		}
		.container1 {
		    border-radius: 5px;
		    background-color: #f2f2f2;
		    padding: 20px;
		}
		</style>
	</head>

	<body>
		<h1>eDnevnik</h1>
		
		<ul>
		<li><a href="/eDnevnik/servlets/studentmain">Početna stranica</a></li>
		<li><a href="/eDnevnik/servlets/student">Pregled eDnevnika</a></li>
		<li><a href="/eDnevnik/servlets/studentabsence">Pregled izostanaka</a></li>
		<li><a href="/eDnevnik/servlets/studentinfo">Pregled osobnih podataka</a></li>
		<li><a href="/eDnevnik/servlets/contactinfoprof">Pregled kontaktnih podataka profesora</a></li>
		<li><a href="/eDnevnik/servlets/log-out">Odjava</a></li>
		</ul>
		
		<h2>Pregled predmeta:</h2>
		
		<div class="container1">
		<p><b>Naziv predmeta:</b> ${subjectGrades.subject.name}</p>
		
		<p><b>Godina na kojoj se predaje:</b> ${subjectGrades.schoolYear}</p>
		
		<p><b>Opis predmeta:</b> ${subjectGrades.subject.description}</p>
		
		<p><b>Termini nastave:</b></p>
		
		<c:forEach var="term" items="${terms}">
			<p>${term.ordinalNumberOfHour}. sat u ${days.get(term.dayOfWeek)} u prostoriji ${term.room.name}</p>
		</c:forEach>
		
		<p><b>Ukupan broj sati:</b> ${termsSize}</p>
		
		<p><b>Ime i prezime profesora:</b> ${termProfessor.name} ${termProfessor.surname} </p>
		
		</div>
		
		<p><b>Ocjene:</b></p>
		<table>
			<tr>
			    <td width="20%">Zalaganje</td>
			    
			    <td>
			    <c:forEach var="grade" items="${subjectGrades.grades}">
				<c:choose>
					<c:when test="${grade.component == 'zalaganje'}">
						${grade.value}   
					</c:when>
				</c:choose>			    
			    </c:forEach>
			    </td>
			 </tr>
			
			 <tr>
			    <td>Pismeno ispitivanje</td>
			    <td>
			    <c:forEach var="grade" items="${subjectGrades.grades}">
				<c:choose>
					<c:when test="${grade.component == 'pismeno ispitivanje'}">
						${grade.value}  
					</c:when>
				</c:choose>			    
			    </c:forEach>
			    </td>
			 </tr>
			 <tr>
			    <td>Usmeno ispitivanje</td>
			    <td>
			    <c:forEach var="grade" items="${subjectGrades.grades}">
				<c:choose>
					<c:when test="${grade.component == 'usmeno ispitivanje'}">
						${grade.value}  
					</c:when>
				</c:choose>			    
			    </c:forEach>
			    </td>
			</tr>
		</table>
		
		<c:if test="${subjectGrades.finalGrade != 0}">
			<p>Zaključena ocjena: ${subjectGrades.finalGrade} </p>
		</c:if>
		
		<h3>Izostanci učenika:</h3>
		
		<div class="container1">
		<c:choose>
				<c:when test="${absences == null || absences.isEmpty()}">
					<p>Učenik nema izostanaka!</p>
				</c:when>
				<c:otherwise>
					<table>
						<colgroup>
							<col span="1" style="background-color:#80ff80">
						</colgroup>
						<tr>
						<th>Datum</th>
						<th>Sat</th>
						<th>Predmet</th>
						<th>Opravdano</th>
						</tr>
					
						<c:forEach var="absence" items="${absences}">
						
							<tr>
							<td><fmt:formatDate value="${absence.date}" pattern="dd.MM.yyyy." /></td>
							<td>${absence.term.ordinalNumberOfHour}.</td>
							<td>${absence.term.subject.name}</td>
							<c:choose>
								<c:when test="${absence.justifiably}">
									<td>opravdano</td>
								</c:when>
								<c:otherwise>
									<td>neopravdano</td>
								</c:otherwise>
							</c:choose>
							</tr>
							
						</c:forEach>
					
					</table>
		
			</c:otherwise>
		</c:choose>
		</div>
		
		<h3>Komentari za učenika:</h3>
		
		<c:choose>
				<c:when test="${comments == null || comments.isEmpty()}">
					<p>Nema komentara za učenika!</p>
				</c:when>
				<c:otherwise>
				
				<table>
					<colgroup>
						<col span="1" style="background-color:#80ff80">
					</colgroup>
					<tr>
					<th>Datum</th>
					<th>Komentar</th>
					</tr>
					
					<c:forEach var="comment" items="${comments}">
						
						<tr>
						<td><fmt:formatDate value="${comment.date}" pattern="dd.MM.yyyy. HH:mm" /></td>
						<td>${comment.text}</td>
						</tr>
						
					</c:forEach>
					
				</table>
		
			</c:otherwise>
		</c:choose>
		
	</body>
</html>